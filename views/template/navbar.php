<?php
$pending_requests = null;
if (Session::get('user')) {
    $pending_requests = Request::getDepartmentRequests(user('type'), ['pending'])->count();
}
?>
<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row ">
            <?php if (Auth::isLoggedIn()) : ?>
            <!--                <div class="col-lg-3 ml-auto">
                                    <form class="input-icon my-3 my-lg-0">
                                        <input type="search" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
                                        <div class="input-icon-addon">
                                            <i class="fe fe-search"></i>
                                        </div>
                                    </form>
                                </div>-->
            <?php else : ?>
            <div class="col-auto">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="<?= linkTo('request'); ?>" class="nav-link"><i class="fe fe-clipboard"></i> Apply to be a
                            member</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= linkTo('login'); ?>" class="nav-link"><i class="fe fe-log-in"></i> Login</a>
                    </li>
                </ul>
            </div>
            <?php endif; ?>
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="<?= linkTo(''); ?>" class="nav-link"><i class="fe fe-home"></i> Home</a>
                    </li>
                    <?php if (!Auth::isLoggedIn()) : ?>
                    <li class="nav-item">
                        <a href="<?= linkTo('events/teetimes'); ?>" class="nav-link"><i class="fe fe-flag"></i> Tee
                            Times</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= linkTo('events/reservations'); ?>" class="nav-link"><i class="fe fe-flag"></i> Events
                            & Banquets</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= linkTo('news'); ?>" class="nav-link"><i class="fe fe-bookmark"></i> Blogs & News</a>
                    </li>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <i class="fe fe-map"></i> Club Facilities
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="<?= linkTo('courses'); ?>" class="dropdown-item "> Golf Courses</a>
                            <a href="<?= linkTo('venues'); ?>" class="dropdown-item "> Club Venues</a>
                        </div>
                    </li>
                    <?php endif; ?>
                    <?php if (Auth::isLoggedIn()) : ?>
                    <?php if (in_array(user('type'), ['officer', 'accounting', 'staff'])) : ?>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <i class="fe fe-cpu"></i> System
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <?php if (in_array(user('type'), ['accounting', 'staff'])) : ?>
                            <a href="<?= linkTo('config/dashboard'); ?>" class="dropdown-item "> Dashboard</a>
                            <?php elseif (in_array(user('type'), ['officer', 'admin', 'administrator'])) : ?>
                            <a href="<?= linkTo('config/dashboard'); ?>" class="dropdown-item "> Dashboard</a>
                            <a href="<?= linkTo('config/dashboard/controller'); ?>" class="dropdown-item "> Update Dashboard View</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('config/traffic'); ?>" class="dropdown-item "> Site Traffic Logs</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('access'); ?>" class="dropdown-item "> Manage User Access</a>
                            <a href="<?= linkTo('access/add'); ?>" class="dropdown-item "> Create User Access</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('config/limits'); ?>" class="dropdown-item "> Site Default Values</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('config/site'); ?>" class="dropdown-item "> Update Front Page</a>
                            <a href="<?= linkTo('config/terms'); ?>" class="dropdown-item "> Update Terms and Conditions</a>
                            <a href="<?= linkTo('config/club'); ?>" class="dropdown-item "> Club Details</a>
                            <a href="<?= linkTo('config/system'); ?>" class="dropdown-item "> System Configuration</a>
                            <a href="<?= linkTo('config/accounting'); ?>" class="dropdown-item "> Accounting Defaults</a>
                            <a href="<?= linkTo('config/email'); ?>" class="dropdown-item "> Email Account</a>
                            <?php endif; ?>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <i class="fe fe-bar-chart"></i> Reports
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <?php if (in_array(user('type'), ['officer', 'accounting', 'staff'])) : ?>
                            <a href="<?= linkTo('reports/delinquency'); ?>" class="dropdown-item ">Delinquency Report</a>
                            <a href="<?= linkTo('reports/profit'); ?>" class="dropdown-item ">Revenue and Profit Report</a>
                            <a href="<?= linkTo('reports/rounds'); ?>" class="dropdown-item "> Golf Rounds Report</a>
                            <a href="<?= linkTo('reports/reservations'); ?>" class="dropdown-item "> Reservations Report</a>
                            <a href="<?= linkTo('reports/reservations/revenue'); ?>" class="dropdown-item "> Reservations Revenue Report</a>
                            <a href="<?= linkTo('reports/booking'); ?>" class="dropdown-item ">Booking Breakdown Report</a>
                            <a href="<?= linkTo('reports/appearance'); ?>" class="dropdown-item ">Reservation Usage Report</a>
                            <a href="<?= linkTo('reports/facilities'); ?>" class="dropdown-item ">Facilities Revenue Report</a>
                            <a href="<?= linkTo('reports/courses'); ?>" class="dropdown-item ">Courses Revenue Report</a>
                            <?php endif; ?>
                        </div>
                    </li>
                    <?php endif; ?>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <i class="fe fe-map"></i> Facilities
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="<?= linkTo('courses'); ?>" class="dropdown-item "> Golf Courses</a>
                            <a href="<?= linkTo('venues'); ?>" class="dropdown-item "> Club Venues</a>
                            <?php if (in_array(user('type'), ['staff', 'officer', 'accounting'])) : ?>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('facilities'); ?>" class="dropdown-item "> View Facilities</a>
                            <a href="<?= linkTo('facilities/add'); ?>" class="dropdown-item "> Add New Facility</a>
                            <?php endif; ?>
                        </div>
                    </li>
                    <?php if (in_array(user('type'), ['staff', 'officer', 'accounting'])) : ?>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <i class="fe fe-package"></i> Inventory
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="<?= linkTo('inventory'); ?>" class="dropdown-item "> View Inventory</a>
                            <a href="<?= linkTo('inventory/add'); ?>" class="dropdown-item "> Add Inventory</a>
                        </div>
                    </li>
                    <?php endif; ?>
                    <?php if (in_array(user('type'), ['member', 'dependent', 'staff', 'officer', 'accounting'])) : ?>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <i class="fe fe-calendar"></i> Reservations
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="<?= linkTo('events/teetimes'); ?>" class="dropdown-item "> Current Tee Times</a>
                            <a href="<?= linkTo('events/reservations'); ?>" class="dropdown-item "> Events & Banquets</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('teetimes'); ?>" class="dropdown-item "> Tee Time Reservations</a>
                            <a href="<?= linkTo('teetimes/add'); ?>" class="dropdown-item "> Make a Tee Time
                                Reservation</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('tournaments'); ?>" class="dropdown-item "> Tournaments</a>
                            <a href="<?= linkTo('tournaments/add'); ?>" class="dropdown-item "> Make a Tournament
                                Request</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('reservations'); ?>" class="dropdown-item "> Facility Reservations</a>
                            <a href="<?= linkTo('reservations/add'); ?>" class="dropdown-item "> Make a Reservation</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('banquets'); ?>" class="dropdown-item "> Banquets</a>
                            <a href="<?= linkTo('banquets/add'); ?>" class="dropdown-item "> Make a Banquet Request</a>
                        </div>
                    </li>
                    <?php endif; ?>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <i class="fe fe-database"></i> Accounting
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="<?= linkTo('invoices'); ?>" class="dropdown-item "> View Invoices</a>
                            <a href="<?= linkTo('billings'); ?>" class="dropdown-item "> View Billings Statements</a>
                            <?php if (in_array(user('type'), ['accounting', 'officer', 'staff'])) : ?>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('accounting/pos'); ?>" class="dropdown-item "> Point of Sale</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('accounting/members'); ?>" class="dropdown-item "> Member Balance
                                Overview</a>
                            <a href="<?= linkTo('accounting/adjust'); ?>" class="dropdown-item "> Adjust Member
                                Balance</a>
                            <a href="<?= linkTo('billings/generate'); ?>" class="dropdown-item "> Generate Billing
                                Statements</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('discounts'); ?>" class="dropdown-item "> View Discount Promotions</a>
                            <a href="<?= linkTo('discounts/add'); ?>" class="dropdown-item "> Add a Discount Promotion</a>
                            <?php endif; ?>
                        </div>
                    </li>
                    <?php if (in_array(user('type'), ['member', 'dependent', 'officer', 'accounting', 'staff'])) : ?>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <?php if ($pending_requests) : ?>
                            <span class="nav-unread"></span>
                            <?php endif; ?>
                            <i class="fe fe-file-text"></i>
                            Requests
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="<?= linkTo('tickets'); ?>" class="dropdown-item "> View Tickets</a>
                            <a href="<?= linkTo('tickets/add'); ?>" class="dropdown-item ">Create a Ticket</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('requests/pending'); ?>" class="dropdown-item "> Pending Requests</a>
                            <a href="<?= linkTo('requests/approved'); ?>" class="dropdown-item ">Approved Requests</a>
                            <a href="<?= linkTo('requests/rejected'); ?>" class="dropdown-item ">Rejected Requests</a>
                            <a href="<?= linkTo('requests/cancelled'); ?>" class="dropdown-item ">Cancelled Requests</a>
                        </div>
                    </li>
                    <?php endif; ?>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <i class="fe fe-bookmark"></i> Club News
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="<?= linkTo('news'); ?>" class="dropdown-item "> Club News & Blog Posts</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('blogs'); ?>" class="dropdown-item "> My Blog Posts</a>
                            <a href="<?= linkTo('blogs/add'); ?>" class="dropdown-item "> Create a Blog Post</a>
                        </div>
                    </li>
                    <?php endif; ?>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-users"></i>
                            Members</a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="<?= linkTo('members/current'); ?>" class="dropdown-item "> Current Members</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('members/officers'); ?>" class="dropdown-item "> Club Officers</a>
                            <a href="<?= linkTo('members/staff'); ?>" class="dropdown-item "> Club Staff</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('members/review'); ?>" class="dropdown-item "> In Review</a>
                            <?php if (in_array(user('type'), ['member', 'officer'])) : ?>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('members/endorse'); ?>" class="dropdown-item "> Endorse a New Member</a>
                            <?php endif; ?>
                            <?php if (in_array(user('type'), ['member', 'officer'])) : ?>
                            <div class="dropdown-divider"></div>
                            <a href="<?= linkTo('dependents'); ?>" class="dropdown-item "> Dependents</a>
                            <a href="<?= linkTo('dependents/add'); ?>" class="dropdown-item "> Add Dependent</a>
                            <?php endif; ?>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="<?= linkTo('contact'); ?>" class="nav-link"><i class="fe fe-phone"></i> Contact Us</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
