<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="<?=File::get(Config::get('system')->favicon, 'app/uploads'); ?>" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon"
        href="<?=File::get(Config::get('system')->favicon, 'app/uploads'); ?>" />
    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title><?=Config::get('club')->name; ?> - <?=Config::get('club')->title; ?></title>
    <link rel="stylesheet" href="/assets/plugins/font-awesome/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">-->
    <!-- JQuery -->
    <script src="/assets/js/jquery-3.3.1.min.js"></script>
    <!-- timeAgo-->
    <script src="/assets/js/vendors/timeago.min.js"></script>
    <!-- Jodit WYSIWYG -->
    <link href="/assets/plugins/jodit/build/jodit.min.css" rel="stylesheet" />
    <script src="/assets/plugins/jodit/build/beautify-html.min.js"></script>
    <script src="/assets/plugins/jodit/build/jodit.min.js"></script>
    <!-- dataTables -->
    <link href="/assets/css/vendors/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <script src="/assets/js/vendors/jquery.dataTables.min.js"></script>
    <script src="/assets/js/vendors/dataTables.bootstrap4.min.js"></script>
    <!-- dateRangePicker -->
    <script src="/assets/plugins/daterangepicker/moment.min.js"></script>
    <script src="/assets/plugins/daterangepicker/daterangepicker.min.js"></script>
    <link href="/assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" />
    <!-- ChartJS -->
    <script src="/assets/js/vendors/Chart.min.js"></script>
    <script src="/assets/js/vendors/chartjs-plugin-colorschemes.min.js"></script>
    <!-- Tabler -->
    <script src="/assets/js/require.min.js"></script>
    <!-- Dashboard Core -->
    <link href="/assets/css/dashboard.css" rel="stylesheet" />
    <script src="/assets/js/dashboard.js"></script>
    <!-- Custom Core -->
    <link href="/assets/css/app.css" rel="stylesheet" />
    <!-- App scripts -->
    <script src="/assets/js/app.js"></script>
    <script>
    requirejs.config({
        baseUrl: '/'
    });
    </script>

    <!-- c3.js Charts Plugin -->
    <!--    <link href="/assets/plugins/charts-c3/plugin.css" rel="stylesheet" />
    <script src="/assets/plugins/charts-c3/plugin.js"></script>-->

</head>
<div style="width: 100%;">
    <?php foreach (['success', 'info', 'warning', 'danger'] as $notificationMessage): ?>
    <?php if (Session::get($notificationMessage)): ?>
    <div class="alert alert-<?=$notificationMessage; ?> alert-dismissible"
        style="margin-bottom: 0; border-radius: 0; z-index: 100; text-align: center;">
        <button type="button" class="close" data-dismiss="alert"></button>
        <?php $notificationMessage = Session::pop($notificationMessage); ?>
        <?php if (is_array($notificationMessage)): ?>
        <?php foreach ($notificationMessage as $m): ?>
        <?=$m; ?><br>
        <?php endforeach; ?>
        <?php else: ?>
        <?=$notificationMessage; ?>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <?php endforeach; ?>
</div>
