<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-auto ml-auto">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="<?=linkTo(''); ?>" class="nav-link"><i class="fe fe-home"></i> Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-users"></i>
                            Members</a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="<?=linkTo('members/current'); ?>" class="dropdown-item "> Current</a>
                            <a href="<?=linkTo('members/review'); ?>" class="dropdown-item "> In Review</a>
                            <a href="<?=linkTo('members/officers'); ?>" class="dropdown-item "> Club Officers</a>
                            <a href="<?=linkTo('members/staff'); ?>" class="dropdown-item "> Club Staff</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-auto">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="<?=linkTo('request'); ?>" class="nav-link"><i class="fe fe-clipboard"></i> Apply to be a
                            member</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=linkTo('login');?>" class="nav-link"><i class="fe fe-log-in"></i> Login</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
