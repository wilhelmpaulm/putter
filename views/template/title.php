<?php
if (Session::get('user')) {
    $notifs = Message::where('sent_to', user('id'))
        ->where('read', 0)
        ->orderBy('id', 'desc')
        ->limit(5)
        ->get();
}
?>
<div class="header py-4">
    <div class="container">
        <div class="d-flex">
            <a class="header-brand" href="/">
                <img src="<?= File::get(Config::get('system', 'header'), 'app/uploads'); ?>" class="header-brand-img" alt="app logo">
            </a>
            <?php if (!Auth::isLoggedIn()): ?>
            <div class="d-flex order-lg-2 ml-auto"></div>
            <?php else: ?>
            <div class="d-flex order-lg-2  ml-auto">
                <div class="dropdown d-none d-md-flex">
                    <a class="nav-link icon" data-toggle="dropdown">
                        <i class="fe fe-bell"></i>
                        <?php if (count($notifs)): ?>
                        <span class="nav-unread"></span>
                        <?php endif; ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <?php foreach ($notifs as $n): ?>
                        <a href="<?= linkTo("messages/{$n->id}/read"); ?>" class="dropdown-item d-flex">
                            <span class="avatar mr-3 align-self-center avatar-<?= $n->user->color; ?>"
                            <?php if ($n->user->image): ?>
                                style="background-image: url('<?= File::get($n->user->image); ?>')"
                            <?php endif; ?>

                                >
                                <?= $n->user->image ? '' : $n->user->short; ?>
                            </span>
                            <div>
                                <strong>
                                    <?= title_case($n->user->first_name); ?></strong>
                                <span class="small text-muted timeago">
                                    <?= $n->created_at; ?></span>
                                <div class="">
                                    <?= substr($n->message, 0, 50).' . . . '; ?>
                                </div>
                            </div>
                        </a>
                        <?php endforeach; ?>
                        <?php if (count($notifs)): ?>
                        <div class="dropdown-divider"></div>
                        <a href="<?= linkTo('messages/all/read'); ?>" class="dropdown-item text-muted-dark">
                            <i class="dropdown-icon fe fe-check"></i> Mark all as read
                        </a>
                        <?php else: ?>
                        <div class="dropdown-item text-muted disabled">
                            <i class="dropdown-icon fe fe-info"></i> No new messages or notifications
                        </div>
                        <?php endif; ?>
                        <div class="dropdown-divider"></div>
                        <a href="<?= linkTo('messages/system'); ?>" class="dropdown-item">
                            <i class="dropdown-icon fe fe-cpu"></i> System Notifications
                        </a>
                        <a href="<?= linkTo('messages/read'); ?>" class="dropdown-item">
                            <i class="dropdown-icon fe fe-mail"></i> View Read Messages
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="<?= linkTo('messages/send'); ?>" class="dropdown-item">
                            <i class="dropdown-icon fe fe-send"></i> Send a Message
                        </a>
                    </div>
                </div>
                <div class="dropdown">
                    <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                        <span class="avatar avatar-<?= user('color'); ?>" <?php if (user('image')): ?>
                            style="background-image: url(
                            <?= File::get(user('image')); ?>)"
                            <?php endif; ?>
                            >
                            <?= File::get(user('image')) ? '' : user('short'); ?>
                        </span>
                        <span class="ml-2 d-none d-lg-block">
                            <span class="text-default">
                                <?= title_case(user('first_name')); ?></span>
                            <small class="text-muted text-center d-block mt-1">
                                <?= user('type'); ?></small>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a class="dropdown-item" href="<?= linkTo('me'); ?>">
                            <i class="dropdown-icon fe fe-user"></i> Profile
                        </a>
                        <a class="dropdown-item" href="/me/settings">
                            <i class="dropdown-icon fe fe-settings"></i> Settings
                        </a>
                        <a class="dropdown-item" href="/messages/unread">
                            <!--<span class="float-right"><span class="badge badge-primary">6</span></span>-->
                            <i class="dropdown-icon fe fe-mail"></i> Messages
                        </a>
                        <?php if (in_array(user('type'), ['member'])): ?>
                        <div class="dropdown-divider"></div>
                        <a href="<?= linkTo('me/cancel'); ?>" class="dropdown-item ">
                            <i class="dropdown-icon fe fe-user-x"></i> Cancel Membership
                        </a>
                        <?php endif; ?>
                        <?php if (in_array(user('type'), ['dependent'])): ?>
                        <div class="dropdown-divider"></div>
                        <a href="<?= linkTo('me/independent'); ?>" class="dropdown-item ">
                            <i class="dropdown-icon fe fe-user-plus"></i> Request to be an Independent Member
                        </a>
                        <?php endif; ?>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="mailto:<?= Config::get('email', 'address'); ?>">
                            <i class="dropdown-icon fe fe-help-circle"></i> Need help?
                        </a>
                        <a class="dropdown-item" href="/logout">
                            <i class="dropdown-icon fe fe-log-out"></i> Sign out
                        </a>
                        <?php if (in_array(user('type'), ['dependent'])): ?>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item disabled text-muted-dark" href="/members/<?= user('sponsor')->id; ?>">
                            <i class="dropdown-icon fe fe-database"></i>
                            Your balances are billed to
                            <?= user('sponsor')->full_name; ?>
                        </a>
                        <?php elseif (in_array(user('type'), ['member', 'guest'])): ?>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item disabled text-muted-dark" title="Your current balance">
                            <i class="dropdown-icon fe fe-database"></i>PHP
                            <?= user('balance'); ?>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="d-flex order-lg-2"></div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
            </a>
        </div>
    </div>
</div>
