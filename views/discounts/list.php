<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('discounts/nav') ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">View Discount Promotions</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th class="w-1">ID.</th>
                                <th>Discount</th>
                                <th>Breakdown</th>
                                <th>Duration</th>
                                <th width="1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($items as $i): ?>
                                <tr>
                                    <td class="">
                                        <a href="<?= linkTo("discounts/{$i->id}") ?>" class="text-muted small"><?= padId($i->id) ?></a>
                                    </td>
                                    <td class="text-wrap">
                                        <a href="<?= File::get($i->image) ?>" target="_blank">
                                            <img src="<?= File::get($i->image ?? 'default.png') ?>" alt="..." class="img-thumbnail"  style="max-width: 10rem">
                                        </a>
                                        <br>
                                        <a href="<?= linkTo("discounts/{$i->id}") ?>">
                                            <strong class="text-capitalize">
                                                <?= $i->name ?>
                                            </strong>
                                        </a>
                                        <div class="small">
                                            <?= $i->label ?>
                                        </div>
                                        <br>
                                        <div class="text-capitalize small">
                                            For Item:
                                            <a href="<?= linkTo("inventory/{$i->id}") ?>">
                                                <?= $i->inventory->name ?>
                                            </a>
                                        </div>
                                        <div class="small">
                                            Published: <?= $i->public ? 'true' : 'false' ?>
                                        </div>
                                        <div class="small">
                                            Active: <?= $i->active ? 'true' : 'false' ?>
                                        </div>
                                        <div class="small">
                                            Created at: <span class="timeago" datetime="<?= $i->created_at ?>"></span>
                                        </div>

                                    </td>
                                    <td class="small text-nowrap">
                                        <div class="">
                                            <strong>New Total:</strong> <br>PHP <?= number_format($i->inventory->price * $i->multiplier, 2) ?>
                                        </div>
                                        <br>
                                        <div class="">
                                            <strong>Multiplier:</strong> <br><?= $i->multiplier ?>%
                                        </div>
                                        <br>
                                        <div class="">
                                            <strong>Current Price:</strong> <br> PHP <?= number_format($i->inventory->price, 2)?>
                                        </div>
                                    </td>
                                    <td class="small text-nowrap">
                                        <div class="">
                                            <strong>Redemption Start:</strong> <br> <?= (new DateTime($i->start_date))->format("F j, Y, g:i A") ?>
                                        </div>
                                        <br>
                                        <div class="">
                                            <strong>Redemption End:</strong> <br><?= (new DateTime($i->end_date))->format("F j, Y, g:i A") ?>
                                        </div>
                                        <?php if ($i->discount_start_date): ?>
                                            <br>
                                            <div class="">
                                                <strong>Reservation End:</strong> <br><?= (new DateTime($i->discount_dtart_date))->format("F j, Y, g:i A") ?>
                                            </div>
                                            <br>
                                            <div class="">
                                                <strong>Reservation End:</strong> <br><?= (new DateTime($i->discount_end_date))->format("F j, Y, g:i A") ?>
                                            </div>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a  href="<?= linkTo("discounts/{$i->id}") ?>"
                                            title="Update details of <?= $i->name ?>"
                                            class="icon">
                                            <i class="fe fe-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>