<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('discounts/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/discounts/add" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Create a Discount Promotion
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-label">Inventory Item</label>
                                <select name="inventory_id"  id="select-inventory" class="form-control custom-select">
                                    <?php foreach ($items as $u): ?>
                                        <option
                                        <?= $item->id == $u->id ? 'selected=""' : '' ?>
                                            data-data='{"image": "<?= File::get($u->image) ?>", "short": "<?= $u->short ?>", "color": "<?= $u->color ?>"}'
                                            value="<?= $u->id ?>">
                                                <?= ucwords("{$u->name} ({$u->type})") ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <?php if ($item): ?>
                                <div class="form-group">
                                    <table class="table table-hover table-bordered">
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td class="">
                                                    <img src="<?= File::get($item->image ?? 'default.png') ?>" class="img-thumbnail" style="width: 8rem"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Type</td>
                                                <td><?= $item->type ?></td>
                                            </tr>
                                            <tr>
                                                <td>Label</td>
                                                <td><?= $item->label ?></td>
                                            </tr>
                                            <tr>
                                                <td>Details</td>
                                                <td><?= $item->details ?></td>
                                            </tr>
                                            <?php if ($item->location): ?>
                                                <tr>
                                                    <td>Location</td>
                                                    <td><?= $item->location ?></td>
                                                </tr>
                                            <?php endif; ?>
                                            <?php if ($item->latitude && $item->longitude): ?>
                                                <tr>
                                                    <td>Coordinates</td>
                                                    <td><?= "$item->latitude , $item->longitude" ?></td>
                                                </tr>
                                            <?php endif; ?>
                                            <tr>
                                                <td>Is Available</td>
                                                <td><?= $item->available ? 'true' : 'false' ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>
                            <div class="border my-3" style="width: 100%"></div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <div class="form-label">Discount Poster</div>
                                        <img src="<?= File::get('default.png') ?>" style="margin-bottom: .5rem; height: 8rem" class="img-thumbnail" alt="Heade image">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" accept="image/*" name="image">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="border my-3" style="width: 100%"></div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Set Discount Duration</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend" id="basic-addon1">
                                                <span class="input-group-text">
                                                    <i class="fe fe-calendar"></i>
                                                </span>
                                            </span>
                                            <input type="text" id="daterangepicker" required=""  class="form-control" title="the start date of the discount">
                                            <input type="hidden" id="start_date" name="start_date" value="<?= date("Y-m-d H:i:s", strtotime("now")); ?>" required="">
                                            <input type="hidden" id="end_date" name="end_date" value="<?= date("Y-m-d H:i:s", strtotime("+1 day")); ?>" required="">
                                        </div>
                                    </div>
                                </div>
                                <div id="reservation_date" class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Set Reservation Duration</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend" id="basic-addon1">
                                                <span class="input-group-text">
                                                    <i class="fe fe-calendar"></i>
                                                </span>
                                            </span>
                                            <input type="text" id="daterangepicker-discount" data-position='left' class="form-control" title="the start date of the discount">
                                            <input type="hidden" id="discount_start_date" name="discount_start_date" value="<?= date("Y-m-d H:i:s", strtotime("now")); ?>">
                                            <input type="hidden" id="discount_end_date" name="discount_end_date" value="<?= date("Y-m-d H:i:s", strtotime("+30 day")); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" id="for_reservation" name="reservation" value="1" class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Is this for a reservation?</span>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Current Price</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend" id="basic-addon1">
                                                <span class="input-group-text">
                                                    PHP
                                                </span>
                                            </span>
                                            <input type="number" id="current_price" name="current_price" class="form-control" readonly="" value="<?= $item->price ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="base_price">Multiplier</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend" id="basic-addon1">
                                                <span class="input-group-text">
                                                    %
                                                </span>
                                            </span>
                                            <input type="number" id="multiplier" name="multiplier" min="0" class="form-control" step=".01" required="true" placeholder="This the multiplier that would be applied to the item price" value="1.0">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="price">Final Price</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend" id="basic-addon1">
                                                <span class="input-group-text">
                                                    PHP
                                                </span>
                                            </span>
                                            <input type="number" id="final_price" name="final_price" class="form-control" min="0"  step="any" value="<?= $item->price ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="active" value="1" checked="" class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Set discount as active</span>
                                </label>
                            </div>
                            <div class="border my-3" style="width: 100%"></div>
                            <div class="form-group">
                                <label class="form-label">Discount Promotion Name</label>
                                <input type="text" id="name" name="name" required="" class="form-control" placeholder="This will appear on pos">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Promotion Display Label</label>
                                <input type="text" id="label" name="label" required="" class="form-control" placeholder="This will serve as the product identifier">
                            </div>
                            <div class="form-group" style="height: 25rem; padding-bottom: 1.5rem">
                                <label class="form-label">Promotion Details Post</label>
                                <textarea class="form-control" id="jodit" name="details" rows="5" placeholder="Big belly rude boy, million dollar hustler. Unemployed."></textarea>
                            </div>

                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="public" value="1" checked="" class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Publish this Promotion</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>
