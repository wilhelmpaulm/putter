<h3 class="page-title mb-5">Discount Promotions</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo('discounts') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-tag"></i></span> View Discount Promotions
        </a>
        <a href="<?= linkTo('discounts/add') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-plus"></i></span> Create a Discount Promotion
        </a>
    </div>
</div>