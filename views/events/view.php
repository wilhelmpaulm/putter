<!doctype html>
<html lang="en" dir="ltr">
<?php include linkPage('template/head'); ?>

<body class="">
    <div class="page">
        <div class="page-main">
            <?php include linkPage('template/title'); ?>
            <?php include linkPage('template/navbar'); ?>
            <div class="my-3 my-md-5">
                <div class="container">
                    <div class="row row-cards row-deck">
                        <div class="col-8 offset-2">
                            <div class="card">
                                <div class="card-body d-flex flex-column">
                                    <?php if ($item->image && File::get($item->image)): ?>
                                    <div class="text-center">
                                        <img src="<?= File::get($item->image); ?>" class="img-thumbnail">
                                    </div>
                                    <br>
                                    <?php endif; ?>
                                    <h2><?= ucwords($item->name); ?></h2>
                                    <p class="">
                                        <strong><?= $item->facility->name; ?> </strong> -
                                        <?= (new DateTime($item->start_date))->format('F j, Y, g:i A'); ?>
                                        Until
                                        <?= (new DateTime($item->end_date))->format('F j, Y, g:i A'); ?>
                                    </p>

                                    <blockquote class="blockquote">
                                        <p class="mb-0"><?= ucfirst($item->label); ?></p>
                                    </blockquote>
                                    <div class="" style="overflow: scroll">
                                        <?= $item->details; ?>
                                    </div>
                                    <br>
                                    <div class="">
                                        <div class="pull-right">
                                            <?php foreach ($item->tag_list as $t) : ?>
                                            <div class="text-center tag">
                                                <?= $t; ?>
                                            </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center pt-5 mt-auto">
                                        <div class="avatar avatar-md mr-3" style="background-image: url(<?= File::get($item->user->image); ?>)">
                                            <?= File::get($item->user->image) ?? $item->user->short; ?>
                                        </div>
                                        <div>
                                            <a href="/members/<?= $item->user_id; ?>" class="text-default"><?= $item->user->full_name; ?></a>
                                            <small class="d-block text-muted timeago" datetime="<?= $item->created_at; ?>"></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include linkPage('template/footer'); ?>
</body>

</html>
