<!doctype html>
<html lang="en" dir="ltr">
    <?php include linkPage("template/head"); ?>
    <body class="">
        <div class="page">
            <div class="page-main">
                <?php include linkPage('template/title') ?>
                <?php include linkPage('template/navbar') ?>    
                <div class="my-3 my-md-5">
                    <div class="container">
                        <div class="page-header">
                            <h1 class="page-title">
                                Tee Time & Tournament Reservations
                            </h1>
                            <form action="/events/teetimes" class="page-options d-flex">
                                <div class="input-group" style="padding-right: 1rem">
                                    <select name="facility_id"  id="select-reload" data-url="/teetimes/add?facility_id=" class="form-control custom-select"> 
                                        <?php foreach ($courses as $u): ?>
                                            <option
                                            <?= Input::get('facility_id') == $u->id ? 'selected=""' : '' ?>
                                                data-data='{"image": "<?= File::get($u->image) ?>", "short": "<?= $u->short ?>", "color": "<?= $u->color ?>"}'
                                                value="<?= $u->id ?>">
                                                    <?= ucwords($u->name) ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-prepend" id="basic-addon1">
                                        <span class="input-group-text">
                                            <i class="fe fe-calendar"></i>
                                        </span>
                                    </span>
                                    <input type="text" id="date" required=""  class="form-control" value=""  title="the start date of the discount">
                                    <input type="hidden" id="start_date" name="start_date" required=""  class="form-control" value="<?= Input::get('start_date') ? Input::get('start_date') : date("Y-m-d", strtotime("now")); ?>"  title="the start date of the discount">
                                </div>
                            </form>
                        </div>
                        <div class="row row-cards row-deck">
                            <div class="col-12">
                                <div class="card">
                                    <div class="">
                                        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center "></th>
                                                    <th class="text-center w-1"><i class="icon-people"></i></th>
                                                    <th>User</th>
                                                    <th class="text-center">Players</th>
                                                    <th></th>
                                                    <th class="text-center">Date Reserved</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($teeTimes as $t => $i): ?>
                                                    <tr>
                                                        <td class="text-center">
                                                            <span class=""><?= $i->value ?></span>
                                                        </td>
                                                        <?php if ($i->reservation && !$i->reservation->public): ?>
                                                            <td colspan="5"class="text-center">
                                                                <strong>
                                                                    Private Reservation
                                                                </strong>
                                                            </td>
                                                        <?php elseif ($i->reservation): ?>
                                                            <td class="text-center">
                                                                <div class="avatar d-block" style="background-image: url(demo/faces/female/26.jpg)">
                                                                    <span class="avatar-status bg-green"></span>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div><?= $i->reservation->user->full_name ?? '' ?></div>
                                                                <div class="small text-muted">

                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="avatar-list avatar-list-stacked">
                                                                    <?php foreach ($i->reservation->guest_list as $u): ?>
                                                                        <a href="/members/<?= $u->id ?>" 
                                                                           class="avatar avatar-<?= $u->color ?>" 
                                                                           style="background-image: url(<?= File::get($u->image) ?>)"
                                                                           title="<?= $u->full_name ?>">
                                                                               <?= File::get($u->image) ? "" : $u->short ?>
                                                                        </a>
                                                                    <?php endforeach; ?>
                                                                    <?php if (count($i->reservation->guest_list) > 5): ?>
                                                                        <span class="avatar" title="<?= count($i->reservation->guest_list) ?> guests">
                                                                            <?= count($i->reservation->guest_list) ?>
                                                                        </span>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <?= $i->reservation->type ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <small class="timeago" datetime="<?= $i->reservation->created_at ?>"></small>
                                                            </td>
                                                        <?php else: ?>
                                                            <td colspan="5"class="text-center">
                                                                <strong>
                                                                    Open for Reservation
                                                                </strong></td>
                                                        <?php endif; ?>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include linkPage("template/footer"); ?>
    </body>
</html>