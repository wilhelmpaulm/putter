<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('members/nav'); ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/members/<?= $id; ?>/update" method="post" enctype="multipart/form-data">
                <?php if (Input::get('sponsor_id')) : ?>
                <input type="hidden" name="sponsor_id" value="<?= Input::get('sponsor_id'); ?>">
                <?php endif; ?>
                <?php if (Input::get('endorser_id')) : ?>
                <input type="hidden" name="endorser_id" value="<?= Input::get('endorser_id'); ?>">
                <?php endif; ?>
                <div class="card-body">
                    <h3 class="card-title">
                        Update Member Details
                    </h3>
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Email address</label>
                                        <input type="email" name="email" required="" readonly="" autocomplete="false" class="form-control" value="<?= $member->email; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">First Name</label>
                                        <input type="text" name="first_name" required="" class="form-control" placeholder="Company" value="<?= $member->first_name; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Middle Name</label>
                                        <input type="text" name="middle_name" class="form-control" placeholder="Company" value="<?= $member->middle_name; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Last Name</label>
                                        <input type="text" name="last_name" required="" class="form-control" placeholder="Last Name" value="<?= $member->last_name; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Gender</label>
                                        <select name="gender" required="" class="form-control custom-select">
                                            <option value="male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Mobile</label>
                                        <input type="text" name="mobile" required="" class="form-control" value="<?= $member->mobile; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Company</label>
                                        <input type="text" name="company" required="" class="form-control" placeholder="Company name" value="<?= $member->company; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Occupation</label>
                                        <input type="text" name="occupation" required="" class="form-control" placeholder="Position and Job Title" value="<?= $member->occupation; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-8">
                                    <div class="form-group">
                                        <div class="form-label">Profile Image</div>
                                        <img src="<?= File::get('default.png'); ?>" style="margin-bottom: .5rem; height: 6rem" class="img-thumbnail" alt="Heade image">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" accept="image/*" name="image">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Address</label>
                                        <input type="text" name="address" required="" class="form-control" placeholder="Home Address" value="<?= $member->address; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">City</label>
                                        <input type="text" name="city" required="" class="form-control" placeholder="City" value="<?= $member->city; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="form-label">Zip Code</label>
                                        <input type="number" name="zip_code" required="" class="form-control" value="<?= $member->zip_code; ?>">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="form-label">Country</label>
                                        <input type="text" name="country" required="" class="form-control" placeholder="Philippines" value="<?= $member->country; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-label">Please tell us more about yourself</label>
                                <textarea rows="5" name="details" required="" class="form-control" placeholder="Give a brief narrative of your background."><?= $member->details; ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-label">Why do you want to join the club?</label>
                                <textarea rows="5" name="intent" required="" class="form-control" placeholder="Give a brief explanation of your intentions in the club and how you found out about us."><?= $member->intent; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-footer text-right">
                        <button type="submit" class="btn btn-primary btn-block">Submit Form</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php include linkPage('template/_bottom'); ?>
