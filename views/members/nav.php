<h3 class="page-title mb-5">
    <?= ucwords("$member->first_name $member->last_name"); ?>
</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo("members/$id/profile"); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-user"></i></span>Profile
        </a>
        <?php if (in_array(user('type'), ['officer', 'admin'])): ?>
        <a href="<?= linkTo("members/$id/edit"); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-edit"></i></span> Update
        </a>
        <?php endif; ?>
        <?php if (Auth::isLoggedIn()): ?>
        <a href="<?= linkTo("members/$id/comments"); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-message-square"></i></span> Comments
        </a>
        <?php endif; ?>
    </div>
    <!--    <div class="mt-6">
            <a href="#" class="btn btn-secondary btn-block">Compose new Email</a>
        </div>-->
</div >
