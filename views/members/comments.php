<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('members/nav') ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <form class="card" action="<?= linkTo("members/$member->id/comments") ?>" method="post"  enctype="multipart/form-data">
                    <div class="card-header">
                        <h3 class="card-title">Comments for <?= $member->full_name?></h3>
                    </div>
                    <div class="card-body">
                        <label class="form-label">Add Comment</label>
                        <div class="input-group">
                            <input type="text" name="message" required="" class="form-control" placeholder="Message">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">
                                    <i class="fe fe-send"></i> Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <ul class="list-group card-list-group">
                    <?php if (!count($member->comments)): ?>
                        <li class="list-group-item text-center">
                            <h5> No Comments</h5>
                        </li>
                    <?php endif; ?>
                    <?php foreach ($member->comments as $c): ?>
                        <li class="list-group-item ">
                            <div class="media">
                                <div class="media-object avatar avatar-md avatar-<?= $c->user->color ?> mr-4" style="background-image: url(demo/faces/male/16.jpg)"><?= $c->user->short ?></div>
                                <div class="media-body">
                                    <div class="media-heading">
                                        <small class="float-right text-muted timeago" datetime="<?= $c->created_at ?>"></small>
                                        <h5 class="text-capitalize"><?= $c->user->first_name . " " . $c->user->last_name ?></h5>
                                    </div>
                                    <div class=""><?= $c->message ?></div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<?php include linkPage('template/_bottom') ?>