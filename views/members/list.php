<!doctype html>
<html lang="en" dir="ltr">
<?php include linkPage('template/head'); ?>

<body class="">
    <div class="page">
        <div class="page-main">
            <?php include linkPage('template/title'); ?>
            <?php include linkPage('template/navbar'); ?>
            <div class="my-3 my-md-5">
                <div class="container">
                    <div class="page-header">
                        <h1 class="page-title">
                            <?= ucfirst($title); ?>
                        </h1>
                    </div>
                    <div class="row row-cards row-deck">
                        <?php foreach ($members as $m) : ?>
                        <div class="col-lg-6">
                            <div class="card card-aside">
                                <a href="<?= linkTo("members/$m->id/profile"); ?>" class="card-aside-column text-center">
                                    <span class="avatar avatar-xxl avatar-<?= $m->color; ?>" style="margin-top: 1.5em; background-image: url(<?= File::get($m->image); ?>);">
                                        <?= File::get($m->image) ? '' : $m->short; ?>
                                    </span>
                                </a>
                                <div class="card-body d-flex flex-column">
                                    <h4><a href="<?= linkTo("members/$m->id/profile"); ?>">
                                            <?= padId($m->id).' - '.ucwords("$m->first_name $m->middle_name $m->last_name"); ?> </a></h4>
                                    <small class="">
                                        <?= $m->created_at; ?></small>
                                    <br>
                                    <b class="">Bio</b>
                                    <small class="">
                                        <?= $m->intent; ?></small>
                                    <br>
                                    <b class="">Details</b>
                                    <small class="">
                                        <?= $m->details; ?></small>
                                    <br>
                                    <?php if ($m->company) : ?>
                                    <b class="">Company</b>
                                    <div class="small">
                                        <?= ucfirst($m->company); ?> -
                                        <?= ucfirst($m->occupation); ?>
                                    </div>
                                    <br>
                                    <?php endif; ?>
                                    <?php if ($m->sponsor_id) : ?>
                                    <b class="">Sponsor</b>
                                    <a href="/members/<?= $m->sponsor_id; ?>">
                                        <?= ucfirst($m->sponsor->first_name.' '.$m->sponsor->last_name).' ('.$m->sponsor->type.')'; ?></a>
                                    <br>
                                    <?php endif; ?>
                                    <?php if (!$m->sponsor_id && $m->endorser_id) : ?>
                                    <b class="">Endorser</b>
                                    <a href="/members/<?= $m->endorser_id; ?>">
                                        <?= ucfirst($m->endorser->first_name.' '.$m->endorser->last_name).' ('.$m->endorser->type.')'; ?></a>
                                    <br>
                                    <?php endif; ?>
                                    <b class="text-default">
                                        <?= ucfirst($m->type); ?></b>
                                    <?php if (Auth::isLoggedIn()) : ?>
                                    <small class="d-block">Email:
                                        <?= $m->email; ?></small>
                                    <small class="d-block">Mobile:
                                        <?= $m->mobile; ?></small>
                                    <br>
                                    <?php endif; ?>
                                    <?php if (user('id') != $m->id && in_array(user('type'), ['officer', 'admin', 'administrator'])) : ?>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="form-label">Update Type</label>
                                                <select class="form-control custom-select staff-toggle-type" data-user_id="<?= $m->id; ?>">
                                                    <?php foreach ($staff_types as $value) : ?>
                                                    <option value="<?= $value; ?>" <?= $value == $m->type ? 'selected=""' : ''; ?> >
                                                        <?= ucfirst($value); ?>
                                                    </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <?php if (user('id') != $m->id && in_array(user('type'), ['officer', 'admin', 'administrator'])) : ?>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="form-label">Update User Access</label>
                                                <select class="form-control custom-select staff-toggle-access" data-user_id="<?= $m->id; ?>">
                                                    <?php foreach ($access_types as $value) : ?>
                                                    <option value="<?= $value; ?>" <?= $value == $m->access ? 'selected=""' : ''; ?> >
                                                        <?= ucfirst($value); ?>
                                                    </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <?php if (user('id') != $m->id && in_array(user('type'), ['officer', 'admin', 'administrator'])) : ?>
                                    <br>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="custom-switch">
                                                    <input type="checkbox" name="active" class="custom-switch-input dependents-toggle-status" data-user_id="<?= $m->id; ?>" <?php if ($m->active) : ?>
                                                    checked=""
                                                    data-status="deactivate"
                                                    <?php else : ?>
                                                    data-status="activate"
                                                    <?php endif; ?>
                                                    value="1">
                                                    <span class="custom-switch-indicator"></span>
                                                    <span class="custom-switch-description">Give club access</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </div>

        <?php include linkPage('template/footer'); ?>
</body>

</html>
