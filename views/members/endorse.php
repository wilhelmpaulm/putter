<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col col-login mx-auto">
            <form class="card" action="/members/endorse" method="post">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Send Endorsed Registration Link
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-label">Email address</label>
                                <input type="email" name="email" class="form-control" required="true" placeholder="Enter email">
                                <ul>
                                    <li>
                                        <small class="form-text">
                                            Your profile would be attached to the person receiving the 
                                            endorsed registration during the review process.
                                        </small>
                                    </li>
                                    <li>
                                        <small class="form-text">
                                            By sending this link you are actively endorsing the receiver.
                                        </small>
                                    </li>
                                    <li>
                                        <small class="form-text">
                                            Please be reminded that all endorsed membership requests are subject to 
                                            officer approval.
                                        </small>
                                    </li>
                                </ul>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="agree" value="1" class="custom-switch-input agree">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">I agree with <a href='/terms'>terms and conditions</a></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="">
                        <button type="submit" disabled="" class="btn btn-primary btn-block needAgree">Send request</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php include linkPage('template/_bottom') ?>