<!doctype html>
<html lang="en" dir="ltr">
    <?php include linkPage("template/head"); ?>
    <body class="">
        <div class="page">
            <div class="page-single">
                <div class="container">
                    <div class="row">
                        <div class="col col-login mx-auto">
                            <div class="text-center mb-6">
                                <a href="/">
                                    <img src="<?= File::get(Config::get('system')->header, 'app/uploads') ?>" class="h-6" alt="">
                                </a>
                            </div>
                            <form id='request' class="card" action="/request" method="post">
                                <div class="card-body p-6">
                                    <div class="card-title">Membership Request</div>
                                    <div class="form-group">
                                        <label class="form-label">Email address</label>
                                        <input type="email" name="email" class="form-control" required="true" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="agree" value="1" class="custom-switch-input agree">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">I agree with <a href='/terms'>terms and conditions</a></span>
                                        </label>
                                    </div>
                                    <div class="form-footer">
                                        <button type="submit" disabled="" class="btn btn-primary btn-block needAgree">Send request</button>
                                    </div>
                                </div>
                            </form>
                            <div class="text-center text-muted">
                                Already have account? <a href="/login">Sign in</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>