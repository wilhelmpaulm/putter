<h3 class="page-title mb-5">Facility Reservations</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo('reservations') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-flag"></i></span>List of Reservations
        </a>
        <a href="<?= linkTo('reservations/add') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-plus"></i></span> Make a Facility Reservation
        </a>
    </div>
</div>