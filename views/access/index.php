<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('config/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card" action="/config/system" method="post" enctype="multipart/div-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Manage User Access
                    </h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th class="">User Type</th>
                                <th>Details</th>
                                <th>Status</th>
                                <th>Access Ratio</th>
                                <th width="1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($types as $i): ?>
                            <tr>
                                <td>
                                    <a href="<?=linkTo("access/{$i->id}/edit"); ?>">
                                        <div class="text-capitalize">
                                            <?=$i->type; ?>
                                        </div>
                                    </a>
                                </td>
                                <td class="">
                                    <small><?=$i->details; ?></small>
                                </td>
                                <td class="">
                                    <div class="text-center tag">
                                        <?=$i->active ? 'active' : 'disabled'; ?>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="progress">
                                        <div class="progress-bar bg-success  progress-bar-striped progress-bar-animated"
                                            role="progressbar" style="width: <?=$i->ratio; ?>%"
                                            aria-valuenow="<?=$i->ratio; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                        <!-- <div class="progress-bar bg-gray" role="progressbar" style="width: 65%"
                                            aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div> -->
                                    </div>
                                </td>
                                <td>
                                    <a href="<?=linkTo("access/{$i->id}/edit"); ?>"
                                        title="Update access of <?=$i->type; ?>" class="icon">
                                        <i class="fe fe-edit"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
