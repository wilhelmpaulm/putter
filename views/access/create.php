<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('config/nav'); ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/access/add" method="post" enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Create New Access Role
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Password</label>
                                        <input type="password" name="password" class="form-control" required="true" placeholder="Enter your password to verify your intent">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">New User Access Type</label>
                                        <input type="text" id="address" name="type" required="" value="<?= $type; ?>" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Details</label>
                                <textarea class="form-control" name="details" required="" rows="2" placeholder="This will serve as the role description"><?= $details; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Update System Email</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
