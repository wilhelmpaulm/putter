<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('config/nav'); ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/config/club" method="post" enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Update <?=$type->type; ?> Access Rights
                    </h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th>Method</th>
                                <th width="1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($routeGroups as $key => $route): ?>
                            <tr>
                                <td class="">
                                    <div class="text-center tag">
                                        <?=$key; ?>
                                    </div>
                                </td>
                                <td>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="routes[]"
                                            value="<?=$key; ?>" checked="">
                                        <span class="custom-control-label"></span>
                                    </label>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Submit Updates</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom');?>
