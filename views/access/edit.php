<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('config/nav'); ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/access/<?= $id; ?>" method="POST" enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Update
                        <?= $type->type; ?> Access Rights
                    </h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th class="">Group</th>
                                <th>Details</th>
                                <th>URI</th>
                                <th>Method</th>
                                <th width="1">
                                    <label class="custom-control custom-checkbox">
                                        <input id="select-all" name="select-all" type="checkbox" class="custom-control-input" value="select-all">
                                        <span class="custom-control-label"></span>
                                    </label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($routes as $key => $route): ?>
                            <tr>
                                <td><span class="text-muted text-capitalize">
                                        <?= $route['base']; ?></span></td>
                                <td>
                                    <small class="text-title">
                                        <?= $route['details']; ?>
                                    </small>
                                </td>
                                <td>
                                    <a href="<?= $route['uri']; ?>" target="_">
                                        <small class="text-title">
                                            <?= $route['uri']; ?>
                                        </small>
                                    </a>
                                </td>
                                <td class="">
                                    <?php foreach ($route['methods'] as $key => $method): ?>
                                    <div class="text-center tag">
                                        <?= $method; ?>
                                    </div>
                                    <?php endforeach; ?>
                                </td>
                                <td>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input select" name="routes[]" <?= in_array($route['name'], $type->getList()) ? 'checked = "" ' : ''; ?>
                                        value="<?= $route['name']; ?>">
                                        <span class="custom-control-label"></span>
                                    </label>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <a href="<?= linkTo("access/{$id}/delete"); ?>"
                            title="Delete access role <?= $type->type; ?>"
                            class="btn btn-link text-muted"> Delete Access Role
                        </a>
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Submit Updates</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
