<html>
    <head>
        <title>Seats Confirmation</title>
    </head>
    <body bgcolor="#FFFFFF">
        <style>
            table{
                background:#fff;
            }
            body{
                background:#f1f1f1;
            }
        </style>
        <table width="600" border="0" cellpadding="0" cellspacing="0" style="margin:0 auto" border="2px solid #f1f1f1">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" width="600">
                        <tr>
                            <td width="302" height="83">
                                <img src="<?= linkPublic("pictures/logo.jpg")?>" alt="logo" style="display:block">
                            </td>
                            <td width="298" height="83" bgcolor="#fff">

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0"  width="600">
                        <tr>
                            <td>
                            <td width="40" height="56"></td>
                            <td width="520" height="56">
                                <span style="display:block; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color: #333">Hi, John Joe Rinks</span>
                            </td>    
                            <td width="40" height="56"></td>
                            </td>
                        </tr>
                    </table>
                </td>   
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0"  width="600">
                        <tr>
                            <td width="600" height="166">
                                <img src="<?= linkPublic("pictures/banner.jpg")?>" alt="heading-banner">
                            </td>
                        </tr>
                    </table>
                </td>   
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0"  width="600">
                        <tr>
                            <td width="36" height="413">                       
                            </td>
                            <td valign="top"> 
                                <table cellpadding="0" cellspacing="0" width="528">
                                    <tr>
                                        <td height="25"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span style="display:block; font-family:Arial, Helvetica, sans-serif; font-size:15px;">
                                                But first, Please confirm your registration within 10 days or you can copy
                                                paste the link below.
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellpadding="0">
                                                <tr>
                                                    <td width="219" height="70">
                                                        <a href="#" style="display:block; padding:0; margin:0">
                                                            <img src="<?= linkPublic("pictures/button.jpg")?>"  alt="confirm">
                                                        </a>
                                                    </td>
                                                    <td width="309" height="70"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a style="display:block; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#92263a">
                                                chrome://quick_start/content/index.html
                                                paste the link below.
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br />
                                            <span style="display:block; font-family:Arial, Helvetica, sans-serif; font-size:15px;">
                                                We look forwawrd to taking care of your needs.
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br />
                                            <span style="display:block; font-family:Arial, Helvetica, sans-serif; font-size:15px;">
                                                Love,<br /> The Seats Team
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="50">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellpadding="0">
                                                <tr>
                                                    <td width="27" height="27">
                                                        <a href="#" style="display:block; margin:0; padding:0">
                                                            <img src="<?= linkPublic("pictures/fb.jpg")?>" alt="">
                                                        </a>
                                                    </td>
                                                    <td width="27" height="27">
                                                    </td>
                                                    <td width="27" height="27">
                                                        <a href="#" style="display:block; margin:0; padding:0">
                                                            <img src="<?= linkPublic("pictures/gmail.jpg")?>" alt="">
                                                        </a>                                        	
                                                    </td>
                                                    <td width="27" height="27">
                                                    </td>
                                                    <td width="27" height="27">
                                                        <a href="#" style="display:block; margin:0; padding:0">
                                                            <img src="<?= linkPublic("pictures/twitter.jpg")?>" alt="">
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>               
                            </td>
                            <td width="36" height="413">                       
                            </td>
                        </tr>
                    </table>
                </td>   
            </tr>
        </table>
    </body>
</html>