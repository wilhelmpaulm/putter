<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('config/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/config/system" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Update Club Details
                    </h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="form-label">Google Maps API Key</label>
                        <input type="text" id="maps" name="maps" required="" value="<?= $system->maps ?>" class="form-control" placeholder="This will appear on pos">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Link to Weather Forecast</label>
                        <input type="url" id="maps" name="weather" required="" value="<?= $system->weather ?>" class="form-control" placeholder="This will appear on pos">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <div class="form-label">Favicon</div>
                                <img src="<?= File::get(Config::get('system', 'favicon'), 'app/uploads') ?>" style="margin-bottom: .5rem; height: 4rem" class="img-thumbnail" alt="Heade image">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" accept="image/*" value="<?= $system->favicon ?>" name="favicon">
                                    <label class="custom-file-label"><?= $system->favicon ?? 'Choose file' ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <div class="form-label">Header Image</div>
                                <img src="<?= File::get(Config::get('system', 'header'), 'app/uploads') ?>" style="margin-bottom: .5rem; height: 6rem" class="img-thumbnail" alt="Heade image">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" accept="image/*" value="<?= $system->header ?>" name="header">
                                    <label class="custom-file-label"><?= $system->header ?? 'Choose file' ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <div class="form-label">Logo</div>
                                <img src="<?= File::get(Config::get('system', 'logo'), 'app/uploads') ?>" style="margin-bottom: .5rem; height: 6rem" class="img-thumbnail" alt="image">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" accept="image/*" value="<?= $system->logo ?>" name="logo">
                                    <label class="custom-file-label"><?= $system->logo ?? 'Choose file' ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="custom-switch">
                            <input type="checkbox" name="mail" value="1" <?= $system->mail ? 'checked=""' : '' ?> class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Allow Email Sending</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="custom-switch">
                            <input type="checkbox" name="message" value="1" <?= $system->message ? 'checked=""' : '' ?> class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Allow Messaging and Notifications</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="custom-switch">
                            <input type="checkbox" name="seed" value="1" <?= $system->seed ? 'checked=""' : '' ?> class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Activate Seed Mode (switch to the seed db)</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="custom-switch">
                            <input type="checkbox" name="traffic" value="1" <?= $system->traffic ? 'checked=""' : '' ?> class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Track Site Traffic</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="custom-switch">
                            <input type="checkbox" name="debug" value="1" <?= $system->debug ? 'checked=""' : '' ?> class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Set System to Debug Mode</span>
                        </label>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Update System Details</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>