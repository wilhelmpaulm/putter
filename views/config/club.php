<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('config/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/config/club" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Update Club Details
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Club Name</label>
                                        <input type="text" id="name" name="name" required="" value="<?= $club->name ?>" class="form-control" placeholder="Club Name">
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="form-group">
                                        <label class="form-label">Website Title</label>
                                        <input type="text" id="title" name="title" required="" value="<?= $club->title ?>" class="form-control" placeholder="serve as your title bar text">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Description</label>
                                <textarea class="form-control" name="description" required="" rows="2" placeholder="This will serve as the long product description"><?= $club->description ?></textarea>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Mobile Number</label>
                                        <input type="text" id="mobile" name="mobile" required=""  value="<?= $club->mobile ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Latitude</label>
                                        <input type="text" id="latitude" name="latitude" required=""  value="<?= $club->latitude ?>" class="form-control" placeholder="0.0">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <div class="form-label">Longitude</div>
                                        <input type="text" id="longitude" name="longitude" required=""  value="<?= $club->longitude ?>" class="form-control" placeholder="0.0">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <div class="form-label">Country</div>
                                        <input type="text" id="country" name="country" required=""  value="<?= $club->country ?>" class="form-control" placeholder="0.0">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Street Address</label>
                                <textarea class="form-control" name="street" required="" rows="2" placeholder="Street address"><?= $club->street ?></textarea>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Barangay</label>
                                        <input type="text" id="barangay" name="barangay" required=""  value="<?= $club->barangay ?>" class="form-control" placeholder="0.0">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <div class="form-label">City</div>
                                        <input type="text" id="city" name="city" required=""  value="<?= $club->city ?>" class="form-control" placeholder="0.0">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <div class="form-label">Region</div>
                                        <input type="text" id="region" name="region" required=""  value="<?= $club->region ?>" class="form-control" placeholder="0.0">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <div class="form-label">Postal</div>
                                        <input type="text" id="postal" name="postal" required=""  value="<?= $club->postal ?>" class="form-control" placeholder="0.0">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Update Club Details</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>