<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('config/nav'); ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/config/limits" method="post" enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Update Site Default Values
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Teetime Limit</label>
                                        <p class="small text-muted">Set the number of unfinished teetimes a user could have</p>
                                        <input type="number" id="teetimes" name="teetimes" required="" value="<?= Config::get('limits', 'teetimes'); ?>" class="form-control" placeholder="10">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Tournament Limit</label>
                                        <p class="small text-muted">Set the number of unfinished tournaments a user could have</p>
                                        <input type="number" id="tournaments" name="tournaments" required="" value="<?= Config::get('limits', 'tournaments'); ?>" class="form-control" placeholder="10">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Tournament Slot Limit</label>
                                        <p class="small text-muted">Set the number of time slots a tournament could claim</p>
                                        <input type="number" id="tournaments_slot_limit" name="tournaments_slot_limit" required="" value="<?= Config::get('limits', 'tournaments_slot_limit'); ?>" class="form-control" placeholder="10">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Banquet Limit</label>
                                        <p class="small text-muted">Set the number of unfinished banquet reservation a user could have</p>
                                        <input type="number" id="banquets" name="banquets" required="" value="<?= Config::get('limits', 'banquets'); ?>" class="form-control" placeholder="10">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Banquet Pax Limit</label>
                                        <p class="small text-muted">Set the number of guests a banquet reservation could have</p>
                                        <input type="number" id="banquets_pax" name="banquets_pax" required="" value="<?= Config::get('limits', 'banquets_pax'); ?>" class="form-control" placeholder="10">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Reservation Limit</label>
                                        <p class="small text-muted">Set the number of unfinished reservations a user could have</p>
                                        <input type="number" id="reservations" name="reservations" required="" value="<?= Config::get('limits', 'reservations'); ?>" class="form-control" placeholder="10">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Reservation Slot Limit</label>
                                        <p class="small text-muted">Set the number of time slots a reservation could claim</p>
                                        <input type="number" id="reservations_slot_limit" name="reservations_slot_limit" required="" value="<?= Config::get('limits', 'reservations_slot_limit'); ?>" class="form-control" placeholder="10">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Reservation Pax Limit</label>
                                        <p class="small text-muted">Set the number of guests a reservation could have</p>
                                        <input type="number" id="reservations_pax" name="reservations_pax" required="" value="<?= Config::get('limits', 'reservations_pax'); ?>" class="form-control" placeholder="10">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Value Added Tax</label>
                                        <p class="small text-muted">Set the VAT multiplier that gets added to invoices</p>
                                        <input type="number" id="vat" name="vat" required="" value="<?= Config::get('limits', 'vat'); ?>" class="form-control" placeholder="10">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
