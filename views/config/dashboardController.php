<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('config/nav'); ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/config/dashboard/controller" method="post" enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Update Dashboard View
                    </h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="custom-switch">
                            <input type="checkbox" name="requests" value="1" <?= $dashboard->requests ? 'checked=""' : ''; ?> class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Show Daily Requests</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="custom-switch">
                            <input type="checkbox" name="revenue" value="1" <?= $dashboard->revenue ? 'checked=""' : ''; ?> class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Show Daily Revenue</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="custom-switch">
                            <input type="checkbox" name="invoices" value="1" <?= $dashboard->invoices ? 'checked=""' : ''; ?> class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Show Invoices For Today</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="custom-switch">
                            <input type="checkbox" name="today" value="1" <?= $dashboard->today ? 'checked=""' : ''; ?> class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Show Reservations For Today</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="custom-switch">
                            <input type="checkbox" name="today_pending" value="1" <?= $dashboard->today_pending ? 'checked=""' : ''; ?> class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Show Pending Reservations For Today</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="custom-switch">
                            <input type="checkbox" name="today_completed" value="1" <?= $dashboard->today_completed ? 'checked=""' : ''; ?> class="custom-switch-input">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Show Completed Reservations For Today</span>
                        </label>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Update Dashboard View</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
