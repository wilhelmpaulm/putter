<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h3 class="page-title">
                Dashboard
            </h3>
        </div>
    </div>
    <br>
    <div class="row row-cards">
        <?php if (Config::get('dashboard', 'requests')) : ?>
        <div class="col-6 col-sm-4 col-lg-2">
            <?php $officerCount = Request::getPendingCountBefore('officer'); ?>
            <div class="card">
                <div class="card-body p-3 text-center">
                    <?php if ($officerCount > 0) : ?>
                    <div class="text-right text-red help" title="Pending officer requests increased by <?= $officerCount; ?> today">
                        +
                        <?= $officerCount; ?>
                        <i class="fe fe-chevron-down"></i>
                    </div>
                    <?php elseif ($officerCount < 0) : ?>
                    <div class="text-right text-green help" title="Pending officer requests decreased by <?= $officerCount; ?> today">
                        -
                        <?= $officerCount; ?>
                        <i class="fe fe-chevron-up"></i>
                    </div>
                    <?php else : ?>
                    <div class="text-right text-blue help" title="No changes in pending requests">
                        <i class="fe fe-check"></i>
                    </div>
                    <?php endif; ?>
                    <div class="h1 m-0">
                        <?= Request::getPendingCount('officer'); ?>
                    </div>
                    <div class="text-muted mb-4">Officer Requests</div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php if (Config::get('dashboard', 'requests')) : ?>
        <div class="col-6 col-sm-4 col-lg-2">
            <?php $accountingCount = Request::getPendingCountBefore('accounting'); ?>
            <div class="card">
                <div class="card-body p-3 text-center">
                    <?php if ($accountingCount > 0) : ?>
                    <div class="text-right text-red help" title="Pending accounting requests increased by <?= $accountingCount; ?> today">
                        +
                        <?= $accountingCount; ?>
                        <i class="fe fe-chevron-down"></i>
                    </div>
                    <?php elseif ($accountingCount < 0) : ?>
                    <div class="text-right text-green help" title="Pending accounting requests decreased by <?= $accountingCount; ?> today">
                        -
                        <?= $accountingCount; ?>
                        <i class="fe fe-chevron-up"></i>
                    </div>
                    <?php else : ?>
                    <div class="text-right text-blue help" title="No changes in pending request count">
                        <i class="fe fe-check"></i>
                    </div>
                    <?php endif; ?>
                    <div class="h1 m-0">
                        <?= Request::getPendingCount('accounting'); ?>
                    </div>
                    <div class="text-muted mb-4">Accounting Requests</div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <div class="row row-cards">
        <?php if (Config::get('dashboard', 'revenue')) : ?>
        <div class="col-12 col-sm-6 col-lg-4">
            <?php $lessRevenue = Invoice::getRevenueBefore('officer'); ?>
            <div class="card">
                <div class="card-body p-3 text-center">
                    <?php if ($lessRevenue > 0) : ?>
                    <div class="text-right text-green help" title="Compared to yesterday revenue today has increased by PHP <?= number_format($lessRevenue, 2); ?>">
                        +
                        <?= $lessRevenue; ?>
                        <i class="fe fe-chevron-up"></i>
                    </div>
                    <?php elseif ($lessRevenue < 0) : ?>
                    <div class="text-right text-red help" title="Compared to yesterday revenue today has decreased by PHP <?= number_format($lessRevenue, 2); ?>">
                        -
                        <?= $lessRevenue; ?>
                        <i class="fe fe-chevron-down"></i>
                    </div>
                    <?php else : ?>
                    <div class="text-right text-blue help" title="No changes in revenue">
                        <i class="fe fe-check"></i>
                    </div>
                    <?php endif; ?>
                    <div class="h1 m-0">PHP
                        <?= Invoice::getRevenueToday(); ?>
                    </div>
                    <div class="text-muted mb-4">Revenue For Today</div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <div class="row row-cards">
        <?php if (Config::get('dashboard', 'invoices')) : ?>
        <div class="col-12 col-sm-4 col-lg-2">
            <?php $lessRevenue = Invoice::getPendingBefore(); ?>
            <div class="card">
                <div class="card-body p-3 text-center">
                    <?php if ($lessRevenue > 0) : ?>
                    <div class="text-right text-red help" title="Number of pending invoices increased by <?= $lessRevenue; ?>">
                        +
                        <?= $lessRevenue; ?>
                        <i class="fe fe-chevron-down"></i>
                    </div>
                    <?php elseif ($lessRevenue < 0) : ?>
                    <div class="text-right text-green help" title="Number of pending invoices decreased by <?= $lessRevenue; ?>">
                        -
                        <?= $lessRevenue; ?>
                        <i class="fe fe-chevron-up"></i>
                    </div>
                    <?php else : ?>
                    <div class="text-right text-blue help" title="No changes in number of pending invoices">
                        <i class="fe fe-check"></i>
                    </div>
                    <?php endif; ?>
                    <div class="h1 m-0">
                        <?= Invoice::getPending(); ?>
                    </div>
                    <div class="text-muted mb-4">Pending Invoices</div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php if (Config::get('dashboard', 'today')) : ?>
        <div class="col-12 col-sm-6 col-lg-4">
            <?php $lessRevenue = Invoice::getExpectedRevenueBefore('officer'); ?>
            <div class="card">
                <div class="card-body p-3 text-center">
                    <?php if ($lessRevenue > 0) : ?>
                    <div class="text-right text-green help" title="Compared to yesterday expected revenue today has increased by PHP <?= number_format($lessRevenue, 2); ?>">
                        +
                        <?= $lessRevenue; ?>
                        <i class="fe fe-chevron-up"></i>
                    </div>
                    <?php elseif ($lessRevenue < 0) : ?>
                    <div class="text-right text-red help" title="Compared to yesterday expected revenue today has decreased by PHP <?= number_format($lessRevenue, 2); ?>">
                        -
                        <?= $lessRevenue; ?>
                        <i class="fe fe-chevron-down"></i>
                    </div>
                    <?php else : ?>
                    <div class="text-right text-blue help" title="No changes in expected revenue">
                        <i class="fe fe-check"></i>
                    </div>
                    <?php endif; ?>
                    <div class="h1 m-0 text-muted">PHP
                        <?= Invoice::getExpectedRevenueToday(); ?>
                    </div>
                    <div class="text-muted mb-4">Pending Invoice Today</div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <?php if (Config::get('dashboard', 'today')) : ?>
    <div class="row row-cards">
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getReservationsCountToday('reservations'); ?>
                    </div>
                    <div class="text-muted mb-4">Reservations Today</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getReservationsCountToday('teetime'); ?>
                    </div>
                    <div class="text-muted mb-4">Tee Times Today</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getReservationsCountToday('tournament'); ?>
                    </div>
                    <div class="text-muted mb-4">Tournaments Today</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getReservationsCountToday('banquet'); ?>
                    </div>
                    <div class="text-muted mb-4">Banquets Today</div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if (Config::get('dashboard', 'today_pending')) : ?>
    <div class="row row-cards">
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getPendingReservationsCountToday('reservations'); ?>
                    </div>
                    <div class="text-muted mb-4">Pending Reservations Today</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getPendingReservationsCountToday('teetime'); ?>
                    </div>
                    <div class="text-muted mb-4">Pending Tee Times Today</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getPendingReservationsCountToday('tournament'); ?>
                    </div>
                    <div class="text-muted mb-4">Pending Tournaments Today</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getPendingReservationsCountToday('banquet'); ?>
                    </div>
                    <div class="text-muted mb-4">Pending Banquets Today</div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if (Config::get('dashboard', 'today_completed')) : ?>
    <div class="row row-cards">
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getCompletedReservationsCountToday('reservations'); ?>
                    </div>
                    <div class="text-muted mb-4">Completed Reservations Today</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getCompletedReservationsCountToday('teetime'); ?>
                    </div>
                    <div class="text-muted mb-4">Completed Tee Times Today</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getCompletedReservationsCountToday('tournament'); ?>
                    </div>
                    <div class="text-muted mb-4">Completed Tournaments Today</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-lg-2">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <div class="h1 m-0">
                        <?= Reservation::getCompletedReservationsCountToday('banquet'); ?>
                    </div>
                    <div class="text-muted mb-4">Completed Banquets Today</div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>
<?php include linkPage('template/_bottom'); ?>
