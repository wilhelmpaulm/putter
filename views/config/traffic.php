<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">
                Site Traffic Logs
            </h1>
            <br>
            <div class="card">
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th width="1">Log Date</th>
                                <th width="1">Device</th>
                                <th width='1'></th>
                                <th width='1'>Member</th>
                                <th>URI</th>
                                <th>Input</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($logs as $i): ?>
                            <tr>
                                <td class="text-nowrap">
                                    <div class="timeago text-muted" datetime="<?= $i->created_at; ?>">
                                        <?= $i->created_at; ?>
                                    </div>
                                    <small class=" text-muted">
                                        <?= $i->created_at; ?>
                                    </small>
                                </td>
                                <td class="">
                                    <small class="tag">
                                        <?= $i->device; ?>
                                    </small>
                                </td>
                                <?php if ($i->user_id): ?>
                                <td>
                                    <a href="<?= linkTo("members/{$i->user->id}"); ?>">
                                        <div class="avatar d-block avatar-<?= $i->user->color; ?>" <?= View::getBgStyle($i->user->image); ?>>
                                            <?= View::getAvatarName($i->user->image, $i->user->short); ?>
                                        </div>
                                    </a>
                                </td>
                                <td class="text-nowrap">
                                    <a href="<?= linkTo("members/{$i->user->id}"); ?>">
                                        <div class="text-capitalize">
                                            <?= $i->user->full_name; ?>
                                        </div>
                                    </a>
                                </td>
                                <?php else: ?>
                                <td class="text-nowrap">
                                </td>
                                <td class="text-nowrap">
                                    Guest
                                </td>
                                <?php endif; ?>
                                <td class="">
                                    <code class="small">
                                        <?= $i->uri; ?>
                                    </code>
                                </td>
                                <td class="">
                                    <textarea class="form-control" cols="14" rows="1"><?= $i->input; ?></textarea>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
