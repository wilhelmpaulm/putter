<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('config/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/config/system" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Update Accounting Defaults
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-label">Invoice Footer</label>
                                <p class="small text-muted">This will appear at the bottom of each invoice</p>
                                <textarea id="" class="form-control" name="invoice_footer" required="" rows="2" placeholder=""><?= Config::get('accounting', 'invoice_footer') ?></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Billing Statement Footer</label>
                                <p class="small text-muted">This will appear at the bottom of each billing statement invoice</p>
                                <textarea id="" class="form-control" name="billing_footer" required="" rows="2" placeholder=""><?= Config::get('accounting', 'billing_footer') ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Update Accounting Defaults</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>