<h3 class="page-title mb-5">System</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <?php if (in_array(user('type'), ['accounting', 'staff'])) : ?>
        <a href="<?= linkTo('config/dashboard'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-monitor"></i></span> Dashboard
        </a>
        <a href="<?= linkTo('config/dashboard/controller'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-monitor"></i></span> Update Dashboard View
        </a>
        <?php elseif (in_array(user('type'), ['officer', 'admin', 'administrator'])) : ?>
        <a href="<?= linkTo('config/dashboard'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-monitor"></i></span> Dashboard
        </a>
        <a href="<?= linkTo('config/dashboard/controller'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-sliders"></i></span> Update Dashboard View
        </a>
        <div class="dropdown-divider"></div>
        <a href="<?= linkTo('config/traffic'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-activity"></i></span> Site Traffic Logs
        </a>
        <div class="dropdown-divider"></div>
        <a href="<?= linkTo('access'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-users"></i></span> Manage User Access
        </a>
        <a href="<?= linkTo('access/add'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-user-plus"></i></span> Create User Access
        </a>
        <div class="dropdown-divider"></div>
        <a href="<?= linkTo('config/limits'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-sliders"></i></span> Site Default Values
        </a>
        <div class="dropdown-divider"></div>
        <a href="<?= linkTo('config/site'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-home"></i></span> Update Front Page
        </a>
        <a href="<?= linkTo('config/terms'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-clipboard"></i></span> Update Terms and Conditions
        </a>
        <a href="<?= linkTo('config/club'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-flag"></i></span> Club Details
        </a>
        <a href="<?= linkTo('config/system'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-cpu"></i></span> System Configuration
        </a>
        <a href="<?= linkTo('config/accounting'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-database"></i></span> Accounting Defaults
        </a>
        <a href="<?= linkTo('config/email'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-mail"></i></span> Email Account
        </a>
        <?php endif; ?>
    </div>
</div>
