<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('tickets/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/tickets/add" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Create a new Ticket
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Type</label>
                                        <select class="form-control text-capitalize reload" name="type" required="">
                                            <option value="" disabled selected>Please Select From the List</option>
                                            <?php foreach ($types as $type): ?>
                                                <option
                                                <?= Input::get('type') == $type ? 'selected=""' : '' ?>
                                                    value="<?= $type ?>">
                                                        <?= $type ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <?php if (Input::get('type')): ?>
                                <div class="form-group">
                                    <label class="form-label">Subject</label>
                                    <select name="target_id"  id="select-target" class="form-control custom-select">
                                        <?php foreach ($targets as $u): ?>
                                            <option
                                                <?= Input::get('target_id') == $u->id ? 'selected=""' : '' ?>
                                                data-data='{"image": "<?= File::get($u->image) ?>", "short": "<?= $u->short ?>", "color": "<?= $u->color ?>"}'
                                                value="<?= $u->id ?>">
                                                    <?= ucwords("{$u->name} ({$u->type})") ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Reason</label>
                                    <select class="form-control text-capitalize reload" name="title" required="">
                                        <option value="" disabled selected>Please Select From the List</option>
                                        <?php foreach ($reasons as $t): ?>
                                            <option
                                            <?= Input::get('title') == $t ? 'selected=""' : '' ?>
                                                value="<?= $t ?>">
                                                    <?= $t ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php endif; ?>
                            <?php if (Input::get('title')): ?>
                                <div class="form-group">
                                    <label class="form-label">Further Details</label>
                                    <textarea class="form-control" name="body" required="" rows="2" placeholder="This will serve as the long product description"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Password</label>
                                    <input type="password" name="password" class="form-control" required="true" placeholder="Enter your password to verify your intent">
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <?php if (Input::get('title')): ?>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        <?php endif; ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>
