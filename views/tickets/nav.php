<h3 class="page-title mb-5">Tickets</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo('tickets') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-clipboard"></i></span> View Tickets
        </a>
        <a href="<?= linkTo('tickets/add') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-plus"></i></span> Create a Ticket
        </a>
    </div>
</div>