<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('tickets/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Ticket #
                        <?= padId($item->id); ?>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <?php if ($item) : ?>
                            <table class="table table-hover table-bordered">
                                <tbody>
                                    <?php if ($item->target->image) : ?>
                                    <tr>
                                        <td></td>
                                        <td class="">
                                            <img src="<?= File::get($item->target->image ?? 'default.png'); ?>" class="img-thumbnail" style="width: 6rem" />
                                        </td>
                                    </tr>
                                    <?php endif; ?>
                                    <tr>
                                        <td>Target</td>
                                        <td>
                                            <?php if (in_array($item->type, ['member', 'staff'])) : ?>
                                                <a href="<?= linkTo("members/{$item->target->id}"); ?>">
                                            <?php elseif (in_array($item->type, ['facility', 'course'])) : ?>
                                                <a href="<?= linkTo("facilities/{$item->target->id}"); ?>">
                                            <?php else: ?>
                                                <a href="#">
                                            <?php endif; ?>
                                                <?= $item->target->name; ?>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Reason</td>
                                        <td>
                                            <?= $item->title; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Details</td>
                                        <td>
                                            <?= $item->body; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Type</td>
                                        <td>
                                            <div class="tag">
                                                <?= $item->type; ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>
                                            <div class="tag">
                                                <?= $item->status; ?>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <?php if ($item->status == 'pending') : ?>
                        <?php if ($item->user_id == user('id')) : ?>
                        <a href="<?= linkTo('tickets/'.$item->id.'/cancelled'); ?>" class="btn btn-warning"><i class="fe fe-trash"></i> Cancel Ticket</a>
                        <?php elseif (in_array(user('type'), ['officer', 'accounting'])) : ?>
                        <a href="<?= linkTo('tickets/'.$item->id.'/rejected'); ?>" class="btn btn-danger"><i class="fe fe-x"></i> Reject Ticket</a>
                        <a href="<?= linkTo('tickets/'.$item->id.'/resolved'); ?>" class="btn btn-success"><i class="fe fe-check"></i> Mark as Resolved</a>
                        <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <form class="card" action="<?= linkTo("tickets/$item->id/comments"); ?>" method="post" enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title">Comments for
                        <?= $item->target->name; ?>
                    </h3>
                </div>
                <?php if ($item->status == 'pending') : ?>
                <div class="card-body">
                    <label class="form-label">Add Comment</label>
                    <div class="input-group">
                        <input type="text" name="message" required="" class="form-control" placeholder="Message">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary">
                                <i class="fe fe-send"></i> Submit
                            </button>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <ul class="list-group card-list-group">
                    <?php if (!count($item->comments)) : ?>
                    <li class="list-group-item text-center">
                        <h5> No Comments</h5>
                    </li>
                    <?php endif; ?>
                    <?php foreach ($item->comments as $c) : ?>
                    <li class="list-group-item ">
                        <div class="media">
                            <div class="media-object avatar avatar-md avatar-<?= $c->user->color; ?> mr-4" style="background-image: url(demo/faces/male/16.jpg)">
                                <?= $c->user->short; ?>
                            </div>
                            <div class="media-body">
                                <div class="media-heading">
                                    <small class="float-right text-muted timeago" datetime="<?= $c->created_at; ?>"></small>
                                    <h5 class="text-capitalize">
                                        <?= $c->user->first_name.' '.$c->user->last_name; ?>
                                    </h5>
                                </div>
                                <div class="">
                                    <?= $c->message; ?>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </form>

        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
