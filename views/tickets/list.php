<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('tickets/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">View Tickets</h3>
                </div>
                <table class="table card-table table-vcenter dataTable">
                    <thead>
                        <tr>
                            <th class="w-1">ID.</th>
                            <th>Date Prepared</th>
                            <?php if (in_array(user('type'), ['accounting', 'officer', 'staff'])) : ?>
                            <th width='1'></th>
                            <th width='1'>User</th>
                            <?php endif; ?>
                            <th width='1'></th>
                            <th width='1'>Target</th>
                            <th>Reason</th>
                            <th width='1'>Status</th>
                            <th width="1"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($items as $i) : ?>
                        <tr>
                            <td><a href="/tickets/<?= $i->id; ?>" class="text-muted small">
                                    <?= padId($i->id); ?></a></td>
                            <td class="">
                                <div class="text-center small">
                                    <?= (new DateTime($i->created_at))->format('F j, Y, g:i A'); ?>
                                </div>
                            </td>
                            <?php if (in_array(user('type'), ['accounting', 'officer', 'staff'])) : ?>
                            <?php if (in_array($i->type, ['member', 'staff'])) : ?>
                            <td>
                                <a href="<?= linkTo("members/{$i->user->id}"); ?>">
                                    <div class="avatar d-block avatar-<?= $i->user->color; ?>" <?= View::getBgStyle($i->user->image); ?>>
                                        <?= View::getAvatarName($i->user->image, $i->user->short); ?>
                                    </div>
                                </a>
                            </td>
                            <td class="text-nowrap">
                                <a href="<?= linkTo("members/{$i->user->id}"); ?>">
                                    <div class="text-capitalize">
                                        <?= $i->user->full_name; ?>
                                    </div>
                                </a>
                            </td>
                            <?php elseif (in_array($i->type, ['facility', 'course'])) : ?>
                            <td>
                                <a href="<?= linkTo("facilities/{$i->target->id}"); ?>">
                                    <div class="avatar d-block avatar-<?= $i->target->color; ?>" <?= View::getBgStyle($i->target->image); ?>>
                                        <?= View::getAvatarName($i->target->image, $i->target->short); ?>
                                    </div>
                                </a>
                            </td>
                            <td class="text-nowrap">
                                <a href="<?= linkTo('facilities'); ?>">
                                    <div class="text-capitalize">
                                        <?= $i->target->name; ?>
                                    </div>
                                </a>
                            </td>
                            <?php endif; ?>
                            <?php endif; ?>
                            <td>
                                <a href="<?= linkTo("members/{$i->target->id}"); ?>">
                                    <div class="avatar d-block avatar-<?= $i->target->color; ?>" <?= View::getBgStyle($i->target->image); ?>>
                                        <?= View::getAvatarName($i->target->image, $i->target->short); ?>
                                    </div>
                                </a>
                            </td>
                            <td class="text-nowrap">
                                <a href="<?= linkTo("members/{$i->target->id}"); ?>">
                                    <div class="text-capitalize">
                                        <?= $i->target->full_name; ?>
                                    </div>
                                </a>
                            </td>
                            <td class="">
                                <div class="small">
                                    <?= $i->title; ?>
                                </div>
                            </td>
                            <td class="text-nowrap">
                                <span class="tag">
                                    <?= $i->status; ?>
                                </span>
                            </td>
                            <?php if (in_array(user('type'), ['officer', 'accounting', 'staff', 'maintenance'])) : ?>
                            <td class="text-right">
                                <div class="item-action dropdown show">
                                    <div class="item-action dropdown">
                                        <a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-menu"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <?php if ($i->status == 'pending') : ?>
                                            <?php if ($i->user_id == user('id')) : ?>
                                            <a href="<?= linkTo('tickets/'.$i->id.'/cancelled'); ?>" class="dropdown-item"><i class="dropdown-icon fe fe-trash"></i> Cancel Ticket</a>
                                            <?php else : ?>
                                            <a href="<?= linkTo('tickets/'.$i->id.'/resolved'); ?>" class="dropdown-item"><i class="dropdown-icon fe fe-check"></i> Mark as Resolved</a>
                                            <a href="<?= linkTo('tickets/'.$i->id.'/rejected'); ?>" class="dropdown-item"><i class="dropdown-icon fe fe-x"></i> Reject Ticket</a>
                                            <?php endif; ?>
                                            <div class="dropdown-divider"></div>
                                            <?php endif; ?>
                                            <a href="<?= linkTo('tickets/'.$i->id); ?>" class="dropdown-item"><i class="dropdown-icon fe fe-external-link"></i> View Ticket</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <?php elseif ($i->user_id == user('id')) : ?>
                            <td class="text-right">
                                <div class="item-action dropdown show">
                                    <div class="item-action dropdown">
                                        <a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-menu"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <?php if ($i->status == 'pending') : ?>
                                            <a href="<?= linkTo('tickets/'.$i->id.'/cancelled'); ?>" class="dropdown-item"><i class="dropdown-icon fe fe-trash"></i> Cancel Ticket</a>
                                            <div class="dropdown-divider"></div>
                                            <?php endif; ?>
                                            <a href="<?= linkTo('tickets/'.$i->id); ?>" class="dropdown-item"><i class="dropdown-icon fe fe-external-link"></i> View Ticket</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <?php endif; ?>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
