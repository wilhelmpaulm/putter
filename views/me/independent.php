<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col col-login mx-auto">
            <form class="card" action="/me/independent" method="post">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Request to Be an Independent Member
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <input type="password" name="password" class="form-control" required="true" placeholder="Enter your password to complete this action">
                                <ul>
                                    <li>
                                        <small class="form-text">
                                            Requests of dependent members to become independent would entail
                                            that the user would be subject to reviews similar to new applicants.
                                        </small>
                                    </li>
                                    <li>
                                        <small class="form-text">
                                            An officer would first approve to review your membership, send a requested
                                            to other members to verify your good behavior, schedule an interview, and
                                            finally approve your request.
                                        </small>
                                    </li>
                                    <li>
                                        <small class="form-text">
                                            Please be note that this request cannot be canceled while it is being processed.
                                        </small>
                                    </li>
                                </ul>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="agree" value="1" class="custom-switch-input agree">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">I agree with <a href='/terms'>terms and conditions</a></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="">
                        <button type="submit" disabled="" class="btn btn-primary btn-block needAgree">Send request</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php include linkPage('template/_bottom') ?>