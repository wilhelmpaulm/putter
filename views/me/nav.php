<h3 class="page-title mb-5">Profile and Settings</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo('me') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-user"></i></span>Profile
        </a>
        <a href="<?= linkTo('me/settings') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-sliders"></i></span>Settings 
        </a>
        <a href="<?= linkTo('me/password') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-lock"></i></span>Change Password
        </a>
        <?php if (user('type') == 'member'): ?>
            <a href="<?= linkTo('me/cancel') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
                <span class="icon mr-3"><i class="fe fe-x"></i></span>Cancel Membership
            </a>
        <?php endif; ?>
    </div>
    <!--    
        <div class="mt-6">
            <a href="#" class="btn btn-secondary btn-block">Compose new Email</a>
        </div>
    -->
</div>