<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('me/nav') ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">My Requests</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap dataTable">
                        <thead>
                            <tr>
                                <th class="w-1">ID.</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Date Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($requests as $r): ?>
                                <tr>
                                    <td><span class="text-muted"><?= padId($r->id) ?></span></td>
                                    <td class="text-capitalize"><?= str_replace('_', ' ', $r->request) ?></td>
                                    <td class="text-capitalize"><?= $r->status ?></td>
                                    <td>
                                        <div><?= $r->date_created ?></div>
                                        <div class="timeago text-capitalize" datetime="<?= $r->created_at ?>"></div>
                                        <div class="small text-muted">
                                            <?= $r->created_at ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>