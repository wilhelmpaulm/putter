<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('me/nav') ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Settings
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="active"
                                           class="custom-switch-input notifications-toggle-status"
                                           data-user_id="<?= user('id') ?>"
                                           <?php if (user('notifications')): ?> 
                                               checked="" 
                                               data-status="deactivate"
                                           <?php else: ?>
                                               data-status="activate"
                                           <?php endif; ?>
                                           value="1"> 
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Receive system notifications</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="active"
                                           class="custom-switch-input messages-toggle-status"
                                           data-user_id="<?= user('id') ?>"
                                           <?php if (user('messages')): ?> 
                                               checked="" 
                                               data-status="deactivate"
                                           <?php else: ?>
                                               data-status="activate"
                                           <?php endif; ?>
                                           value="1"> 
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Receive messages from other users</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="active"
                                           class="custom-switch-input emails-toggle-status"
                                           data-user_id="<?= user('id') ?>"
                                           <?php if (user('emails')): ?> 
                                               checked="" 
                                               data-status="deactivate"
                                           <?php else: ?>
                                               data-status="activate"
                                           <?php endif; ?>
                                           value="1"> 
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Receive emails from the system</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>