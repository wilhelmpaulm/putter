<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('me/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/me/cancel" method="post">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Request To Cancel Membership
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <input type="password" name="password" class="form-control" required="true" placeholder="Enter your password to verify your intent">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Reason for Cancellation</label>
                                <textarea class="form-control" name="reason" rows="5" placeholder="Please provide the details of your intent to cancel"></textarea>
                                <ul>
                                    <li>
                                        <small class="form-text">
                                            Canceling your membership would also invalidate the membership of your dependents.
                                        </small>
                                    </li>
                                    <li>
                                        <small class="form-text">
                                            Please take note that membership cancellation would be
                                            validated by an officer or the club and the accounting department.
                                        </small>
                                    </li>
                                    <li>
                                        <small class="form-text">
                                            Consider chatting with a club officer before finalizing your request.
                                        </small>
                                    </li>
                                </ul>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="agree" value="1" class="custom-switch-input agree">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">I agree with <a href='/terms'>terms and conditions</a></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" disabled="" class="btn btn-primary needAgree">Send request</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>