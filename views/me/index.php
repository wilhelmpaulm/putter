<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('me/nav') ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        <?= padId($member->id) ?>
                        <span class="small text-muted">
                            - <?= $member->first_name ?> <?= $member->last_name ?>
                        </span>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <div class="form-group">
                                        <span class="avatar avatar-xxl avatar-<?= $member->color ?>" style="background-image: url(<?= File::get($member->image) ?>)">
                                            <?= File::get($member->image) ? '' : $member->short ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($member->sponsor_id || $member->endorser_id): ?>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <?php if ($member->sponsor_id || $member->endorser_id): ?>
                                            <b>Sponsor & Endorser: </b>
                                        <?php else: ?>
                                            <b>Endorser: </b>
                                        <?php endif; ?>
                                        <a href="/members/<?= $member->sponsor->id ?>" class="btn btn-link btn-lg text-capitalize">
                                            <?= $member->sponsor->full_name ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="border my-3" style="width: 100%"></div>
                        <?php endif; ?>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Account Type</label>
                                        <input type="type" readonly="" name="type" required="" autocomplete="false" class="form-control" placeholder="Email" value="<?= $member->type ?>">
                                    </div>
                                </div>
                                <?php if (in_array(user('type'), ['member', 'guest'])): ?>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="form-group">
                                            <label class="form-label">Current Balance</label>
                                            <input type="text" readonly="" name="balance" required="" class="form-control" value="PHP <?= $member->balance ?>">
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="border my-3" style="width: 100%"></div>
                        <?php if (user('company')): ?>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-sm-6 col-md-4">
                                        <div class="form-group">
                                            <label class="form-label">Company</label>
                                            <input type="type" readonly="" name="type" required="" autocomplete="false" class="form-control" placeholder="Email" value="<?= $member->company ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="form-group">
                                            <label class="form-label">Occupation</label>
                                            <input type="text" readonly="" name="balance" required="" class="form-control" value="<?= $member->occupation ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="border my-3" style="width: 100%"></div>
                        <?php endif; ?>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Email address</label>
                                        <input type="email" readonly="" name="email" required="" autocomplete="false" class="form-control" placeholder="Email" value="<?= $member->email ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Gender</label>
                                        <input type="text" readonly="" name="last_name" required="" class="form-control" value="<?= $member->gender ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">First Name</label>
                                        <input type="text" readonly="" name="first_name" required="" class="form-control" placeholder="Company" value="<?= $member->first_name ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Middle Name</label>
                                        <input type="text" readonly="" name="middle_name" class="form-control" placeholder="Company" value="<?= $member->middle_name ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Last Name</label>
                                        <input type="text" readonly="" name="last_name" required="" class="form-control" placeholder="Last Name" value="<?= $member->last_name ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Mobile</label>
                                        <input type="text"readonly="" name="mobile" required="" class="form-control" <input type="text" readonly="" name="last_name" required="" class="form-control" placeholder="Last Name" value="<?= $member->mobile ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Address</label>
                                        <input type="text"readonly="" name="address" required="" class="form-control" placeholder="Home Address" <input type="text" readonly="" name="last_name" required="" class="form-control" placeholder="Last Name" value="<?= $member->address ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">City</label>
                                        <input type="text"readonly="" name="city" required="" class="form-control" placeholder="City" <input type="text" readonly="" name="last_name" required="" class="form-control" placeholder="Last Name" value="<?= $member->city ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="form-label">Zip Code</label>
                                        <input type="number"readonly="" name="zip_code" required="" class="form-control" <input type="text" readonly="" name="last_name" required="" class="form-control" placeholder="Last Name" value="<?= $member->zip_code ?>">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="form-label">Country</label>
                                        <input type="text"readonly="" name="country" required="" class="form-control" placeholder="Philippines" value="<?= $member->country ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-label">Background</label>
                                <textarea rows="5"readonly="" name="intent" required="" class="form-control" placeholder="Give a brief explanation of your intentions in the club and how you found out about us."><?= $member->details ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-label">Why do you want to join the club?</label>
                                <textarea rows="5"readonly="" name="intent" required="" class="form-control" placeholder="Give a brief explanation of your intentions in the club and how you found out about us." value="Mike"><?= $member->intent ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include linkPage('template/_bottom') ?>