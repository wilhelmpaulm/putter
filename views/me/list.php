<!doctype html>
<html lang="en" dir="ltr">
    <?php include linkPage("template/head"); ?>
    <body class="">
        <div class="page">
            <div class="page-main">
                <?php include linkPage('template/title') ?>
                <?php include linkPage('template/navbar') ?>    
                <div class="my-3 my-md-5">
                    <div class="container">
                        <div class="page-header">
                            <h1 class="page-title">
                                <?= ucfirst($title) ?>
                            </h1>
                        </div>
                        <div class="row row-cards row-deck">
                            <?php foreach ($members as $m): ?>
                                <div class="col-lg-6">
                                    <div class="card card-aside">
                                        <a href="#" class="card-aside-column text-center">
                                            <span class="avatar avatar-xxl avatar-<?= $m->color ?>" style="margin-top: 1.5em; background-image: url(<?= $m->image ? File::get($m->image) : $m->short ?>);"><?= $m->short ?></span>
                                        </a>
                                        <div class="card-body d-flex flex-column">
                                            <h4><a href="#"><?= ucwords("$m->first_name $m->middle_name $m->last_name") ?> </a></h4>
                                            <small class="text-muted"><?= $m->created_at ?></small>
                                            <br>
                                            <div class="">Bio</div>
                                            <small class="text-muted"><?= $m->intent ?></small>
                                            <br>
                                            <div class="">Details</div>
                                            <small class="text-muted"><?= $m->details ?></small>
                                            <div class="d-flex align-items-center pt-5 mt-auto">
                                                <div>
                                                    <a href="./profile.html" class="text-default"><?= ucfirst($m->type) ?></a>
                                                    <small class="d-block text-muted">Email: <?= $m->email ?></small>
                                                    <small class="d-block text-muted">Mobile: <?= $m->mobile ?></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>
            </div>

            <?php include linkPage("template/footer"); ?>
    </body>
</html>