<h3 class="page-title mb-5">Accounting</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo('invoices') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-shopping-bag"></i></span> View Invoices
        </a>
        <a href="<?= linkTo('billings') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-file-text"></i></span> View Billing Statements
        </a>
        <?php if (in_array(user('type'), ['officer', 'accounting'])): ?>
            <div class="dropdown-divider"></div>
            <a href="<?= linkTo('accounting/pos') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
                <span class="icon mr-3"><i class="fe fe-shopping-cart"></i></span> Point of Sale
            </a>
            <div class="dropdown-divider"></div>
            <a href="<?= linkTo('accounting/members') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
                <span class="icon mr-3"><i class="fe fe-database"></i></span> Member Balance Overview
            </a>
            <a href="<?= linkTo('accounting/adjust') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
                <span class="icon mr-3"><i class="fe fe-edit"></i></span> Adjust Member Balance
            </a>
            <a href="<?= linkTo('billings/generate') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
                <span class="icon mr-3"><i class="fe fe-send"></i></span> Generate Billing Statement
            </a>
        <?php endif; ?>
    </div>
</div>