<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('accounting/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Member Balance Overview</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap dataTable">
                        <thead>
                            <tr>
                                <th class="w-1">ID.</th>
                                <th width="5">User</th>
                                <th></th>
                                <th>Current Balance</th>
                                <th width="1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($members as $m): ?>
                            <tr>
                                <td><span class="text-muted small">
                                        <?= padId($m->id); ?></span></td>
                                <td>
                                    <a href="<?= linkTo("members/{$m->id}"); ?>">
                                        <div class="avatar d-block avatar-<?= $m->color; ?>" <?= View::getBgStyle($m->image); ?>>
                                            <?= View::getAvatarName($m->image, $m->short); ?>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?= linkTo("members/{$m->id}"); ?>">
                                        <div class="text-capitalize">
                                            <?= $m->full_name; ?>
                                        </div>
                                    </a>
                                    <div class="text-muted small">
                                        Member since: <span class="timeago" datetime="<?= $m->created_at; ?>"></span>
                                    </div>
                                </td>
                                <td class="">PHP
                                    <?= $m->balance; ?>
                                </td>
                                <td>
                                    <a href="<?= linkTo("accounting/adjust?user_id={$m->id}"); ?>"
                                        title="Adjust
                                        <?= $m->first_name; ?>'s balance"
                                        class="icon">
                                        <i class="fe fe-edit"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
