<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('accounting/nav'); ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/accounting/adjust" method="post">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Adjust Member Balance
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <input type="password" name="password" class="form-control" required="true" placeholder="Enter your password to verify your intent">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Users list</label>
                                <select name="user_id" id="select-user" class="form-control custom-select">
                                    <?php foreach ($members as $u): ?>
                                        <option
                                            data-data='{"image": "<?= File::get($u->image); ?>", "short": "<?= $u->short; ?>", "color": "<?= $u->color; ?>"}'
                                            <?= $u->id == (int) Input::get('user_id') ? 'selected' : ''; ?>
                                            value="<?= $u->id; ?>">
                                                <?=ucwords($u->full_name.' ('.$u->type.')'); ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <?php if ($member): ?>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="form-label">Current Balance</label>
                                            <input type="number" id="current_balance" readonly="" class="form-control" value="<?= $member->balance; ?>">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="form-label">Amount</label>
                                            <input type="number" id="amount" name="amount" min="-999999999999999" step="any" class="form-control update-balance" required="true" placeholder="Please make sure to add + or -." value="0.0">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="form-label">Final Balance</label>
                                            <input type="number" id="final_balance" min="-99999999999" readonly="" class="form-control" value="<?= $member->balance; ?>">
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label class="form-label">Reason for Adjusting Member's Balance</label>
                                <textarea class="form-control" name="reason" rows="5" placeholder="Please provide the details of your intent to update member's balance"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="agree" value="1" class="custom-switch-input agree">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">I agree with <a href='/terms'>terms and conditions</a></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" disabled="" class="btn btn-primary needAgree">Adjust Balance</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
