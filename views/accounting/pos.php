<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-6 d-print-none">
            <form class="card" action="/accounting/pos" method="post">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Point of Sale System
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-label">Select Member</label>
                                <select name="user_id" id="select-reload" class="form-control custom-select">
                                    <?php foreach ($members as $u): ?>
                                    <option
                                        data-data='{"image": "<?=File::get($u->image); ?>", "short": "<?=$u->short; ?>", "color": "<?=$u->color; ?>"}'
                                        <?=$u->id == (int) Input::get('user_id') ? 'selected' : ''; ?>
                                        value="<?=$u->id; ?>">
                                        <?=ucwords($u->full_name.' ('.$u->type.')'); ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <?php if ($member): ?>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Current Balance</label>
                                        <input type="number" id="current_balance" readonly="" class="form-control"
                                            value="<?=$member->balance; ?>">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Amount</label>
                                        <input type="number" id="amount" readonly="" name="amount"
                                            min="-999999999999999" step="any" class="form-control update-balance"
                                            required="true" value="<?=$item->total ?? 0; ?>">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Final Balance</label>
                                        <input type="number" id="final_balance" min="-99999999999" readonly=""
                                            class="form-control" value="<?=$member->balance; ?>">
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php if ($member): ?>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Submit Sale</button>
                    </div>
                </div>
                <div class="border my-3" style="width: 100%"></div>
                <div class="table-responsive">
                    <table class="table card-table table-striped table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th width="1"></th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>QTY</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($items as $i): ?>
                            <tr>
                                <td class="">
                                    <a href="<?=File::get($i->image); ?>" target="_blank">
                                        <img src="<?=File::get($i->image ?? 'default.png'); ?>" style="max-width: 3.5rem"
                                            alt="..." class="img-thumbnail">
                                    </a>
                                </td>
                                <td title="<?=$i->details; ?>">
                                    <div class="">
                                        <?=$i->name; ?>
                                    </div>
                                    <div class="small text-muted">
                                        - Amount Left: <?=$i->amount; ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="text-nowrap">
                                        PHP <?=number_format($i->current_price, 2); ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="text-nowrap amount">
                                        <input type="number" id="amount" name="inventory_id[<?=$i->id; ?>]" min="0"
                                            step="1" max="<?=$i->amount; ?>" class="form-control reload" required="true"
                                            value="<?=Input::get('inventory_id')[$i->id] ?? 0; ?>">
                                    </div>
                                </td>
                                <!--<td class="w-1"><a class="icon pos-remove"><i class="fe fe-trash"></i></a></td>-->
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <?php endif; ?>
            </form>
        </div>
        <?php if ($item && $member): ?>
        <div class="col-12 col-md-6">
            <div class="container" style="">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">#<?=padId($item->id); ?></h3>
                        <div class="card-options">
                            <button type="button" class="btn btn-primary" onclick="javascript:window.print();"><i
                                    class="fe fe-printer"></i> Print Invoice</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row my-8">
                            <div class="col-6">
                                <p class="h3"><?=Config::get('club', 'name'); ?></p>
                                <address>
                                    <?=Config::get('club', 'street'); ?>,
                                    <?=Config::get('club', 'city'); ?>, <?=Config::get('club', 'region'); ?>,
                                    <?=Config::get('club')->postal; ?><br>
                                    <?=Config::get('email', 'address'); ?>
                                </address>
                            </div>
                            <div class="col-6 text-right">
                                <p class="h3"><?=$item->payer->full_name; ?></p>
                                <address>
                                    <?=$item->payer->address; ?><br>
                                    <?=$item->payer->email; ?>
                                </address>
                            </div>
                        </div>
                        <div class="table-responsive push">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <td colspan="2" class="text-center">Invoice ID #<?=padId($item->id); ?></td>
                                    <td colspan="3" class="text-center text-nowrap">Prepared at <?=$item->created_at; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center" style="width: 1%"></th>
                                    <th>Product</th>
                                    <th class="text-center" style="width: 1%">Qnt</th>
                                    <th class="text-right" style="width: 1%">Unit</th>
                                    <th class="text-right" style="width: 1%">Price</th>
                                </tr>
                                <?php foreach ($item->items as $i): ?>
                                <tr>
                                    <td class="text-center">#<?=padId($i->id); ?></td>
                                    <td>
                                        <p class="font-w600 mb-1"><?=$i->name; ?></p>
                                        <?php if ($i->label): ?>
                                        <div class="text-muted small">- <?=$i->label; ?></div>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?=$i->amount; ?>
                                    </td>
                                    <td class="text-right text-nowrap">PHP <?=number_format($i->price, 2); ?></td>
                                    <td class="text-right text-nowrap">PHP
                                        <?=number_format($i->price * $i->amount, 2); ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right"></td>
                                    <td colspan="2" class="font-w600 text-right">Subtotal</td>
                                    <td class="text-right">PHP <?=number_format($item->subtotal, 2); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right"></td>
                                    <td colspan="2" class="font-w600 text-right">Vat Rate</td>
                                    <td class="text-right"><?=$item->vat; ?>%</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right"></td>
                                    <td colspan="2" class="font-w600 text-right">Vat Due</td>
                                    <td class="text-right">PHP
                                        <?=number_format($item->subtotal && $item->vat ? $item->subtotal * ($item->vat / 100) : 0.0, 2); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right"></td>
                                    <td colspan="2" class="font-weight-bold text-uppercase text-right">Total</td>
                                    <td class=" text-right text-nowrap">PHP <?=number_format($item->total * 1.0, 2); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center"></td>
                                    <td colspan="3" class="text-center">
                                        <strong class="font-weight-bold text-uppercase">
                                            Invoice Pending
                                        </strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <p class="text-muted text-center"><?=Config::get('accounting', 'invoice_footer'); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
