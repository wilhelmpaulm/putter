<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('blogs/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/blogs/add" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Create a New Blog Post
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-label">Title of the Post</label>
                                <input type="text" id="name" name="name" required="" class="form-control" placeholder="This will appear on pos">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Summary or Hook</label>
                                <input type="text" id="label" name="label" required="" class="form-control" placeholder="This will serve as the product identifier">
                                <ul>
                                    <li>
                                        <small class="form-text">
                                            This will be shown on the item card as the description.
                                        </small>
                                    </li>
                                    <li>
                                        <small class="form-text">
                                            Make sure to keep this catchy to get more attention.
                                        </small>
                                    </li>
                                </ul>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <div class="form-label">Header Image</div>
                                        <img src="<?= File::get('default.png') ?>" style="margin-bottom: .5rem; height: 6rem" class="img-thumbnail" alt="Heade image">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" accept="image/*" required="" name="image">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="height: 30rem; padding-bottom: 1.5rem">
                                <label class="form-label">Body</label>
                                <textarea id="jodit" class="form-control" name="body" required="" rows="10" placeholder="This will serve as the long product description"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tags</label>
                                <input type="text" class="form-control" name="tags" id="input-tags" value="blog, club">
                            </div>
                            <?php if (in_array(user('type'), ['officer', 'staff', 'accounting'])): ?>
                                <div class="form-group">
                                    <label class="custom-switch">
                                        <input type="checkbox" name="news" value="1" checked="" class="custom-switch-input">
                                        <span class="custom-switch-indicator"></span>
                                        <span class="custom-switch-description">This is a news post</span>
                                    </label>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="public" value="1" checked="" class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Make this available to the public</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>