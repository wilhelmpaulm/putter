<h3 class="page-title mb-5">Blog Posts</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo('blogs') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-package"></i></span> My Blog Posts
        </a>
        <a href="<?= linkTo('blogs/add') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-plus"></i></span> Create a New Blog Post
        </a>
    </div>
</div>