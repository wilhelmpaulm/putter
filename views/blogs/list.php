<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('blogs/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">View Blog Posts</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th class="w-1">ID.</th>
                                <th width='1'>Banner</th>
                                <th >Summary</th>
                                <th width='20%'>Tags</th>
                                <th width='1'>Type</th>
                                <th width='1'>Views</th>
                                <th width="1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($items as $i): ?>
                                <tr>
                                    <td><span class="text-muted small"><?= padId($i->id); ?></span></td>
                                    <td class="text-center">
                                        <a href="<?= File::get($i->image); ?>" target="_blank">
                                            <img src="<?= File::get($i->image ?? 'default.png'); ?>" alt="..." class="img-thumbnail">
                                        </a>
                                    </td>
                                    <td class="">
                                        <a href="<?= linkTo("blogs/{$i->id}"); ?>">
                                            <strong class="text-capitalize text-nowrap">
                                                <?= $i->name; ?>
                                            </strong>
                                        </a>
                                        <div class="small">
                                            <?= $i->label; ?>
                                        </div>
                                    </td>
                                    <td class="">
                                        <?php foreach ($i->tag_list as $t): ?>
                                            <div class="text-center tag">
                                                <?= $t; ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </td>
                                    <td class="">
                                        <div class="text-center tag">
                                            <?= $i->type; ?>
                                        </div>
                                    </td>
                                    <td class="">
                                        <div class="text-center">
                                            <?= $i->views; ?>
                                        </div>
                                    </td>
                                    <td>
                                        <a  href="<?= linkTo("blogs/{$i->id}"); ?>"
                                            title="Update details of <?= $i->name; ?>"
                                            class="icon">
                                            <i class="fe fe-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
