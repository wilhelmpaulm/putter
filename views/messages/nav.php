<h3 class="page-title mb-5">Notifs and Messages</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo('messages/system') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-cpu"></i></span> System Notifications 
        </a>
        <a href="<?= linkTo('messages/unread') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-eye-off"></i></span> Unread Messages
        </a>
        <a href="<?= linkTo('messages/read') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-eye"></i></span> Read Messages
        </a>
        <a href="<?= linkTo('messages/send') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-send"></i></span> Send a Message
        </a>
    </div>
</div>