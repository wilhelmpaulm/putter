<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('messages/nav') ?>
        </div>
        <div class="col-md-9">
            <form action="/messages/send" class="card" method="post">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Send a Message
                    </h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="form-label">Users list</label>
                        <select name="sent_to[]"  multiple="" id="select-users" class="form-control custom-select">
                            <?php foreach (User::where('active', 1)->where('messages', 1)->get() as $u): ?>
                                <option
                                <?= $u->id == (int)Input::get('user_id') ? 'selected' : '' ?>
                                    data-data='{"image": "<?= File::get($u->image) ?>", "short": "<?= $u->short ?>", "color": "<?= $u->color ?>"}'
                                    value="<?= $u->id ?>">
                                        <?= ucwords($u->full_name) ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Message</label>
                        <textarea class="form-control" name="message" rows="5" placeholder="Big belly rude boy, million dollar hustler. Unemployed."></textarea>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="reset" class="btn btn-link">Clear Form</button>
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php include linkPage('template/_bottom') ?>
