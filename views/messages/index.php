<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('messages/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <?= ucfirst($status); ?> Messages</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>

                                <th width="15%">Date Created</th>
                                <th width="1"></th>
                                <th width="1">User</th>
                                <th>Message</th>
                                <th width="1">
                                </th>
                                <th width="1">
                                    <?php if ($status != 'read' && count($messages)): ?>
                                    <a href="<?= linkTo('messages/all/read'); ?>"
                                        title="mark all messages as read"
                                        class="icon">
                                        <i class="fe fe-trash"></i>
                                    </a>
                                    <?php endif; ?>
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($messages as $m): ?>
                            <tr>
                                <td>
                                    <div>
                                        <?= $m->date_created; ?>
                                    </div>
                                    <small class="timeago text-capitalize" datetime="<?= $m->created_at; ?>"></small>
                                    <div class="small text-muted">
                                        <?= $m->created_at; ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="avatar d-block avatar-<?= $m->user->color; ?>" <?= View::getBgStyle($m->user->image); ?>>
                                        <?= View::getAvatarName($m->user->image, $m->user->short); ?>
                                    </div>
                                </td>
                                <td class="text-nowrap">
                                    <div class="text-capitalize">
                                        <?= $m->user->first_name; ?>
                                        <?= $m->user->last_name; ?>
                                    </div>
                                    <div class="small text-muted text-capitalize">
                                        User Type:
                                        <?= $m->user->type; ?>
                                    </div>
                                </td>
                                <td colspan="<?php $m->read ? 2 : 1; ?>" class="">
                                    <!--<span style="margin-left: 2rem">-->
                                    <?= ucfirst($m->message); ?>
                                    <!--</span>-->
                                </td>
                                <td class="">
                                    <?php if ($m->user_id): ?>
                                    <a href="<?= linkTo("messages/{$m->id}/read?user_id={$m->user_id}"); ?>"
                                        title="reply to
                                        <?= $m->user->first_name; ?>"
                                        class="icon">
                                        <i class="fe fe-corner-up-left"></i>
                                    </a>
                                    <?php endif; ?>
                                </td>
                                <td class="">
                                    <?php if (!$m->read): ?>
                                    <a href="<?= linkTo("messages/{$m->id}/read"); ?>"
                                        title="mark as read"
                                        class="icon">
                                        <i class="fe fe-trash"></i>
                                    </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
