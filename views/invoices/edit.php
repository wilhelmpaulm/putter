<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('discounts/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/discounts/<?= $id ?>" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Update Discount (<?= $item->name ?>) for <?= $item->inventory->name ?>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Discount Name</label>
                                        <input type="text" id="name" name="name" required="" value="<?= $item->name ?>" class="form-control" placeholder="This will appear on pos">
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="form-group">
                                        <label class="form-label">Label</label>
                                        <input type="text" id="label" name="label" required="" value="<?= $item->label ?>" class="form-control" placeholder="This will serve as the product identifier">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Details</label>
                                <textarea class="form-control" name="details" required="" rows="2" placeholder="This will serve as the long product description"><?= $item->details ?></textarea>
                            </div>
                            <div class="form-group">
                                <div class="form-label">Image</div>
                                <img src="<?= File::get('default.png') ?>" style="margin-bottom: .5rem; height: 6rem" class="img-thumbnail" alt="Heade image">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" accept="image/*" name="image">
                                    <label class="custom-file-label"><?= $item->image ?? 'Choose file' ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Set Discount Duration</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend" id="basic-addon1">
                                                <span class="input-group-text">
                                                    <i class="fe fe-calendar"></i>
                                                </span>
                                            </span>
                                            <input type="text" id="daterangepicker" required=""  class="form-control" title="the start date of the discount">
                                            <input type="hidden" id="start_date" name="start_date" value="<?= $item->start_date ?>" required="">
                                            <input type="hidden" id="end_date" name="end_date" value="<?= $item->end_date ?>" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Current Price</label>
                                        <input type="number" id="current_price" name="current_price" class="form-control" readonly="" value="<?= $item->inventory->price ?>">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="base_price">Multiplier</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend" id="basic-addon1">
                                                <span class="input-group-text">
                                                    <i class="fe fe-x"></i>
                                                </span>
                                            </span>
                                            <input type="number" id="multiplier" name="multiplier" min="1" class="form-control" required="true"  step="any" placeholder="This the multiplier that would be applied to the item price" value="<?= $item->multiplier ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="price">Final Price</label>
                                        <input type="number" id="final_price" name="final_price" class="form-control" min="0" step="any" value="<?= $item->multiplier * $item->inventory->price ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="active" value="1" <?= $item->active ? 'checked=""' : '' ?> class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Set discount as active</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <a  href="<?= linkTo("discounts/{$id}/delete") ?>"
                            title="Delete discount <?= $item->name ?>"
                            class="btn btn-link text-muted"> Delete Discount
                        </a>
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>