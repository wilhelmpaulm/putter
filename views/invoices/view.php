<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3 d-print-none">
            <?php include linkPage('accounting/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="container">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">#<?= padId($item->id); ?></h3>
                        <div class="card-options">
                            <button type="button" class="btn btn-primary" onclick="javascript:window.print();"><i class="fe fe-printer"></i> Print Invoice</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row my-8">
                            <div class="col-6">
                                <p class="h3"><?= Config::get('club', 'name'); ?></p>
                                <address>
                                    <?= Config::get('club', 'street'); ?>,
                                    <?= Config::get('club', 'city'); ?>, <?= Config::get('club', 'region'); ?>,
                                    <?= Config::get('club')->postal; ?><br>
                                    <?= Config::get('email', 'address'); ?>
                                </address>
                            </div>
                            <div class="col-6 text-right">
                                <p class="h3"><?= $item->payer->full_name; ?></p>
                                <address>
                                    <?= $item->payer->address; ?><br>
                                    <?= $item->payer->email; ?>
                                </address>
                            </div>
                        </div>
                        <div class="table-responsive push">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <td colspan="2" class="text-center">Invoice ID #<?= padId($item->id); ?></td>
                                    <td colspan="3" class="text-center text-nowrap">Prepared at <?= $item->created_at; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center" style="width: 1%"></th>
                                    <th>Product</th>
                                    <th class="text-center" style="width: 1%">Qnt</th>
                                    <th class="text-right" style="width: 1%">Unit</th>
                                    <th class="text-right" style="width: 1%">Price</th>
                                </tr>
                                <?php foreach ($item->items as $i) : ?>
                                <tr>
                                    <td class="text-center">#<?= padId($i->id); ?></td>
                                    <td>
                                        <p class="font-w600 mb-1"><?= $i->name; ?></p>
                                        <?php if ($i->label) : ?>
                                        <div class="text-muted small">- <?= $i->label; ?></div>
                                        <?php endif; ?>
                                        <?php if ($i->details) : ?>
                                        <div class="text-muted small">- <?= $i->details; ?></div>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?= $i->amount; ?>
                                    </td>
                                    <td class="text-right text-nowrap">PHP <?= number_format($i->price, 2); ?></td>
                                    <td class="text-right text-nowrap">PHP
                                        <?= number_format($i->price * $i->amount, 2); ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right"></td>
                                    <td colspan="2" class="font-w600 text-right">Subtotal</td>
                                    <td class="text-right">PHP <?= number_format($item->subtotal, 2); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right"></td>
                                    <td colspan="2" class="font-w600 text-right">Vat Rate</td>
                                    <td class="text-right"><?= $item->vat; ?>%</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right"></td>
                                    <td colspan="2" class="font-w600 text-right">Vat Due</td>
                                    <td class="text-right">PHP
                                        <?= number_format($item->subtotal && $item->vat ? $item->subtotal * ($item->vat / 100) : 0.0, 2); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right"></td>
                                    <td colspan="2" class="font-weight-bold text-uppercase text-right">User Balance
                                    </td>
                                    <td class=" text-right text-nowrap">PHP
                                        <?= number_format($item->current_balance * 1.0, 2); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right"></td>
                                    <td colspan="2" class="font-weight-bold text-uppercase text-right">Total Amount Due
                                    </td>
                                    <td class=" text-right text-nowrap">PHP <?= number_format($item->total * 1.0, 2); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center"></td>
                                    <td colspan="3" class="text-center">
                                        <strong class="font-weight-bold text-uppercase">
                                            Invoice <?= $item->status; ?>
                                        </strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <p class="text-muted text-center"><?= Config::get('accounting', 'invoice_footer'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include linkPage('template/_bottom'); ?>
