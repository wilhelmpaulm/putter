<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('tournaments/nav') ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tournament Reservations</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th>Created</th>
                                <th>Details</th>
                                <th>Date</th>
                                <th class="text-center" width="20%">Players</th>
                                <th width='1'>Status</th>
                                <th width='1'></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($items as $i): ?>
                                <tr>
                                    <td class="">
                                        <span class="text-muted small"><?= padId($i->id) ?></span>
                                    </td>
                                    <td class="text-wrap" >
                                        <a href="<?= File::get($i->image) ?>" target="_blank">
                                            <img src="<?= File::get($i->image ?? 'default.png') ?>" alt="..." class="img-thumbnail"  style="max-width: 10rem">
                                        </a>
                                        <br>
                                        <a href="<?= linkTo("events/reservations/$i->id") ?>" title="preview event post">
                                            <strong class="text-capitalize">
                                                <?= $i->name ?>
                                            </strong>
                                        </a>
                                        <div class="small text-capitalize">
                                            <?= $i->label ?>
                                        </div>
                                        <br>
                                        <div class="text-capitalize small">
                                            Course:
                                            <a href="<?= linkTo("inventory/{$i->id}") ?>">
                                                <?= $i->facility->name ?>
                                            </a>
                                        </div>
                                        <div class="small">
                                            Published: <?= $i->public ? 'true' : 'false' ?>
                                        </div>
                                    </td>
                                    <td class="small text-nowrap">
                                        <div class="">
                                            <strong>Start:</strong> <br> <?= (new DateTime($i->start_date))->format("F j, Y, g:i A") ?>
                                        </div>
                                        <br>
                                        <div class="">
                                            <strong>End:</strong> <br><?= (new DateTime($i->end_date))->format("F j, Y, g:i A") ?>
                                        </div>
                                        <br>
                                        <div class="">
                                            <strong>Created at:</strong> <br><?= (new DateTime($i->created_at))->format("F j, Y, g:i A") ?>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="avatar-list avatar-list-stacked">
                                            <?php foreach ($i->guest_list as $u): ?>
                                                <a href="/members/<?= $u->id ?>" 
                                                   class="avatar avatar-<?= $u->color ?>" 
                                                   style="background-image: url(<?= File::get($u->image) ?>)"
                                                   title="<?= $u->full_name ?>">
                                                       <?= File::get($u->image) ? "" : $u->short ?>
                                                </a>
                                            <?php endforeach; ?>
                                            <?php if (count($i->guest_list) > 5): ?>
                                                <span class="avatar" title="<?= count($i->guest_list) ?> guests">
                                                    <?= count($i->guest_list) ?>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                    <td class="text-nowrap">
                                        <div class="text-center tag">
                                            <?= $i->status ?>
                                        </div>
                                    </td>
                                    <td>
                                        <?php if (in_array($i->status, ['pending', 'reviewed'])): ?>
                                            <a  href="<?= linkTo("tournaments/{$i->id}/cancel") ?>"
                                                title="Cancel this reservation <?= $i->name ?>"
                                                class="icon">
                                                <i class="fe fe-trash"></i>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>