<h3 class="page-title mb-5">Tournament Reservations</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo('tournaments') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-flag"></i></span>Tournament Reservations
        </a>
        <a href="<?= linkTo('tournaments/add') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-plus"></i></span> Make a Tournament Reservation
        </a>
    </div>
</div>