<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('tournaments/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/tournaments/add" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Create a Tournament Reservation
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Golf Course</label>
                                        <select name="facility_id"  id="select-reload" data-url="/tournaments/add?facility_id=" class="form-control custom-select"> 
                                            <?php foreach ($courses as $u): ?>
                                                <option
                                                <?= Input::get('facility_id') == $u->id ? 'selected=""' : '' ?>
                                                    data-data='{"image": "<?= File::get($u->image) ?>", "short": "<?= $u->short ?>", "color": "<?= $u->color ?>"}'
                                                    value="<?= $u->id ?>">
                                                        <?= ucwords($u->name) ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <div class="form-text">
                                            <a class="btn btn-link" href="<?= Config::get('system', 'weather') ?>" target="_blank">View Weather Forecasts</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Select Date</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend" id="basic-addon1">
                                                <span class="input-group-text">
                                                    <i class="fe fe-calendar"></i>
                                                </span>
                                            </span>
                                            <input type="text" id="date" required=""  class="form-control" value=""  title="the start date of the discount">
                                            <input type="hidden" id="start_date" name="start_date" required=""  class="form-control" value="<?= Input::get('start_date') ? Input::get('start_date') : date("Y-m-d", strtotime("now")); ?>"  title="the start date of the discount">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Start Time</label>
                                        <select name="start_time" required="" id="select-beast" class="form-control custom-select reload">
                                            <?php foreach ($start_times as $key => $val): ?>
                                                <option
                                                <?= $key == Input::get('start_time') ? 'selected=""' : '' ?>
                                                    value="<?= $key ?>"><?= $val ?></option>
                                                <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <?php if ($end_times): ?>
                                    <div class="col-4">
                                        <div class="form-group">    
                                            <label class="form-label">End Time</label>
                                            <select name="end_time" required="" id="select-beast" class="form-control custom-select reload">
                                                <?php foreach ($end_times as $key => $val): ?>
                                                    <option
                                                    <?= $key == Input::get('end_time') ? 'selected=""' : '' ?>
                                                        value="<?= $key ?>"><?= $val ?></option>
                                                    <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php $discount = $facility->discount(Input::get('start_date') . " " . Input::get('start_time'), Input::get('start_date') . " " . Input::get('start_time')); ?>
                            <?php if ($discount && $discount->id): ?>
                                <div class="row">
                                    <div class="col-12">
                                        <label class="form-label">Discount Promotion</label>
                                        <div class="card card-aside">
                                            <a href="/promotions/<?= $discount->id ?>" class="card-aside-column" style="background-image: url(<?= File::get($discount->image ?? 'default.png') ?>)"></a>
                                            <div class="card-body d-flex flex-column">
                                                <h4><a href="/promotions/<?= $discount->id ?>"><?= $discount->name ?></a></h4>
                                                <blockquote class=""><?= $discount->label ?></blockquote>
                                                <p class="small">Venue: <?= $discount->inventory->name ?></p>
                                                <p class="small">
                                                    Reserve During: <?= (new DateTime($discount->start_date))->format("F j, Y, g:i A") ?>
                                                    until
                                                    <?= (new DateTime($discount->end_date))->format("F j, Y, g:i A") ?>
                                                </p>
                                                <?php if ($discount->discount_start_date): ?>
                                                    <p class="small">
                                                        For Reservations In: <?= (new DateTime($discount->discount_start_date))->format("F j, Y, g:i A") ?>
                                                        up until
                                                        <?= (new DateTime($discount->discount_end_date))->format("F j, Y, g:i A") ?>
                                                    </p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php $price = $facility->current_price(Input::get('start_date') . " " . Input::get('start_time'), Input::get('start_date') . " " . Input::get('start_time')); ?>
                            <div class="border my-3" style="width: 100%"></div>
                            <?php if ($discount && $discount->id): ?>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="form-label">Old Price</label>
                                            <input type="text" readonly="" class="form-control" value="PHP <?= $facility->price ?>">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="form-label">Difference</label>
                                            <input type="text" readonly="" class="form-control" value="PHP <?= $price - $facility->price ?>">
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-label">Price Per Slot</label>
                                        <input type="text" readonly="" class="form-control" 
                                               value="PHP <?= $price ?>">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-label">Time Slots</label>
                                        <input type="text" readonly="" class="form-control" value="<?= $slots ?>">
                                    </div>
                                </div>
                                <?php if ($slots): ?>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label class="form-label">Recommended Max Players</label>
                                            <input type="text" readonly="" class="form-control" value="<?= 4 * $slots ?>">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label class="form-label">Total Price</label>
                                            <input type="text" readonly="" class="form-control" value="PHP <?= $price * $slots ?>">
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="border my-3" style="width: 100%"></div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <div class="form-label">Event Poster Image</div>
                                        <img src="<?= File::get('default.png') ?>" style="margin-bottom: .5rem; max-height: 15rem" class="img-thumbnail" alt="Heade image">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" accept="image/*" name="image">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="border my-3" style="width: 100%"></div>
                            <div class="form-group">
                                <label class="form-label">Name of the Event</label>
                                <input type="text" name="name" required="" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Brief Description of the Event</label>
                                <input type="text" name="label" required="" class="form-control" value="">
                            </div>
                            <div class="form-group" style="height: 25rem; padding-bottom: 1.5rem">
                                <label class="form-label">Event Post</label>
                                <textarea class="form-control" id="jodit" name="details" rows="5" placeholder="Big belly rude boy, million dollar hustler. Unemployed."></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tags</label>
                                <input type="text" class="form-control" name="tags" id="input-tags" value="tournament, golf, course, club">
                            </div>
                            <div class="form-group" style="padding-bottom: 3rem">
                                <label class="form-label">Invite Club Members</label>
                                <select name="guests[]" data-max="30"  multiple="" id="select-users" class="form-control custom-select">
                                    <option selected=""
                                            data-data='{"image": "<?= File::get(user('image')) ?>", "short": "<?= user('short') ?>", "color": "<?= user('color') ?>"}'
                                            value="<?= user('id') ?>">
                                                <?= ucwords(user('full_name')) ?>
                                    </option> 
                                    <?php foreach ($members as $u): ?>
                                        <option 
                                        <?= $u->id == (int) Input::get('user_id') ? 'selected' : '' ?> 
                                            data-data='{"image": "<?= File::get($u->image) ?>", "short": "<?= $u->short ?>", "color": "<?= $u->color ?>"}'
                                            value="<?= $u->id ?>">
                                                <?= ucwords($u->full_name) ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="public" value="1" checked="" class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Make this event public</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>