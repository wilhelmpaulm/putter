<!doctype html>
<html lang="en" dir="ltr">
<?php include linkPage('template/head'); ?>

<body class="">
    <div class="page">
        <div class="page-single">
            <div class="container">
                <div class="row">
                    <div class="col col-6 mx-auto">
                        <div class="text-center mb-6">
                            <a href="/">
                                <img src="<?=File::get(Config::get('system')->header, 'app/uploads'); ?>" class="h-6"
                                    alt="">
                            </a>
                        </div>
                        <div class="card">
                            <div class="card-body p-6">
                                <?=Config::get('terms', 'main');?>
                            </div>
                            <div class="card-footer text-right">
                                <a class="btn btn-primary" href="javascript:history.back()" onclick="back()">
                                    <i class="fe fe-arrow-left mr-2"></i>Go back
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
