<!doctype html>
<html lang="en" dir="ltr">
    <?php include linkPage("template/head"); ?>
    <body class="">
        <div class="page">
            <div class="page-single">
                <div class="container">
                    <div class="row">
                        <div class="col col-login mx-auto">
                            <div class="text-center mb-6">
                                <a href="/">
                                    <img src="<?= File::get(Config::get('system')->header, 'app/uploads') ?>" class="h-6" alt="">
                                </a>
                            </div>
                            <form class="card" action="/forgot" method="post">
                                <div class="card-body p-6">
                                    <div class="card-title">Reset your password via email</div>
                                    <div class="form-group">
                                        <label class="form-label">Email address</label>
                                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required="" placeholder="Enter email">
                                    </div>
                                    <div class="form-footer">
                                        <button type="submit" class="btn btn-primary btn-block">Confirm</button>
                                    </div>
                                </div>
                            </form>
                            <div class="text-center text-muted">
                                Don't have account yet? <a href="/register">Sign up</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>