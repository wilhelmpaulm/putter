<!doctype html>
<html lang="en" dir="ltr">
<?php include linkPage('template/head'); ?>

<body class="">
    <div class="page">
        <div class="page-content">
            <div class="container text-center">
                <?php if ($header || $body) : ?>
                <h1 class="h2 mb-5">
                    <?= getResponseCodeMessage($code); ?>
                </h1>
                <h1 class="h4 mb-4"><?= $header ?? '' ?></h1>
                <p class="h4 text-muted font-weight-normal mb-7"><?= $body ?? '' ?></p>
                <?php else : ?>
                <div class="display-1 text-muted mb-5"><i class="si si-exclamation"></i>
                    <?= $code; ?>
                </div>
                <h1 class="h2 mb-5">
                    <?= getResponseCodeMessage($code); ?>
                </h1>
                <h1 class="h4 mb-4">Oops... that page doesn't exist</h1>
                <p class="h4 text-muted font-weight-normal mb-7">Are you sure you're in the right page?</p>
                <?php endif; ?>
                <a class="btn btn-primary" href="#" onclick="back()">
                    <i class="fe fe-arrow-left mr-2"></i>Go back
                </a>
            </div>
        </div>
    </div>
</body>

</html>
