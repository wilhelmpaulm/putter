<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('facilities/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/facilities/<?= $id ?>" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Update Facility Details of <?= $facility->name ?>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Type</label>
                                        <select class="form-control" name="type" required="">
                                            <?php foreach ($types as $type): ?>
                                                <option value="<?= $type ?>" <?= $facility->type == $type ? 'selected=""' : '' ?>><?= $type ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Name</label>
                                        <input type="text" id="name" name="name" required="" value="<?= $facility->name ?>" class="form-control" placeholder="This will appear on pos">
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="form-group">
                                        <label class="form-label">Label</label>
                                        <input type="text" id="label" name="label" required="" value="<?= $facility->label ?>" class="form-control" placeholder="This will serve as the product identifier">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Details</label>
                                <textarea class="form-control" name="details" required="" rows="2" placeholder="This will serve as the long product description"><?= $facility->details ?></textarea>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <div class="form-label">Image</div>
                                        <img src="<?= File::get($facility->image ?? 'default.png') ?>" style="margin-bottom: .5rem; height: 6rem" class="img-thumbnail" alt="Heade image">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" accept="image/*" value="<?= $facility->image ?>" name="image">
                                            <label class="custom-file-label"><?= $facility->image ?? 'Choose file' ?></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="price">Price</label>
                                        <input type="number" id="price" name="price"  value="<?= $facility->price ?>" class="form-control" required="true" placeholder="Please make sure to add + or -." value="0.0">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Latitude</label>
                                        <input type="text" id="latitude" name="latitude" required=""  value="<?= $facility->latitude ?>" class="form-control" placeholder="0.0">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <div class="form-label">Longitude</div>
                                        <input type="text" id="longitude" name="longitude" required=""  value="<?= $facility->longitude ?>" class="form-control" placeholder="0.0">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="active" value="1" <?= $facility->active ? 'checked=""' : '' ?> class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Mark item as active</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <a  href="<?= linkTo("facilities/{$id}/delete") ?>"
                            title="Delete facility <?= $facility->name ?>"
                            class="btn btn-link text-muted"> Delete Facility
                        </a>
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>
