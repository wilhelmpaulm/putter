<?php include linkPage('template/_top') ?>
<div class="container">
    <h1 class="page-title">
        Club Facilities and Venues
    </h1>
    <br>
    <div class="row">
        <?php foreach ($items as $i): ?>
            <div class="col-md-4">
                <div class="card">
                    <a href="#"><img class="card-img-top" src="<?= File::get($i->image) ?>" alt="And this isn&#39;t my nose. This is a false one."></a>
                    <div class="card-body d-flex flex-column">
                        <h4><a href="#"><?= $i->name ?></a></h4>
                        <div class="text-muted"><?= $i->label ?></div>
                        <br>
                        <div><?= $i->details ?></div>
                        <br>
                        <table class="table card-table">
                            <tbody>
                                <tr>
                                    <td>Latitude</td>
                                    <td class="text-right"><span class="text-muted"><?= $i->latitude ?></span></td>
                                </tr>
                                <tr>
                                    <td>Longitude</td>
                                    <td class="text-right"><span class="text-muted"><?= $i->longitude ?></span></td>
                                </tr>
                                <tr>
                                    <td>Reservation Price</td>
                                    <td class="text-right"><span class="text-muted">PHP <?= $i->price ?></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>