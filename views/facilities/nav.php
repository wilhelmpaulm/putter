<h3 class="page-title mb-5">Facilities</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo('facilities') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-map"></i></span> View Facilities
        </a>
        <a href="<?= linkTo('facilities/add') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-plus"></i></span> Add Facility
        </a>
    </div>
</div>