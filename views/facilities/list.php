<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('facilities/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">View Facilities</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th class="w-1">ID.</th>
                                <th width='1'></th>
                                <th width="1">Facility</th>
                                <th width='1'>Price</th>
                                <th width='1'>Type</th>
                                <th>Details</th>
                                <th width="1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($facilities as $f): ?>
                            <tr>
                                <td><span class="text-muted small"><?=padId($f->id); ?></span></td>
                                <td class="text-center">
                                    <a href="<?=File::get($f->image ?? 'default.png'); ?>" target="_blank">
                                        <div class="avatar avatar-list"
                                            style="background-image: url(<?=File::get($f->image ?? 'default.png'); ?>);">
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?=linkTo("facilities/{$f->id}"); ?>">
                                        <div class="text-capitalize">
                                            <?=$f->name; ?>
                                        </div>
                                    </a>
                                </td>
                                <td class="text-nowrap">
                                    <div class="small">
                                        Price: PHP <?=$f->price; ?>
                                    </div>
                                    <div class="small text-muted">
                                        Base: PHP <?=$f->base_price; ?>
                                    </div>
                                    <div class="small text-muted">
                                        Profit: PHP <?=$f->profit; ?>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="text-center tag">
                                        <?=$f->type; ?>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="small">
                                        <?=$f->label; ?>
                                    </div>
                                    <div class="text-muted small">
                                        <?=$f->details; ?>
                                    </div>
                                </td>
                                <td>
                                    <a href="<?=linkTo("facilities/{$f->id}"); ?>"
                                        title="Update details of <?=$f->name; ?>" class="icon">
                                        <i class="fe fe-edit"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom');?>
