<?php include linkPage('template/_top') ?>
<div class="container">
    <h1 class="page-title">
        Golf Courses
    </h1>
    <br>
    <div class="row">
        <?php foreach ($items as $i): ?>
            <div class="col-lg-4">
                <div class="card">
                    <a href="/teetimes/add?facility_id=<?= $i->id ?>"><img class="card-img-top " src="<?= File::get($i->image ?? 'default.png') ?>"  alt="And this isn&#39;t my nose. This is a false one."></a>
                    <div class="card-body d-flex flex-column">
                        <h4><a href="/teetimes/add?facility_id=<?= $i->id ?>&start_date=<?= date("Y-m-d", strtotime("now")); ?>"><?= $i->name ?></a></h4>
                        <div class="text-muted"><?= $i->label ?></div>
                        <br>
                        <div><?= $i->details ?></div>
                        <br>
                        <table class="table card-table">
                            <tbody>
                                <tr>
                                    <td>Latitude</td>
                                    <td class="text-right"><span class="text-muted"><?= $i->latitude ?></span></td>
                                </tr>
                                <tr>
                                    <td>Longitude</td>
                                    <td class="text-right"><span class="text-muted"><?= $i->longitude ?></span></td>
                                </tr>
                                <tr>
                                    <td>Tee Time Price</td>
                                    <td class="text-right"><span class="text-muted">PHP <?= $i->price ?></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>