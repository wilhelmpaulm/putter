<h3 class="page-title mb-5"><?= ucwords("$member->first_name $member->last_name")?></h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo("members/$id/profile") ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-user"></i></span>Profile
        </a>
        <a href="<?= linkTo("members/$id/comments") ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-message-square"></i></span> Comments
        </a>
        <a href="<?= linkTo("members/$id/report") ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-thumbs-down"></i></span> Report
        </a>
    </div>
<!--    <div class="mt-6">
        <a href="#" class="btn btn-secondary btn-block">Compose new Email</a>
    </div>-->
</div>