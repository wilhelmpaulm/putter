<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('members/nav') ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <form class="card" action="<?= linkTo("members/$member->id/comments") ?>" method="post"  enctype="multipart/form-data">
                    <div class="card-header">
                        <div class="input-group">
                            <input type="text" name="message" required="" class="form-control" placeholder="Message">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">
                                    <i class="fe fe-send"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <ul class="list-group card-list-group">
                    <?php foreach ($comments as $c): ?>
                        <li class="list-group-item py-5">
                            <div class="media">
                                <div class="media-object avatar avatar-md avatar-<?= $c->user->color ?> mr-4" style="background-image: url(demo/faces/male/16.jpg)"><?= $c->user->short ?></div>
                                <div class="media-body">
                                    <div class="media-heading">
                                        <small class="float-right text-muted timeago" datetime="<?= $c->created_at ?>"></small>
                                        <h5><?= $c->user->first_name . " " . $c->user->last_name ?></h5>
                                    </div>
                                    <div><?= $c->message ?></div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<?php include linkPage('template/_bottom') ?>