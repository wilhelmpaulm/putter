<!doctype html>
<html lang="en" dir="ltr">
    <?php include linkPage("template/head"); ?>
    <body class="">
        <div class="page">
            <div class="page-single">
                <div class="container">
                    <div class="row">
                        <div class="col col-login mx-auto">
                            <div class="text-center mb-6">
                                <a href="/">
                                    <img src="<?= File::get(Config::get('system')->header, 'app/uploads') ?>" class="h-6" alt="">
                                </a>
                            </div>
                            <form class="card" action="/login" method="post">
                                <div class="card-body p-6">
                                    <div class="card-title">Login to your account</div>
                                    <div class="form-group">
                                        <label class="form-label">Email address</label>
                                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">
                                            Password
                                            <a href="forgot" class="float-right small">I forgot my password</a>
                                        </label>
                                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="remember" class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">Remember me</span>
                                        </label>
                                    </div>
                                    <div class="form-footer">
                                        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                    </div>
                                </div>
                            </form>
                            <div class="text-center text-muted">
                                Don't have account yet? <a href="/request">Sign up</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>