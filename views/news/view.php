<!doctype html>
<html lang="en" dir="ltr">
<?php include linkPage('template/head'); ?>

<body class="">
    <div class="page">
        <div class="page-main">
            <?php include linkPage('template/title'); ?>
            <?php include linkPage('template/navbar'); ?>
            <div class="my-3 my-md-5">
                <div class="container">
                    <div class="row row-cards row-deck">
                        <div class="col-8 offset-2">
                            <div class="card">
                                <div class="card-body d-flex flex-column">
                                    <?php if ($item->image && File::get($item->image)) : ?>
                                    <div class="text-center">
                                        <a href="<?= File::get($item->image); ?>" target="_">
                                            <img src="<?= File::get($item->image); ?>" class="img-thumbnail" style="width: 320px">
                                        </a>
                                    </div>
                                    <br>
                                    <?php endif; ?>
                                    <h2><a href="/news/<?= $item->id; ?>">
                                            <?= ucwords($item->name); ?></a></h2>
                                    <blockquote class="blockquote">
                                        <p class="mb-0">
                                            <?= ucfirst($item->label); ?>
                                        </p>
                                    </blockquote>
                                    <div class="" style="overflow: scroll">
                                        <?= $item->body; ?>
                                    </div>
                                    <br>
                                    <div class="">
                                        <div class="pull-right">
                                            <?php foreach ($item->tag_list as $t) : ?>
                                            <div class="text-center tag">
                                                <?= $t; ?>
                                            </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center pt-5 mt-auto">
                                        <div class="avatar avatar-md mr-3" <?php if ($item->user->image) : ?> style="background-image: url(<?= File::get($item->user->image); ?>)" <?php endif; ?>>
                                            <?= File::get($item->user->image) ?? $item->user->short; ?>
                                        </div>
                                        <div>
                                            <a href="/members/<?= $item->user_id; ?>" class="text-default">
                                                <?= $item->user->full_name; ?></a>
                                            <small class="d-block text-muted timeago" datetime="<?= $item->created_at; ?>"></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include linkPage('template/footer'); ?>
</body>

</html>
