<!doctype html>
<html lang="en" dir="ltr">
<?php include linkPage('template/head'); ?>

<body class="">
    <div class="page">
        <div class="page-main">
            <?php include linkPage('template/title'); ?>
            <?php include linkPage('template/navbar'); ?>
            <div class="my-3 my-md-5">
                <div class="container">
                    <div class="page-header">
                        <h1 class="page-title">
                            Blog Posts and Club News
                        </h1>
                        <form action="/news" class="page-options d-flex">
                            <div class="input-group" style="padding-right: 1rem">
                                <select name="type" class="form-control reload">
                                    <option value="all">All</option>
                                    <?php foreach (Blog::types() as $u): ?>
                                    <option <?= Input::get('type') == $u ? 'selected=""' : ''; ?>
                                        value="
                                        <?= $u; ?>">
                                        <?= ucwords($u); ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="input-group">
                                <span class="input-group-prepend" id="basic-addon1">
                                    <span class="input-group-text">
                                        <i class="fe fe-calendar"></i>
                                    </span>
                                    <input type="text" id="daterangepicker" required="" data-position="left" data-min="false" class="form-control" style="width: 20rem" title="filter posts">
                                    <input type="hidden" id="start_date" name="start_date" value="<?= Input::get('start_date') ? Input::get('start_date') : date('Y-m-d H:i:s', strtotime('-7 day')); ?>" required="">
                                    <input type="hidden" id="end_date" name="end_date" value="<?= Input::get('end_date') ? Input::get('end_date') : date('Y-m-d H:i:s', strtotime('+7 day')); ?>" required="">
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="row row-cards row-deck">
                        <?php if (!count($items)): ?>
                        <div class="col-lg-12 text-center">
                            <br>
                            <br>
                            <b class="h2">No Avaliable
                                <?= Input::get('type') != 'all' ? ucfirst(Input::get('type')) : ''; ?> Posts</b>
                        </div>
                        <?php endif; ?>
                        <?php foreach ($items as $i): ?>
                        <div class="col-lg-6">
                            <div class="card card-aside">
                                <a href="/news/<?= $i->id; ?>" class="card-aside-column" style="background-image: url(<?= File::get($i->image ?? 'default.png'); ?>)"></a>
                                <div class="card-body d-flex flex-column">
                                    <h4><a href="/news/<?= $i->id; ?>">
                                            <?= $i->name; ?></a></h4>
                                    <div class="text-muted">
                                        <?= $i->label; ?>
                                    </div>
                                    <br>
                                    <div class="">
                                        <div class="pull-right">
                                            <?php foreach ($i->tag_list as $t): ?>
                                            <div class="text-center tag">
                                                <?= $t; ?>
                                            </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center pt-5 mt-auto">
                                        <div class="avatar avatar-md  avatar-<?= $i->user->color; ?> mr-3" <?= View::getBgStyle($i->user->image); ?>>
                                            <?= View::getAvatarName($i->user->image, $i->user->short); ?>
                                        </div>
                                        <div>
                                            <a href="/members/<?= $i->user_id; ?>" class="text-default">
                                                <?= $i->user->full_name; ?></a>
                                            <small class="d-block text-muted timeago" datetime="<?= $i->created_at; ?>"></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include linkPage('template/footer'); ?>
</body>

</html >
