<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3 d-print-none">
            <?php include linkPage('accounting/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="container">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">#<?=padId($item->id); ?></h3>
                        <div class="card-options">
                            <button type="button" class="btn btn-primary" onclick="javascript:window.print();"><i
                                    class="fe fe-printer"></i> Print Billing Statement</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row my-8">
                            <div class="col-6">
                                <p class="h3"><?=Config::get('club', 'name'); ?></p>
                                <address>
                                    <?=Config::get('club', 'street'); ?>,
                                    <?=Config::get('club', 'city'); ?>, <?=Config::get('club', 'region'); ?>,
                                    <?=Config::get('club')->postal; ?><br>
                                    <?=Config::get('email', 'address'); ?>
                                </address>
                            </div>
                            <div class="col-6 text-right">
                                <p class="h3"><?=$item->payer->full_name; ?></p>
                                <address>
                                    <?=$item->payer->address; ?><br>
                                    <?=$item->payer->email; ?>
                                </address>
                            </div>
                        </div>
                        <div class="table-responsive push">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <td colspan="3" class="text-center">Billing Statement ID #<?=padId($item->id); ?>
                                    </td>
                                    <td colspan="3" class="text-center">Prepared at <?=$item->created_at; ?></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center">From <?=$item->start_date; ?> until
                                        <?=$item->end_date; ?></td>
                                    <td colspan="3" class="text-center"></td>
                                </tr>
                                <tr>
                                    <th class="text-center" style="width: 1%">Created</th>
                                    <th class="text-center text-nowrap" style="width: 1%">Invoice ID</th>
                                    <th>Invoice Details</th>
                                    <th class="text-center" style="width: 1%">Amount</th>
                                    <th class="text-right" style="width: 1%">Status</th>
                                    <th class="text-right text-nowrap" style="width: 1%">Amount Due</th>
                                </tr>
                                <?php if (!count($item->items)): ?>
                                <td colspan="6" class="font-weight-bold text-center">
                                    No Invoices for this Billing Duration
                                </td>
                                <?php endif; ?>
                                <?php foreach ($item->items as $i): ?>
                                <tr>
                                    <td class="text-center small  text-nowrap"><?=padId($i->created_at); ?></td>
                                    <td class="text-center">#<?=padId($i->id); ?></td>
                                    <td>
                                        <p class="font-w600 mb-1 small"><?=$i->name; ?></p>
                                    </td>
                                    <td class="text-center text-nowrap">
                                        PHP <?=number_format($i->price, 2); ?>
                                    </td>
                                    <td class="text-center text-uppercase"><?=$i->status ?? 'pending'; ?></td>
                                    <td class="text-right text-nowrap">PHP
                                        <?=number_format($i->amount ? $i->price : '0.0', 2); ?></td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <td colspan="5" class="font-weight-bold text-uppercase text-right">User Balance
                                    </td>
                                    <td class="text-right text-nowrap">PHP
                                        <?=number_format($item->current_balance * 1.0, 2); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="font-weight-bold text-uppercase text-right">Total Amount Due
                                    </td>
                                    <td class="text-right text-nowrap">PHP <?=number_format($item->total * 1.0, 2);?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <p class="text-muted text-center"><?=Config::get('accounting', 'billing_footer');?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include linkPage('template/_bottom');?>
