<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('accounting/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">View Invoices</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th class="w-1">ID.</th>
                                <th>Date Prepared</th>
                                <?php if (in_array(user('type'), ['accounting', 'officer', 'staff'])): ?>
                                <th width='1'></th>
                                <th>Member</th>
                                <?php endif; ?>
                                <th width='1'>Items</th>
                                <th width='1'>Total</th>
                                <th width="1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($items as $i): ?>
                            <tr>
                                <td><a href="/billings/<?= $i->id; ?>" class="text-muted small">
                                        <?= padId($i->id); ?></a></td>
                                <td class="">
                                    <div class="text-center small">
                                        <?= (new DateTime($i->created_at))->format('F j, Y, g:i A'); ?>
                                    </div>
                                </td>
                                <?php if (in_array(user('type'), ['accounting', 'officer', 'staff'])): ?>
                                <td>
                                    <a href="<?= linkTo("members/{$i->user->id}"); ?>">
                                        <div class="avatar d-block avatar-<?= $i->user->color; ?>" <?= View::getBgStyle($i->user->image); ?>>
                                            <?= View::getAvatarName($i->user->image, $i->user->short); ?>
                                        </div>
                                    </a>
                                </td>
                                <td class="text-nowrap">
                                    <a href="<?= linkTo("members/{$i->user->id}"); ?>">
                                        <div class="text-capitalize">
                                            <?= $i->user->full_name; ?>
                                        </div>
                                    </a>
                                </td>
                                <?php endif; ?>
                                <td class="text-nowrap">
                                    <div class="small">
                                        <?= count($i->items); ?>
                                    </div>
                                </td>
                                <td class="text-nowrap">
                                    <div class="small">
                                        PHP
                                        <?= number_format($i->total, 2); ?>
                                    </div>
                                </td>
                                <td class="">
                                    <a href="<?= linkTo('billings/'.$i->id); ?>" class="dropdown-item"><i class="dropdown-icon fe fe-external-link"></i></a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
