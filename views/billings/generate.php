<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('accounting/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/billings/generate" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Generate User Billing Statements
                    </h3>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <input type="password" name="password" class="form-control" required="true" placeholder="Enter your password to verify your intent">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Select Billing Duration</label>
                                <div class="input-group">
                                    <span class="input-group-prepend" id="basic-addon1">
                                        <span class="input-group-text">
                                            <i class="fe fe-calendar"></i>
                                        </span>
                                    </span>
                                    <input type="text" id="daterangepicker-plain" required=""  class="form-control">
                                    <input type="hidden" id="start_date" name="start_date" value="<?= date("Y-m-d H:i:s", strtotime("-30 days")); ?>" required="">
                                    <input type="hidden" id="end_date" name="end_date" value="<?= date("Y-m-d H:i:s", strtotime("now")); ?>" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="agree" value="1" class="custom-switch-input agree">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">I agree with <a href='/terms'>terms and conditions</a></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" disabled="" class="btn btn-primary needAgree">Generate Billing Statements</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>