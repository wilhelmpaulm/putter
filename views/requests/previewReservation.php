<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('requests/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        <?= padId($request->id); ?> -
                        <?= str_replace('_', ' ', $request->request); ?>
                        <span class="small text-muted">
                            by
                            <?= $request->user->first_name; ?>
                            <?= $request->user->last_name; ?>
                        </span>
                    </h3>
                </div>
                <div class="card-body d-flex flex-column">
                    <?php if ($item->image && File::get($item->image)) : ?>
                    <div class="text-center">
                        <img src="<?= File::get($item->image); ?>" class="img-thumbnail">
                    </div>
                    <br>
                    <?php endif; ?>
                    <h2>
                        <?= ucwords($item->name); ?>
                    </h2>
                    <p class="">
                        <strong>
                            <?= $item->facility->name; ?> </strong> -
                        <?= (new DateTime($item->start_date))->format('F j, Y, g:i A'); ?>
                        Until
                        <?= (new DateTime($item->end_date))->format('F j, Y, g:i A'); ?>
                    </p>

                    <blockquote class="blockquote">
                        <p class="mb-0">
                            <?= ucfirst($item->label); ?>
                        </p>
                    </blockquote>
                    <div class="" style="overflow: scroll">
                        <?= $item->details; ?>
                    </div>
                    <br>
                    <div class="">
                        <div class="pull-right">
                            <?php foreach ($item->tag_list as $t) : ?>
                            <div class="text-center tag">
                                <?= $t; ?>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="d-flex align-items-center pt-5 mt-auto">
                        <div class="avatar avatar-md mr-3" style="background-image: url(<?= File::get($item->user->image); ?>)">
                            <?= File::get($item->user->image) ?? $item->user->short; ?>
                        </div>
                        <div>
                            <a href="/members/<?= $item->user_id; ?>" class="text-default">
                                <?= $item->user->full_name; ?></a>
                            <small class="d-block text-muted timeago" datetime="<?= $item->created_at; ?>"></small>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <a href="<?= linkTo('requests/'.$request->id.'/reject'); ?>">
                        <button class="btn btn-link">Reject Application</button>
                    </a>
                    <a href="<?= linkTo('requests/'.$request->id.'/approve'); ?>">
                        <button type="submit" class="btn btn-primary">Approve Request</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include linkPage('template/_bottom'); ?>
