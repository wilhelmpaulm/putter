<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('requests/nav') ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title text-capitalize"><?= padId($request->id) ?> - <?= str_replace('_', ' ', $request->request) ?>
                        <span class="small text-muted">
                            by <?= $request->user->first_name ?> <?= $request->user->last_name ?>
                        </span>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <div class="form-group">
                                        <span class="avatar avatar-xxl avatar-<?= $request->user->color?>" style="background-image: url(<?= File::get($request->user->image) ?>)"><?= $request->user->short ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Email address</label>
                                        <input type="email" readonly="" name="email" required="" autocomplete="false" class="form-control" placeholder="Email" value="<?= $request->user->email ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Gender</label>
                                        <input type="text" readonly="" name="last_name" required="" class="form-control" value="<?= $request->user->gender ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">First Name</label>
                                        <input type="text" readonly="" name="first_name" required="" class="form-control" placeholder="Company" value="<?= $request->user->first_name ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Middle Name</label>
                                        <input type="text" readonly="" name="middle_name" class="form-control" placeholder="Company" value="<?= $request->user->middle_name ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Last Name</label>
                                        <input type="text" readonly="" name="last_name" required="" class="form-control" placeholder="Last Name" value="<?= $request->user->last_name ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Mobile</label>
                                        <input type="text"readonly="" name="mobile" required="" class="form-control" <input type="text" readonly="" name="last_name" required="" class="form-control" placeholder="Last Name" value="<?= $request->user->mobile ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Address</label>
                                        <input type="text"readonly="" name="address" required="" class="form-control" placeholder="Home Address" <input type="text" readonly="" name="last_name" required="" class="form-control" placeholder="Last Name" value="<?= $request->user->address ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">City</label>
                                        <input type="text"readonly="" name="city" required="" class="form-control" placeholder="City" <input type="text" readonly="" name="last_name" required="" class="form-control" placeholder="Last Name" value="<?= $request->user->city ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="form-label">Zip Code</label>
                                        <input type="number"readonly="" name="zip_code" required="" class="form-control" <input type="text" readonly="" name="last_name" required="" class="form-control" placeholder="Last Name" value="<?= $request->user->zip_code ?>">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="form-label">Country</label>
                                        <input type="text"readonly="" name="country" required="" class="form-control" placeholder="Philippines" value="<?= $request->user->country ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group mb-0">
                                <label class="form-label">Background</label>
                                <textarea rows="5"readonly="" name="intent" required="" class="form-control" placeholder="Give a brief explanation of your intentions in the club and how you found out about us."><?= $request->user->details ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group mb-0">
                                <label class="form-label">Why do you want to join the club?</label>
                                <textarea rows="5"readonly="" name="intent" required="" class="form-control" placeholder="Give a brief explanation of your intentions in the club and how you found out about us." value="Mike"><?= $request->user->intent ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <a href="<?= linkTo('requests/' . $request->id . "/reject") ?>">
                    <button class="btn btn-link">Reject Application</button>
                </a>
                <a href="<?= linkTo('requests/' . $request->id . "/approve") ?>">
                    <button type="submit" class="btn btn-primary">Submit For Review</button>
                </a>
            </div>

        </div>
    </div>
</div>
</div>
<?php include linkPage('template/_bottom') ?>