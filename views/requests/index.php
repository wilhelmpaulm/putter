<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('requests/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <?= ucfirst($request_status); ?> Requests</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap dataTable">
                        <thead>
                            <tr>
                                <th class="w-1">ID.</th>
                                <th width="5">User</th>
                                <th></th>
                                <th>Type</th>
                                <th>Date Created</th>
                                <?php if ($request_status == 'pending'): ?>
                                <th></th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($requests as $r): ?>
                            <tr>
                                <td><span class="text-muted small">
                                        <?= padId($r->id); ?></span></td>
                                <td>
                                    <a href="<?= linkTo("members/{$r->user->id}"); ?>">
                                        <div class="avatar d-block avatar-<?= $r->user->color; ?>" <?= View::getBgStyle($r->user->image); ?>>
                                            <?= View::getAvatarName($r->user->image, $r->user->short); ?>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?= linkTo("members/{$r->user->id}"); ?>">
                                        <div class="text-capitalize">
                                            <?= $r->user->first_name; ?>
                                            <?= $r->user->last_name; ?>
                                        </div>
                                    </a>
                                    <div class="small text-muted text-capitalize">
                                        User Type:
                                        <?= $r->user->type; ?>
                                    </div>
                                </td>
                                <td class="text-capitalize">
                                    <?= str_replace('_', ' ', $r->request); ?>
                                </td>
                                <td>
                                    <div>
                                        <?= $r->date_created; ?>
                                    </div>
                                    <div class="timeago text-capitalize" datetime="<?= $r->created_at; ?>"></div>
                                    <div class="small text-muted">
                                        <?= $r->created_at; ?>
                                    </div>
                                </td>
                                <?php if ($r->status == 'pending' && $r->user_id != user('id') && $r->type == user('type')): ?>
                                <td class="text-right">
                                    <div class="item-action dropdown show">
                                        <div class="item-action dropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-menu"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a href="<?= linkTo('requests/'.$r->id.'/approve'); ?>" class="dropdown-item"><i class="dropdown-icon fe fe-check"></i> Approve </a>
                                                <a href="<?= linkTo('requests/'.$r->id.'/reject'); ?>" class="dropdown-item"><i class="dropdown-icon fe fe-x"></i> Reject</a>
                                                <div class="dropdown-divider"></div>
                                                <a href="<?= linkTo('requests/'.$r->id); ?>" class="dropdown-item"><i class="dropdown-icon fe fe-external-link"></i> View</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <?php else: ?>
                                <td>
                                </td>
                                <?php endif; ?>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
