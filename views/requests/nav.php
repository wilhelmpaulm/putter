<h3 class="page-title mb-5">Requests</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo('requests/pending') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-file-text"></i></span>Pending Requests
        </a>
        <a href="<?= linkTo('requests/approved') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-check"></i></span>Approved Requests
        </a>
        <a href="<?= linkTo('requests/rejected') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-x"></i></span>Rejected Requests
        </a>
        <a href="<?= linkTo('requests/cancelled') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-minus"></i></span>Cancelled Requests
        </a>
    </div>
    <!--    <div class="mt-6">
            <a href="#" class="btn btn-secondary btn-block">Compose new Email</a>
        </div>-->
</div>