<!doctype html>
<html lang="en" dir="ltr">
    <?php include linkPage("template/head"); ?>
    <body class="">
        <div class="page">
            <div class="page-main">
                <?php include linkPage('template/title') ?>
                <?php include linkPage('template/navbar') ?>    
                <div class="my-3 my-md-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="card card-body" style="padding: 2rem">
                                    <?= Config::get('site', 'main') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include linkPage("template/footer"); ?>
    </body>
</html>