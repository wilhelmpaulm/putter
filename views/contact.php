<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row row-cards">
        <div class="offset-2 col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Contact Us</h3>
                </div>
                <div class="card-map" id="card-map-placeholder"  style="background-image: url(./demo/staticmap.png)"></div>
                <div class="card-body">
                    <div class="media mb-5">
                        <img class="d-flex mr-5 rounded" height="64" src="<?= File::get(Config::get('system')->logo, 'app/uploads') ?>" alt="Generic placeholder image">
                        <div class="media-body">
                            <h5><?= Config::get('club')->name ?></h5>
                            <address class="text-muted small">
                                <?= Config::get('club')->street ?>,
                                <?= Config::get('club')->barangay ?><br>
                                <?= Config::get('club')->city ?>,
                                <?= Config::get('club')->region ?>,
                                <?= Config::get('club')->postal ?>,
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="h6">Business Type</div>
                            <p>Golf Club</p>
                        </div>
                        <div class="col-4">
                            <div class="h6">Website</div>
                            <p><a href="<?= linkTo() ?>"><?= linkTo() ?></a></p>
                        </div>
                        <div class="col-4">
                            <div class="h6">Office Phone</div>
                            <p><?= Config::get('club')->mobile ?></p>
                        </div>
                    </div>
                    <div class="h6">Description</div>
                    <p><?= Config::get('club')->description ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var map;
    function initMap() {
        // The location of Uluru
        var uluru = {lat: <?= Config::get('club', 'latitude') ?>, lng: <?= Config::get('club', 'longitude') ?>};
        // The map, centered at Uluru
        var map = new google.maps.Map(
                document.getElementById('card-map-placeholder'), {zoom: 14, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('system', 'maps') ?>&callback=initMap" async defer></script>
<?php include linkPage('template/_bottom') ?>