<!doctype html>
<html lang="en" dir="ltr">
    <?php include linkPage("template/head"); ?>
    <body class="">
        <div class="page">
            <div class="page-single">
                <div class="container">
                    <div class="row">
                        <div class="col col-login mx-auto">
                            <div class="text-center mb-6">
                                <a href="/">
                                    <img src="<?= File::get(Config::get('system')->header, 'app/uploads') ?>" class="h-6" alt="">
                                </a>
                            </div>
                            <form class="card" action="#">
                                <div class="card-body p-6">
                                    <div class="card-title">Track Your Request</div>
                                    <div class="form-group">
                                        <label class="form-label">Request</label>
                                        <input type="text" name="type"  class="form-control" readonly="" value="<?= $request->request ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Status</label>
                                        <input type="text" name="status"  class="form-control" readonly="" value="<?= $request->status ?>">
                                    </div>
                                </div>
                            </form>
                            <div class="text-center text-muted">
                                Already have account? <a href="/login">Sign in</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>