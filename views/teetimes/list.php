<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('teetimes/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tee Time Reservations</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th>Created</th>
                                <th>Date</th>
                                <th width='1'></th>
                                <th>Course</th>
                                <th>Details</th>
                                <th class="text-center" width="20%">Players</th>
                                <th width='1'>Status</th>
                                <th width='1'>Appearance</th>
                                <th width="1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($items as $i) : ?>
                            <tr>
                                <td class="">
                                    <span class="timeago small" datetime="<?= $i->created_at; ?>"></span>
                                </td>
                                <td class="">
                                    <div class="text-center small">
                                        <?= (new DateTime($i->start_date))->format('F j, Y, g:i A'); ?>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <a href="<?= File::get($i->facility->image); ?>" target="_blank">
                                        <div class="avatar d-block avatar-<?= $i->user->color; ?>" <?= View::getBgStyle($i->user->image); ?>>
                                            <?= View::getAvatarName($i->user->image, $i->user->short); ?>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?= linkTo("facilities/{$i->facility->id}"); ?>">
                                        <div class="text-capitalize">
                                            <?= $i->facility->name; ?>
                                        </div>
                                    </a>
                                </td>
                                <td class="">
                                    <div class="small">
                                        <?= $i->details; ?>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="avatar-list avatar-list-stacked">
                                        <?php foreach ($i->guest_list as $u) : ?>
                                        <a href="/members/<?= $u->id; ?>" class="avatar avatar-<?= $u->color; ?>" style="background-image: url(<?= File::get($u->image); ?>)" title="<?= $u->full_name; ?>">
                                            <?= File::get($u->image) ? '' : $u->short; ?>
                                        </a>
                                        <?php endforeach; ?>
                                        <?php if (count($i->guest_list) > 5) : ?>
                                        <span class="avatar" title="<?= count($i->guest_list); ?> guests">
                                            <?= count($i->guest_list); ?>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </td>
                                <td class="text-nowrap  text-center">
                                    <div class="text-center tag">
                                        <?= $i->status; ?>
                                    </div>
                                </td>
                                <td class="text-nowrap text-center">
                                    <div class="text-center tag">
                                        <?= $i->true_appearance; ?>
                                    </div>
                                </td>
                                <td>
                                    <?php if (!$i->expired && in_array($i->appearance, ['pending']) && !in_array($i->status, ['cancelled']) && in_array(user('type'), ['officer', 'accounting', 'staff', 'admin'])) : ?>
                                    <a href="<?= linkTo("reservations/{$i->id}/showed"); ?>" title="Guests showed up" class="icon">
                                        <i class="fe fe-check"></i>
                                    </a>
                                    <a href="<?= linkTo("reservations/{$i->id}/missed"); ?>" title="Guests missed the reservation" class="icon">
                                        <i class="fe fe-x"></i>
                                    </a>
                                    <?php endif; ?>
                                    <?php if (!$i->expired && !in_array($i->status, ['cancelled']) && $i->true_appearance == 'pending') : ?>
                                    <a href="<?= linkTo("teetimes/{$i->id}/cancel"); ?>" title="Cancel this reservation
                                        <?= $i->name; ?>" class="icon">
                                        <i class="fe fe-trash"></i>
                                    </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
