<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('teetimes/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/teetimes/add" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Create a Tee Time Reservation
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-label">Golf Course</label>
                                        <select name="facility_id"  id="select-reload" data-url="/teetimes/add?facility_id=" class="form-control custom-select">
                                            <?php foreach ($courses as $u): ?>
                                                <option
                                                <?= Input::get('facility_id') == $u->id ? 'selected=""' : '' ?>
                                                    data-data='{"image": "<?= File::get($u->image) ?>", "short": "<?= $u->short ?>", "color": "<?= $u->color ?>"}'
                                                    value="<?= $u->id ?>">
                                                        <?= ucwords($u->name) ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <div class="form-text">
                                            <a class="btn btn-link" href="<?= Config::get('system', 'weather') ?>" target="_blank">View  Weather Forecasts</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Select Date</label>
                                        <div class="input-group">
                                            <span class="input-group-prepend" id="basic-addon1">
                                                <span class="input-group-text">
                                                    <i class="fe fe-calendar"></i>
                                                </span>
                                            </span>
                                            <input type="text" id="date" required=""  class="form-control" value=""  title="the start date of the discount">
                                            <input type="hidden" id="start_date" name="start_date" required=""  class="form-control" value="<?= Input::get('start_date') ? Input::get('start_date') : date("Y-m-d", strtotime("now")); ?>"  title="the start date of the discount">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Tee Time</label>
                                        <select name="time" required="" id="select-beast" class="form-control custom-select">
                                            <?php foreach ($times as $key => $val): ?>
                                                <option
                                                <?= Input::get('time') == $key ? 'selected=""' : '' ?>
                                                    value="<?= $key ?>"><?= $val ?></option>
                                                <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Reservation Price Per Slot</label>
                                        <input type="text" readonly="" class="form-control" value="PHP <?= $facility->current_price(Input::get('start_date') . " " . Input::get('time'), Input::get('start_date') . " " . Input::get('time')) ?>">
                                    </div>
                                </div>
                            </div>
                            <?php $discount = $facility->discount(Input::get('start_date') . " " . Input::get('time'), Input::get('start_date') . " " . Input::get('time')); ?>
                            <?php if ($discount && $discount->id): ?>
                                <div class="row">
                                    <div class="col-12">
                                        <label class="form-label">Discount Promotion</label>
                                        <div class="card card-aside">
                                            <a href="/promotions/<?= $discount->id ?>" class="card-aside-column" style="background-image: url(<?= File::get($discount->image ?? 'default.png') ?>)"></a>
                                            <div class="card-body d-flex flex-column">
                                                <h4><a href="/promotions/<?= $discount->id ?>"><?= $discount->name ?></a></h4>
                                                <blockquote class=""><?= $discount->label ?></blockquote>
                                                <p class="small">Venue: <?= $discount->inventory->name ?></p>
                                                <p class="small">
                                                    Reserve During: <?= (new DateTime($discount->start_date))->format("F j, Y, g:i A") ?>
                                                    until
                                                    <?= (new DateTime($discount->end_date))->format("F j, Y, g:i A") ?>
                                                </p>
                                                <?php if ($discount->discount_start_date): ?>
                                                    <p class="small">
                                                        For Reservations In: <?= (new DateTime($discount->discount_start_date))->format("F j, Y, g:i A") ?>
                                                        up until
                                                        <?= (new DateTime($discount->discount_end_date))->format("F j, Y, g:i A") ?>
                                                    </p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if (in_array(user('type'), ['officer', 'staff', 'accounting'])): ?>
                            <div class="form-group">
                                <label class="form-label">Users list</label>
                                <select name="paid_by" id="select-user-plain" class="form-control custom-select">
                                    <?php foreach ($members as $u): ?>
                                    <option
                                        data-data='{"image": "<?= File::get($u->image) ?>", "short": "<?= $u->short ?>", "color": "<?= $u->color ?>"}'
                                        <?= $u->id == (int) Input::get('paid_by') ? 'selected' : '' ?> value="<?= $u->id ?>">
                                        <?= ucwords($u->full_name) ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label class="form-label">
                                    Add Players
                                    <span class="text-muted-dark">(Suggested Minimum PAX: 4)</span>
                                </label>
                                <select name="guests[]" data-max="4"  multiple="" id="select-users" class="form-control custom-select">
                                    <option selected=""
                                            data-data='{"image": "<?= File::get(user('image')) ?>", "short": "<?= user('short') ?>", "color": "<?= user('color') ?>"}'
                                            value="<?= user('id') ?>">
                                                <?= ucwords(user('full_name')) ?>
                                    </option>
                                    <?php foreach ($members as $u): ?>
                                        <option
                                        <?= $u->id == (int) Input::get('user_id') ? 'selected' : '' ?>
                                            data-data='{"image": "<?= File::get($u->image) ?>", "short": "<?= $u->short ?>", "color": "<?= $u->color ?>"}'
                                            value="<?= $u->id ?>">
                                                <?= ucwords($u->full_name) ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Details</label>
                                <textarea class="form-control" name="details" rows="5" placeholder="Big belly rude boy, million dollar hustler. Unemployed."></textarea>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="public" value="1" checked="" class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Make this event public</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>
