<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('inventory/nav'); ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">View Inventory</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th class="w-1">ID.</th>
                                <th width='1'></th>
                                <th width="1">Inventory</th>
                                <th width='1'>Price</th>
                                <th width='1'>Amount</th>
                                <th width='1'>Type</th>
                                <th>Details</th>
                                <th width="1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($items as $i): ?>
                            <tr>
                                <td><span class="text-muted small"><?=padId($i->id); ?></span></td>
                                <td class="text-center">
                                    <a href="<?=File::get($i->image ?? 'default.png'); ?>" target="_blank">
                                        <div class="avatar avatar-list"
                                            style="background-image: url(<?=File::get($i->image); ?>);">
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?=linkTo("inventory/{$i->id}"); ?>">
                                        <div class="text-capitalize">
                                            <?=$i->name; ?>
                                        </div>
                                    </a>
                                </td>
                                <td class="text-nowrap">
                                    <div class="small">
                                        Price: PHP <?=$i->price; ?>
                                    </div>
                                    <div class="small text-muted">
                                        Base: PHP <?=$i->base_price; ?>
                                    </div>
                                    <div class="small text-muted">
                                        Profit: PHP <?=$i->profit; ?>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="text-center">
                                        <?=$i->amount; ?>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="text-center tag">
                                        <?=$i->type; ?>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="small">
                                        <?=$i->label; ?>
                                    </div>
                                    <div class="text-muted small">
                                        <?=$i->details; ?>
                                    </div>
                                </td>
                                <td>
                                    <a href="<?=linkTo("inventory/{$i->id}"); ?>"
                                        title="Update details of <?=$i->name; ?>" class="icon">
                                        <i class="fe fe-edit"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom');?>
