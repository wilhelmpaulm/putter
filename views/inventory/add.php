<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('inventory/nav') ?>
        </div>
        <div class="col-md-9">
            <form class="card" action="/inventory/add" method="post"  enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title text-capitalize">
                        Add Inventory Item
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Name</label>
                                        <input type="text" id="name" name="name" required="" class="form-control" placeholder="This will appear on pos">
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="form-group">
                                        <label class="form-label">Label</label>
                                        <input type="text" id="label" name="label" required="" class="form-control" placeholder="This will serve as the product identifier">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Details</label>
                                <textarea class="form-control" name="details" required="" rows="2" placeholder="This will serve as the long product description"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-label">Type</label>
                                        <select class="form-control" name="type" required="">
                                            <?php foreach ($types as $type): ?>
                                                <option value="<?= $type ?>"><?= $type ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-label">Code</label>
                                        <input type="text" id="code" name="code" class="form-control" placeholder="If the item has a barcode enter it here">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <div class="form-label">Location</div>
                                        <input type="text" id="location" name="location" required="" class="form-control" placeholder="Where the item is location in the club">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <div class="form-label">Image</div>
                                        <img src="<?= File::get('default.png') ?>" style="margin-bottom: .5rem; height: 6rem" class="img-thumbnail" alt="Heade image">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" accept="image/*" required="" name="image">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-label">Amount</label>
                                        <input type="number" id="amount" name="amount" class="form-control" required="true" placeholder="Please make sure to add + or -." value="0.0">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="base_price">Base Price</label>
                                        <input type="number" id="base_price" name="base_price" class="form-control" required="true" placeholder="This is the price of the product from the supplier" value="0.0">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="price">Price</label>
                                        <input type="number" id="price" name="price" class="form-control" required="true" placeholder="Please make sure to add + or -." value="0.0">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="available" value="1" checked="" class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Mark item as available for members</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="custom-switch">
                                    <input type="checkbox" name="active" value="1" checked="" class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Mark item as active</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="reset" class="btn btn-link">Clear Form</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>