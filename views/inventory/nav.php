<h3 class="page-title mb-5">Inventory</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <a href="<?= linkTo('inventory') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-package"></i></span> View Inventory
        </a>
        <a href="<?= linkTo('inventory/add') ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-plus"></i></span> Add Inventory
        </a>
    </div>
</div>