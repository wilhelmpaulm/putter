<?php include linkPage('template/_top') ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?php include linkPage('banquets/nav') ?>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Banquets</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter dataTable">
                        <thead>
                            <tr>
                                <th>Venue</th>
                                <th>Details</th>
                                <th>Schedule</th>
                                <th class="text-center">Guests</th>
                                <th width='15%'>Tags</th>
                                <th width='1'>Status</th>
                                <th width="1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($items as $i): ?>
                                <tr>
                                    <td class="text-center">
                                        <a href="<?= linkTo("facilities/{$i->facility->id}") ?>">
                                            <div class="text-capitalize">
                                                <?= $i->facility->name ?>
                                            </div>
                                        </a>
                                    </td>
                                    <td class="text-wrap" >
                                        <a href="<?= File::get($i->image) ?>" target="_blank">
                                            <img src="<?= File::get($i->image ?? 'default.png') ?>" alt="..." class="img-thumbnail"  style="max-width: 100%">
                                        </a>
                                        <br>
                                        <strong class="">
                                            <?= $i->name ?>
                                        </strong>
                                        <div class="small">
                                            <?= $i->label ?>
                                        </div>
                                        <br>
                                        <div class="small">
                                            Capacity: <?= $i->capacity ?> pax
                                        </div>
                                        <div class="small">
                                            Created at: <span class="timeago" datetime="<?= $i->created_at ?>"></span>
                                        </div>

                                    </td>
                                    <td class="text-center">
                                        <div class=" small">
                                            <?= (new DateTime($i->start_date))->format("F j, Y, g:i A") ?>
                                        </div>
                                        <div class="text-center small">
                                            Until
                                        </div>
                                        <div class=" small">
                                            <?= (new DateTime($i->end_date))->format("F j, Y, g:i A") ?>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="avatar-list avatar-list-stacked">
                                            <?php foreach ($i->guest_list as $u): ?>
                                                <a href="/members/<?= $u->id ?>" 
                                                   class="avatar avatar-<?= $u->color ?>" 
                                                   style="background-image: url(<?= File::get($u->image) ?>)"
                                                   title="<?= $u->full_name ?>">
                                                       <?= File::get($u->image) ? "" : $u->short ?>
                                                </a>
                                            <?php endforeach; ?>
                                            <?php if (count($i->guest_list) > 5): ?>
                                                <span class="avatar" title="<?= count($i->guest_list) ?> guests">
                                                    <?= count($i->guest_list) ?>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                    <td class="">
                                        <?php foreach ($i->tag_list as $t): ?>
                                            <div class="text-center tag">
                                                <?= $t ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </td>
                                    <td class="text-wrap">
                                        <div class="text-center tag">
                                            <?= $i->status ?>
                                        </div>
                                    </td>
                                    <td>
                                        <?php if (in_array($i->status, ['pending', 'reviewed'])): ?>
                                            <a  href="<?= linkTo("reservations/{$i->id}/cancel") ?>"
                                                title="Cancel this reservation <?= $i->name ?>"
                                                class="icon">
                                                <i class="fe fe-trash"></i>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom') ?>