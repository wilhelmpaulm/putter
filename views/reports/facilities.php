<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-9 col-xl-3 col-0 d-print-none">
            <?php include linkPage('reports/nav'); ?>
        </div>
        <div class="col-xl-9 col-md-9 col-12">
            <div class="container">
                <form class=" card d-print-none" action="/reports/facilities" enctype="multipart/form-data">
                    <div class="card-header">
                        <h3 class="card-title text-capitalize">
                            Facility Revenue Report
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label class="form-label">Select Year</label>
                                            <select name="year" class="form-control custom-select">
                                                <?php foreach ($years as $year): ?>
                                                <option value="<?= $year; ?>" <?= $input && $input['year'] == $year ? 'selected=""' : ''; ?>>
                                                    <?= $year; ?>
                                                </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Chart Type</label>
                                            <select name="type" class="form-control custom-select">
                                                <?php foreach ($chartTypes as $type): ?>
                                                <option value="<?= $type; ?>" <?= $input && $input['type'] == $type ? 'selected=""' : ''; ?>>
                                                    <?= $type; ?>
                                                </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="pull-right">
                            <button type="reset" class="btn btn-link">Clear Form</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
                <?php if ($chart): ?>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Facility Revenue Report</h3>
                        <div class="card-options">
                            <button type="button" class="btn btn-primary" onclick="javascript:window.print();"><i class="fe fe-printer"></i> Print Report</button>
                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <canvas id="myChart" height="300" style="width: 100%"></canvas>
                        <script>
                            var ctx = document.getElementById("myChart");
                            var myChart = new Chart(ctx, {
                                type: '<?= $input['type'] ?? 'bar'; ?>',
                                data: <?= $chart->json(); ?>,
                                options: {
                                    plugins: {
                                        colorschemes: {
                                            scheme: 'tableau.Tableau20'
                                        }
                                    },
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                            });
                        </script>
                    </div>
                    <div class="card-footer table-responsive">
                        <table class="table table-striped ">
                            <thead class="">
                                <tr>
                                    <th></th>
                                    <?php foreach ($chart->all()->labels as $key => $value): ?>
                                    <th>
                                        <?= $value; ?>
                                    </th>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($chart->all()->datasets as $key => $value): ?>
                                <tr>
                                    <td scope="row"><strong>
                                            <?= $value->label; ?></strong></td>
                                    <?php foreach ($value->data as $key => $value): ?>
                                    <th>
                                        <?= $value; ?>
                                    </th>
                                    <?php endforeach; ?>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
