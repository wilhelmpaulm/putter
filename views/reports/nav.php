<h3 class="page-title mb-5">Reports</h3>
<div>
    <div class="list-group list-group-transparent mb-0">
        <?php if (in_array(user('type'), ['officer', 'accounting', 'staff'])): ?>
        <a href="<?= linkTo('reports/profit'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-dollar-sign"></i></span> Revenue and Profit Report
        </a>
        <a href="<?= linkTo('reports/delinquency'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-alert-circle"></i></span> Delinquency Report
        </a>
        <a href="<?= linkTo('reports/rounds'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-flag"></i></span> Golf Rounds Report
        </a>
        <a href="<?= linkTo('reports/reservations'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-calendar"></i></span> Reservations Report
        </a>
        <a href="<?= linkTo('reports/reservations/revenue'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-calendar"></i></span> Reservations Revenue Report
        </a>
        <a href="<?= linkTo('reports/booking'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-book"></i></span> Booking Breakdown Report
        </a>
        <a href="<?= linkTo('reports/appearance'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-book"></i></span> Reservation Usage Report
        </a>
        <a href="<?= linkTo('reports/facilities'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-map"></i></span> Facilities Revenue Report
        </a>
        <a href="<?= linkTo('reports/courses'); ?>" class="list-group-item list-group-item-action d-flex align-items-center">
            <span class="icon mr-3"><i class="fe fe-map"></i></span> Courses Revenue Report
        </a>
        <?php endif; ?>
    </div>
</div>
