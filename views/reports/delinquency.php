<?php include linkPage('template/_top'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-9 col-xl-3 col-0 d-print-none">
            <?php include linkPage('reports/nav'); ?>
        </div>
        <div class="col-xl-9 col-md-9 col-12">
            <div class="container">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Delinquency Report</h3>
                        <div class="card-options">
                            <button type="button" class="btn btn-primary" onclick="javascript:window.print();"><i class="fe fe-printer"></i> Print Report</button>
                        </div>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table card-table table-vcenter text-nowrap">
                            <thead>
                                <tr>
                                    <th width="5">User</th>
                                    <th></th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Amount Owed</th>
                                    <th>Since</th>
                                    <th class=" d-print-none" width="1"></th>
                                    <th class=" d-print-none" width="1"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($members as $m) : ?>
                                <tr>
                                    <td>
                                        <a href="<?= linkTo("members/{$m->id}"); ?>">
                                            <div class="avatar d-block avatar-<?= $m->color; ?>" <?= View::getBgStyle($m->image); ?>>
                                                <?= View::getAvatarName($m->image, $m->short); ?>
                                            </div>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?= linkTo("members/{$m->id}"); ?>">
                                            <div class="text-capitalize">
                                                <?= $m->full_name; ?>
                                            </div>
                                        </a>
                                    </td>
                                    <td class=""><span>
                                            <?= $m->mobile; ?></span></td>
                                    <td class=""><span>
                                            <?= $m->email; ?></span></td>
                                    <td class="">PHP
                                        <?= -1 * $m->balance; ?>
                                    </td>
                                    <td class="">
                                        <div class="text-muted small">
                                            <span class="timeago" datetime="<?= $m->negativeSince ?: $m->created_at;; ?>"></span>
                                        </div>
                                        <div class="text-muted small">
                                            <span>
                                                <?= $m->negativeSince ?: $m->created_at; ?></span>
                                        </div>
                                    </td>
                                    <td class=" d-print-none">
                                        <a href="<?= linkTo("accounting/adjust?user_id={$m->id}"); ?>" title="Adjust <?= $m->first_name; ?>'s balance" class="icon">
                                            <i class="fe fe-edit"></i>
                                        </a>
                                    </td>
                                    <td class=" d-print-none">
                                        <a href="<?= linkTo("reports/delinquency/{$m->id}"); ?>" title="View Invoice <?= $m->first_name; ?>'s balance" class="icon">
                                            <i class="fe fe-file"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include linkPage('template/_bottom'); ?>
