<?php

// user controller
$router->map('GET', '/login', 'UsersController#getLogin', 'login_get');
$router->map('POST', '/login', 'UsersController#postLogin', 'login_post');
$router->map('GET', '/forgot', 'UsersController#getForgot', 'forgot_password_get');
$router->map('GET', '/request', 'UsersController#getRequest', 'request_membership_get');
$router->map('GET', '/register', 'UsersController#getRegister', 'register_get');
$router->map('GET', '/track/[i:id]', 'UsersController#getTrack', 'track_get');
$router->map('GET|POST', '/logout', 'UsersController#getLogout', 'logout_get');
$router->map('POST', '/register', 'UsersController#postRegister', 'register_post');
$router->map('POST', '/forgot', 'UsersController#postForgot', 'forgot_password_post');
$router->map('POST', '/request', 'UsersController#postRequest', 'request_membership_post');

// request controller
$router->map('GET', '/requests/[i:id]', 'RequestsController#show', 'requests_show');
$router->map('GET', '/requests/[pending|approved|rejected|cancelled:request_status]', 'RequestsController#index', 'requests_get');
$router->map('GET', '/requests/[i:id]/[approve|reject:request_status]', 'RequestsController#updateStatus', 'requests_status_update');
$router->map('GET', '/requests', 'RequestsController#getPending', 'requests_pending_get');

// members controller
$router->map('GET', '/members/[current|review|officers|staff:type]', 'MembersController#index', 'members_index');
$router->map('GET', '/members/[i:id]/[activate|deactivate:status]', 'MembersController#updateStatus', 'members_status_update');
$router->map('GET', '/members/[i:id]/type/[officer|accounting|staff:type]', 'MembersController#updateType', 'members_type_update');
$router->map('GET', '/members/[i:id]/role/[*:role]', 'MembersController#updateRole', 'members_role_update');
$router->map('GET', '/members/[i:id]/notifications/[activate|deactivate:status]', 'MembersController#updateNotifications', 'members_notifications_update');
$router->map('GET', '/members/[i:id]/messages/[activate|deactivate:status]', 'MembersController#updateMessages', 'members_messages_update');
$router->map('GET', '/members/[i:id]/emails/[activate|deactivate:status]', 'MembersController#updateEmails', 'members_emails_update');
$router->map('GET', '/members/[i:id]', 'MembersController#show', 'members_show');
$router->map('GET', '/members/[i:id]/profile', 'MembersController#getProfile', 'members_profile_get');
$router->map('GET', '/members/[i:id]/edit', 'MembersController#editProfile', 'members_profile_edit');
$router->map('POST', '/members/[i:id]/update', 'MembersController#updateProfile', 'members_profile_update');
$router->map('GET', '/members/[i:id]/comments', 'MembersController#getComments', 'members_comments_get');
$router->map('POST', '/members/[i:id]/comments', 'MembersController#storeComment', 'members_comment_store');

$router->map('GET', '/members/endorse', 'MembersController#getEndorsement', 'members_endorsement_create_get');
$router->map('POST', '/members/endorse', 'MembersController#storeEndorsement', 'members_endorsement_store');

// dependents controller
$router->map('GET', '/dependents', 'DependentsController#index', 'dependents_get');
$router->map('GET', '/dependents/add', 'DependentsController#create', 'dependents_create');
$router->map('POST', '/dependents/add', 'DependentsController#store', 'dependents_store');

// messages controller
$router->map('GET', '/messages/[system|unread|read:status]', 'MessagesController#index', 'messages_get');
$router->map('GET', '/messages/all/read', 'MessagesController#updateReadAll', 'messages_read_all_update');
$router->map('GET', '/messages/[i:id]/read', 'MessagesController#updateRead', 'messages_read_update');
$router->map('GET', '/messages/send', 'MessagesController#create', 'messages_create');
$router->map('POST', '/messages/send', 'MessagesController#store', 'messages_store');

// me controller
$router->map('GET', '/me', 'MeController#index', 'me_get');
$router->map('GET', '/me/independent', 'MeController#getIndependent', 'me_independent_get');
$router->map('POST', '/me/independent', 'MeController#storeIndependent', 'me_independent_store');

$router->map('GET', '/me/requests', 'MeController#getRequests', 'me_requests_get');
$router->map('GET', '/me/settings', 'MeController#getSettings', 'me_settings_get');
$router->map('GET', '/me/reservations', 'MeController#getReservations', 'me_reservations_get');
$router->map('GET', '/me/teetimes', 'MeController#getTeetimes', 'me_teetimes_get');

$router->map('GET', '/me/cancel', 'MeController#getCancel', 'me_cancel_get');
$router->map('POST', '/me/cancel', 'MeController#postCancel', 'me_cancel_post');

$router->map('GET', '/me/password', 'MeController#getPassword', 'me_password_get');
$router->map('POST', '/me/password', 'MeController#postPassword', 'me_password_post');

// accounting controller
$router->map('GET', '/accounting/pos', 'AccountingController#getPos', 'pos_get');
$router->map('POST', '/accounting/pos', 'AccountingController#postPos', 'pos_post');

$router->map('GET', '/accounting/members', 'AccountingController#getAccountingMembers', 'accounting_members_get');

$router->map('GET', '/accounting/adjust', 'AccountingController#getAccountingAdjust', 'accounting_adjust_get');
$router->map('POST', '/accounting/adjust', 'AccountingController#postAccountingAdjust', 'accounting_adjust_post');

$router->map('GET', '/billings', 'AccountingController#getBillings', 'billings_get');
$router->map('GET', '/billings/[i:id]', 'AccountingController#getBilling', 'billing_get');
$router->map('GET', '/billings/generate', 'AccountingController#getBillingsGenerate', 'billings_generate_get');
$router->map('POST', '/billings/generate', 'AccountingController#postBillingsGenerate', 'billings_generate_post');

// inventory controller
$router->map('GET', '/inventory', 'InventoryController#index', 'inventories_get');
$router->map('GET', '/inventory/add', 'InventoryController#create', 'inventory_create');
$router->map('GET', '/inventory/[i:id]/delete', 'InventoryController#delete', 'inventory_delete');
$router->map('GET', '/inventory/[i:id]', 'InventoryController#show', 'inventory_show');
$router->map('POST', '/inventory/add', 'InventoryController#store', 'inventory_store');
$router->map('POST', '/inventory/[i:id]', 'InventoryController#update', 'inventory_update');

// facilities controller
$router->map('GET', '/courses', 'FacilitiesController#getCourses', 'courses_get');
$router->map('GET', '/venues', 'FacilitiesController#getVenues', 'venues_get');

$router->map('GET', '/facilities', 'FacilitiesController#index', 'facilities_get');
$router->map('GET', '/facilities/add', 'FacilitiesController#create', 'facilities_create');
$router->map('GET', '/facilities/[i:id]/delete', 'FacilitiesController#delete', 'facilities_delete');
$router->map('GET', '/facilities/[i:id]', 'FacilitiesController#show', 'facilities_show');
$router->map('POST', '/facilities/add', 'FacilitiesController#store', 'facilities_store');
$router->map('POST', '/facilities/[i:id]', 'FacilitiesController#update', 'facilities_update');

// discounts controller

$router->map('GET', '/discounts', 'DiscountsController#index', 'discounts_get');
$router->map('GET', '/discounts/add', 'DiscountsController#create', 'discounts_create');
$router->map('POST', '/discounts/add', 'DiscountsController#store', 'discounts_store');
$router->map('GET', '/discounts/[i:id]/delete', 'DiscountsController#delete', 'discounts_delete');
$router->map('GET', '/discounts/[i:id]', 'DiscountsController#show', 'discounts_show');
$router->map('POST', '/discounts/[i:id]', 'DiscountsController#update', 'discounts_update');

// teetimes controller

$router->map('GET', '/teetimes', 'TeetimesController#index', 'teetimes_index');
$router->map('GET', '/teetimes/add', 'TeetimesController#create', 'teetimes_create');
$router->map('POST', '/teetimes/add', 'TeetimesController#store', 'teetimes_store');
$router->map('GET', '/teetimes/[i:id]/cancel', 'TeetimesController#cancel', 'teetimes_cancel');

// reports controller

$router->map('GET', '/reports/profit', 'ReportsController#getProfit', 'reports_profit_get');
$router->map('GET', '/reports/facilities', 'ReportsController#getFacilities', 'reports_facilities_get');
$router->map('GET', '/reports/courses', 'ReportsController#getCourses', 'reports_courses_get');
$router->map('GET', '/reports/rounds', 'ReportsController#getGolfRounds', 'reports_golf_rounds_get');
$router->map('GET', '/reports/reservations/revenue', 'ReportsController#getReservationsRevenue', 'reports_reservations_revenue_get');
$router->map('GET', '/reports/reservations', 'ReportsController#getReservations', 'reports_reservations_get');
$router->map('GET', '/reports/delinquency', 'ReportsController#getDelinquency', 'reports_delinquency_get');
$router->map('GET', '/reports/delinquency/[i:id]', 'ReportsController#showDelinquency', 'reports_delinquency_show');
$router->map('GET', '/reports/booking', 'ReportsController#getBookingUsage', 'reports_booking_usage_get');
$router->map('GET', '/reports/appearance', 'ReportsController#getBookingAppearance', 'reports_booking_appearance_get');

// tournaments controller
$router->map('GET', '/tournaments', 'TournamentsController#index', 'tournaments_index');
$router->map('POST', '/tournaments/add', 'TournamentsController#store', 'tournaments_store');
$router->map('GET', '/tournaments/add', 'TournamentsController#create', 'tournaments_create');
$router->map('GET', '/tournaments/[i:id]/cancel', 'TournamentsController#cancel', 'tournaments_cancel');

// reservations controller
$router->map('GET', '/reservations', 'ReservationsController#index', 'reservations_index');
$router->map('GET', '/reservations/add', 'ReservationsController#create', 'reservations_create');
$router->map('POST', '/reservations/add', 'ReservationsController#store', 'reservations_store');
$router->map('GET', '/reservations/[i:id]/cancel', 'ReservationsController#cancel', 'reservations_cancel');
$router->map('GET', '/reservations/[i:id]/[showed|missed:appearance]', 'ReservationsController#patchAppearance', 'reservations_appearance_patch');

// banquetes controller
$router->map('GET', '/banquets', 'BanquetsController#index', 'banquets_index');
$router->map('GET', '/banquets/add', 'BanquetsController#create', 'banquets_create');
$router->map('POST', '/banquets/add', 'BanquetsController#store', 'banquets_store');
$router->map('GET', '/banquets/[i:id]/cancel', 'BanquetsController#cancel', 'banquets_cancel');

// invoices controller
$router->map('GET', '/invoices', 'InvoicesController#index', 'invoices_index');
$router->map('GET', '/invoices/[i:id]', 'InvoicesController#show', 'invoices_show');
$router->map('GET', '/invoices/[i:id]/[pending|paid|cancelled:status]', 'InvoicesController#updateStatus', 'invoices_status_update');

// events controller
$router->map('GET', '/events/teetimes', 'EventsController#teetimes', 'events_teetimes_index');
$router->map('GET', '/events/reservations', 'EventsController#reservations', 'events_reservations_index');
$router->map('GET', '/events/reservations/[i:id]', 'EventsController#reservation', 'event_reservation_show');

// news controller
$router->map('GET', '/news', 'NewsController#index', 'news_index');
$router->map('GET', '/news/[i:id]', 'NewsController#show', 'news_show');

// blogs controller
$router->map('GET', '/blogs', 'BlogsController#index', 'blogs_index');
$router->map('GET', '/blogs/add', 'BlogsController#create', 'blogs_create');
$router->map('GET', '/blogs/[i:id]', 'BlogsController#edit', 'blogs_edit');
$router->map('GET', '/blogs/[i:id]/delete', 'BlogsController#delete', 'blogs_delete');
$router->map('POST', '/blogs/add', 'BlogsController#store', 'blogs_store');
$router->map('POST', '/blogs/[i:id]', 'BlogsController#update', 'blogs_update');

// config controller
$router->map('GET', '/config/dashboard/controller', 'ConfigController#getDashboardController', 'config_dashboard_controller_get');
$router->map('POST', '/config/dashboard/controller', 'ConfigController#updateDashboardController', 'config_dashboard_controller_update');
$router->map('GET', '/config/dashboard', 'ConfigController#getDashboard', 'config_dashboard_get');
$router->map('GET', '/config/traffic', 'ConfigController#getTraffic', 'config_traffic_get');
$router->map('GET', '/config/accounting', 'ConfigController#getAccounting', 'config_accounting_get');
$router->map('POST', '/config/accounting', 'ConfigController#updateAccounting', 'config_accounting_update');
$router->map('GET', '/config/terms', 'ConfigController#getTerms', 'config_terms_get');
$router->map('POST', '/config/terms', 'ConfigController#updateTerms', 'config_terms_update');
$router->map('GET', '/config/site', 'ConfigController#getSite', 'config_site_get');
$router->map('POST', '/config/site', 'ConfigController#updateSite', 'config_site_update');
$router->map('GET', '/config/club', 'ConfigController#getClub', 'config_club_get');
$router->map('POST', '/config/club', 'ConfigController#updateClub', 'config_club_update');
$router->map('GET', '/config/email', 'ConfigController#getEmail', 'config_email_get');
$router->map('POST', '/config/email', 'ConfigController#updateEmail', 'config_email_update');
$router->map('GET', '/config/system', 'ConfigController#getSystem', 'config_system_get');
$router->map('POST', '/config/system', 'ConfigController#updateSystem', 'config_system_update');
$router->map('GET', '/config/limits', 'ConfigController#getLimits', 'config_limits_get');
$router->map('POST', '/config/limits', 'ConfigController#updateLimits', 'config_limits_update');

// access controller
$router->map('GET', '/access', 'AccessController#index', 'access_index');
$router->map('GET', '/access/add', 'AccessController#create', 'access_create');
$router->map('GET', '/access/[i:id]/edit', 'AccessController#edit', 'access_edit');
$router->map('POST', '/access/[i:id]', 'AccessController#update', 'access_update');
$router->map('GET', '/access/[i:id]/delete', 'AccessController#delete', 'access_delete');
$router->map('POST', '/access/add', 'AccessController#store', 'access_store');

// tickets controller

$router->map('GET', '/tickets', 'TicketsController#index', 'tickets_index');
$router->map('GET', '/tickets/add', 'TicketsController#create', 'tickets_create');
$router->map('GET', '/tickets/[i:id]/[resolved|cancelled|rejected:status]', 'TicketsController#updateStatus', 'tickets_status_update');
$router->map('GET', '/tickets/[i:id]', 'TicketsController#show', 'tickets_show');
$router->map('POST', '/tickets/add', 'TicketsController#store', 'tickets_store');
$router->map('POST', '/tickets/[i:id]/comments', 'TicketsController#comments', 'tickets_comments_get');

// manual seeder
$router->map('GET', '/seed', 'Seeder#seedSendBack');

// error pages
$router->map('GET|POST', '/error', 'IndexController#getError', 'error_get');
$router->map('GET|POST', '/error/[i:code]', 'IndexController#getError', 'error_code_get');

// index controller
$router->map('GET', '/', 'IndexController#getHome', 'home_get');
$router->map('GET', '/guest', 'IndexController#getGuest', 'guest_get');
$router->map('GET|POST', '/test', 'IndexController#getTest', 'test_get');
$router->map('GET', '/terms', 'IndexController#getTerms', 'terms_get');
$router->map('GET', '/contact', 'IndexController#getContact', 'contact_get');
