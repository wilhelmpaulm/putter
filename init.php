<?php

if (preg_match('/\.(?:png|jpg|jpeg|gif|js|json|css|ttf|ico|woff|pdf|txt|map)$/i', $_SERVER['SCRIPT_NAME'])) {
    return false;
}

// setup PHP init values
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);
ini_set('date.timezone', 'Asia/Manila');
ini_set('session.save_path', 'temp/sessions');

// setup PHP gzip
if (extension_loaded('zlib') && !ini_get('zlib.output_compression') && key_exists('HTTP_ACCEPT_ENCODING', $_SERVER) && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
    header('Access-Control-Allow-Origin: *');
    header('Content-Encoding: gzip');
    ob_start('ob_gzhandler');
}

try {
    require './vendor/autoload.php';
} catch (Exception $e) {
    error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');
    echo 'Message: '.$e->getMessage();
}
