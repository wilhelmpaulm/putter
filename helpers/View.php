<?php

class View
{
    public static function getBgStyle(string $imageUrl = null)
    {
        if ($imageUrl) {
            $url = File::get($imageUrl);

            return "style='background-image: url({$url})'";
        }

        return '';
    }

    public static function getAvatarName(string $imageUrl = null, string $alt = null)
    {
        if (File::get($imageUrl)) {
            return '';
        }

        return $alt ?? 'NA';
    }
}
