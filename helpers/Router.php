<?php

use Illuminate\Support\Collection;

class Router extends AltoRouter
{
    public static $router;

    /**
     * Create router in one call from config.
     *
     * @param array  $routes
     * @param string $basePath
     * @param array  $matchTypes
     */
    public function __construct($routes = array(), $basePath = '', $matchTypes = array())
    {
        parent::__construct($routes, $basePath, $matchTypes);
    }

    public static function Router($routes = array(), $basePath = '', $matchTypes = array())
    {
        if (!isset(self::$router)) {
            self::$router = new static($routes, $basePath, $matchTypes);
        }

        return self::$router;
    }

    public function getRouteList()
    {
        $list = [];
        foreach ($this->routes as $key => $value) {
            if ($value[3]) {
                $list[] = $value[3];
            }
        }

        return $list;
    }

    public function getRoutes(): Collection
    {
        $routeCollection = collect($this->routes);
        $routes = $routeCollection->map(function ($item, $key) {
            $breakdown = collect(explode('_', $item[3]));
            $base = ucwords($breakdown->first());
            $action = strtolower($breakdown->pop());
            $body = ucwords($action.' '.implode(' ', $breakdown->all()));
            $details = '';

            switch ($action) {
                case 'get':
                    $details = 'View the '.$body.' page.';
                    break;
                case 'index':
                    $details = 'View the list of '.$body.'.';
                    break;
                case 'show':
                    $details = 'View the a single instance of '.$body.'.';
                    break;
                case 'post':
                    $details = 'Submit the '.$body.' form.';
                    break;
                case 'update':
                    $details = 'Update the '.$body.'.';
                    break;
                case 'edit':
                    $details = 'Edit an instance of '.$body.'.';
                    break;
                case 'delete':
                    $details = 'Delete an instance of '.$body.'.';
                    break;
                case 'create':
                    $details = 'View the form to create a new '.$body.'.';
                    break;
                case 'store':
                    $details = 'Create a new '.$body.'.';
                    break;
                case 'cancel':
                    $details = 'Cancel the '.$body.'.';
                    break;
                case 'view':
                    $details = 'View an instance of '.$body.'.';
                    break;
            }

            return [
                'method' => $item[0],
                'methods' => explode('|', $item[0]),
                'uri' => $item[1],
                'function' => $item[2],
                'name' => $item[3],
                'base' => $base,
                'body' => $body,
                'action' => $action ?? '',
                'details' => $details ?? '',
            ];
        });

        return $routes->where('name', '!=', null);
    }

    public function getGroupedRoutes(): Collection
    {
        $routes = [];
        foreach ($this->getRoutes() as $key => $item) {
            $base = $item['base'];
            if (!array_key_exists($base, $routes)) {
                $routes[$base] = collect([]);
            }
            $routes[$base]->push($item);
        }

        return collect($routes);
    }
}
