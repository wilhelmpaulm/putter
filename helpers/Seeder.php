<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class Seeder
{
    public static function up()
    {
        try {
            if (!Capsule::schema()->hasTable('requests')) {
                Capsule::schema()->create('requests', function ($table) {
                    $table->increments('id');

                    $table->integer('target_id')->nullable();
                    $table->integer('user_id');
                    $table->string('request');
                    $table->text('message')->nullable();

                    $table->integer('approved_by')->nullable();

                    $table->string('type')->default('staff');
                    $table->string('access')->default('staff');
                    $table->enum('status', ['pending', 'rejected', 'approved', 'cancelled'])->default('pending');

                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('blogs')) {
                Capsule::schema()->create('blogs', function ($table) {
                    $table->increments('id');
                    $table->integer('user_id');
                    $table->integer('views')->default(0);

                    $table->string('name')->nullable();
                    $table->string('label')->nullable();
                    $table->text('body')->nullable();

                    $table->text('image')->nullable();
                    $table->json('tags')->default('[]');

                    $table->enum('type', ['news', 'blog'])->default('blog');
                    $table->boolean('public')->default(1);
                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('roles')) {
                Capsule::schema()->create('roles', function ($table) {
                    $table->increments('id');

                    $table->string('name')->nullable();
                    $table->json('rights')->default('[]');
                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('discounts')) {
                Capsule::schema()->create('discounts', function ($table) {
                    $table->increments('id');

                    $table->integer('inventory_id');
                    $table->string('name')->nullable();
                    $table->string('label')->nullable();
                    $table->text('details')->nullable();

                    $table->text('image')->nullable();

                    $table->datetime('start_date')->nullable();
                    $table->datetime('end_date')->nullable();

                    $table->datetime('discount_start_date')->nullable();
                    $table->datetime('discount_end_date')->nullable();

                    $table->decimal('multiplier', 13, 2)->default(1.0);

                    $table->boolean('active')->default(1);
                    $table->boolean('public')->default(1);
                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('inventories')) {
                Capsule::schema()->create('inventories', function ($table) {
                    $table->increments('id');

                    $table->string('name');
                    $table->string('code')->default('');
                    $table->string('label')->default('');
                    $table->text('details')->nullable();

                    $table->float('latitude')->default(0.0);
                    $table->float('longitude')->default(0.0);
                    $table->text('location')->nullable();

                    $table->text('image')->default('default.png');

                    $table->decimal('base_price', 13, 2)->default('0.0');
                    $table->decimal('price', 13, 2)->default('0.0');

                    $table->integer('amount')->default(1);
                    $table->json('options')->default('{"pax_limit": 50, "slot_limit": 5, "limit": 5, "blocked_dates" : []}');
                    $table->string('string')->default('');

                    $table->enum('type', ['item', 'service', 'rental', 'facility', 'course'])->default('item');

                    $table->boolean('available')->default(1);
                    $table->boolean('active')->default(1);
                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('reservations')) {
                Capsule::schema()->create('reservations', function ($table) {
                    $table->increments('id');
                    $table->integer('user_id');
                    $table->integer('facility_id');
                    $table->integer('invoice_id')->nullable();

                    $table->string('name')->default('');
                    $table->string('label')->default('');
                    $table->text('details')->default('');

                    $table->integer('capacity')->default(0);
                    $table->text('image')->nullable('default.png');

                    $table->json('organizers')->default('[]');
                    $table->json('guests')->default('[]');

                    $table->json('tags')->default('[]');

                    $table->datetime('start_date')->nullable();
                    $table->datetime('end_date')->nullable();

                    $table->enum('type', ['reservation', 'teetime', 'banquet', 'tournament'])->default('reservation');
                    $table->enum('status', ['pending', 'approved', 'rejected', 'reviewed', 'cancelled'])->default('pending');
                    $table->enum('appearance', ['pending', 'showed', 'missed'])->default('pending');
                    $table->boolean('public')->default(1);
                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('invoices')) {
                Capsule::schema()->create('invoices', function ($table) {
                    $table->increments('id');

                    $table->integer('user_id')->nullable();
                    $table->integer('staff_id')->nullable();
                    $table->integer('paid_by')->nullable();

                    $table->json('details')->default('[]');
                    $table->text('message')->nullable();

                    $table->decimal('subtotal', 13, 2)->default(0.0);
                    $table->decimal('vat', 13, 2)->default(1.0);
                    $table->decimal('total', 13, 2)->default(0.0);
                    $table->decimal('current_balance', 13, 2)->default(0.0);

                    $table->datetime('start_date')->nullable();
                    $table->datetime('end_date')->nullable();

                    $table->enum('type', ['invoice', 'billing'])->default('invoice');
                    $table->enum('status', ['pending', 'paid', 'cancelled', ''])->default('pending');
                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('rentals')) {
                Capsule::schema()->create('rentals', function ($table) {
                    $table->increments('id');

                    $table->integer('inventory_id');
                    $table->integer('user_id')->nullable();
                    $table->integer('staff_id')->nullable();

                    $table->text('message')->nullable();

                    $table->datetime('start_date')->nullable();
                    $table->datetime('end_date')->nullable();
                    $table->datetime('returned_date')->nullable();

                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('messages')) {
                Capsule::schema()->create('messages', function ($table) {
                    $table->increments('id');

                    $table->integer('user_id')->nullable();
                    $table->string('sent_to');
                    $table->text('message');

                    $table->boolean('read')->default(0);

                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('comments')) {
                Capsule::schema()->create('comments', function ($table) {
                    $table->increments('id');

                    $table->integer('topic_id');
                    $table->text('up_votes')->default('[]');
                    $table->text('down_votes')->default('[]');
                    $table->integer('reply_to')->nullable();
                    $table->integer('user_id')->nullable();
                    $table->enum('type', ['review', 'member', 'post', 'blog', 'ticket'])->default('post');
                    $table->text('message');

                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('reports')) {
                Capsule::schema()->create('reports', function ($table) {
                    $table->increments('id');

                    $table->integer('user_id')->nullable();
                    $table->text('message');

                    $table->enum('type', ['member', 'inventory'])->default('request');
                    $table->enum('status', ['pending', 'resolved', 'rejected'])->default('pending');

                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('tickets')) {
                Capsule::schema()->create('tickets', function ($table) {
                    $table->increments('id');

                    $table->integer('user_id')->nullable();
                    $table->integer('target_id')->nullable();
                    $table->integer('staff_id')->nullable();

                    $table->string('title')->nullable();
                    $table->text('body')->nullable();

                    $table->enum('type', ['member', 'staff', 'inventory', 'facility'])->default('request');
                    $table->enum('status', ['pending', 'resolved', 'rejected', 'cancelled'])->default('pending');

                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('pings')) {
                Capsule::schema()->create('pings', function ($table) {
                    $table->increments('id');
                    $table->integer('user_id')->nullable();

                    $table->json('input')->nullable()->default(null);
                    $table->text('uri')->default('desktop');
                    $table->string('method')->default('GET');
                    $table->string('device')->default('desktop');

                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('access')) {
                Capsule::schema()->create('access', function ($table) {
                    $table->increments('id');

                    $table->string('type')->unique();
                    $table->text('details')->nullable();
                    $table->json('list')->default('[]');
                    $table->boolean('active')->default(1);

                    $table->timestamps();
                });
            }

            if (!Capsule::schema()->hasTable('users')) {
                Capsule::schema()->create('users', function ($table) {
                    $table->increments('id');
                    $table->integer('endorser_id')->nullable();
                    $table->integer('sponsor_id')->nullable();

                    $table->string('email')->unique();
                    $table->string('mobile')->nullable();
                    $table->string('password');

                    $table->enum('type', ['request', 'review', 'member', 'dependent', 'guest', 'staff', 'officer', 'accounting', 'maintenance'])->default('request');
                    $table->string('access')->default('unregistered');
                    $table->enum('gender', ['male', 'female'])->default('male');

                    $table->text('address');
                    $table->string('first_name');
                    $table->string('last_name');
                    $table->string('middle_name')->nullable();

                    $table->string('company')->nullable();
                    $table->string('occupation')->nullable();

                    $table->string('short')->nullable();
                    $table->string('color')->default('gray')->nullable();

                    $table->string('city')->nullable();
                    $table->string('zip_code')->nullable();
                    $table->string('country')->nullable();

                    $table->decimal('balance', 13, 2)->default(0.0);
                    $table->text('dependents')->default('{}')->nullable();

                    // for request type users
                    $table->text('details')->nullable();
                    $table->text('intent')->nullable();

                    $table->boolean('confirmed')->default(0);
                    $table->boolean('active')->default(0);
                    $table->boolean('member')->default(0);

                    $table->boolean('notifications')->default(1);
                    $table->boolean('emails')->default(1);
                    $table->boolean('messages')->default(1);

                    $table->text('image')->nullable();
                    $table->timestamps();
                });
            }
        } catch (Exception $e) {
            error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');
            Session::set('danger', $e->getMessage());
        }
    }

    public static function down()
    {
        Capsule::schema()->dropAllTables();
    }

    public static function seed()
    {
        $faker = Faker\Factory::create();
        $faker->seed(1234);

        try {
            if (Capsule::schema()->hasTable('access')) {
                foreach (['admin', 'member', 'dependent', 'guest', 'staff', 'officer', 'accounting', 'maintenance', 'unregistered'] as $x) {
                    $access = new Access();
                    $access->type = $x;
                    $access->details = $faker->text(80);
                    $access->active = 1;
                    // if (in_array($x, ['admin', 'staff', 'officer', 'accounting', 'maintenance'])) {
                    $access->list = json_encode( ["login_get", "login_post", "forgot_password_get", "request_membership_get", "register_get", "track_get", "logout_get", "register_post", "forgot_password_post", "request_membership_post", "requests_show", "requests_get", "requests_status_update", "requests_pending_get", "members_index", "members_status_update", "members_type_update", "members_role_update", "members_notifications_update", "members_messages_update", "members_emails_update", "members_show", "members_profile_get", "members_profile_edit", "members_profile_update", "members_comments_get", "members_comment_store", "members_endorsement_create_get", "members_endorsement_store", "dependents_get", "dependents_create", "dependents_store", "messages_get", "messages_read_all_update", "messages_read_update", "messages_create", "messages_store", "me_get", "me_independent_get", "me_independent_store", "me_requests_get", "me_settings_get", "me_reservations_get", "me_teetimes_get", "me_cancel_get", "me_cancel_post", "me_password_get", "me_password_post", "pos_get", "pos_post", "accounting_members_get", "accounting_adjust_get", "accounting_adjust_post", "billings_get", "billing_get", "billings_generate_get", "billings_generate_post", "inventories_get", "inventory_create", "inventory_delete", "inventory_show", "inventory_store", "inventory_update", "courses_get", "venues_get", "facilities_get", "facilities_create", "facilities_delete", "facilities_show", "facilities_store", "facilities_update", "discounts_get", "discounts_create", "discounts_store", "discounts_delete", "discounts_show", "discounts_update", "teetimes_index", "teetimes_create", "teetimes_store", "teetimes_cancel", "reports_profit_get", "reports_facilities_get", "reports_courses_get", "reports_golf_rounds_get", "reports_reservations_revenue_get", "reports_reservations_get", "reports_delinquency_get", "reports_delinquency_show", "reports_booking_usage_get", "reports_booking_appearance_get", "tournaments_index", "tournaments_store", "tournaments_create", "tournaments_cancel", "reservations_index", "reservations_create", "reservations_store", "reservations_cancel", "reservations_appearance_patch", "banquets_index", "banquets_create", "banquets_store", "banquets_cancel", "invoices_index", "invoices_show", "invoices_status_update", "events_teetimes_index", "events_reservations_index", "event_reservation_show", "news_index", "news_show", "blogs_index", "blogs_create", "blogs_edit", "blogs_delete", "blogs_store", "blogs_update", "config_dashboard_controller_get", "config_dashboard_controller_update", "config_dashboard_get", "config_traffic_get", "config_accounting_get", "config_accounting_update", "config_terms_get", "config_terms_update", "config_site_get", "config_site_update", "config_club_get", "config_club_update", "config_email_get", "config_email_update", "config_system_get", "config_system_update", "config_limits_get", "config_limits_update", "access_index", "access_create", "access_edit", "access_update", "access_delete", "access_store", "tickets_index", "tickets_create", "tickets_status_update", "tickets_show", "tickets_store", "tickets_comments_get", "error_get", "error_code_get", "home_get", "guest_get", "test_get", "terms_get", "contact_get"]);
                    // } elseif (in_array($x, ['member', 'dependent', 'guest'])) {
                    // $access->list = json_encode(['login_get', 'login_post', 'forgot_password_get', 'request_membership_get', 'register_get', 'track_get', 'logout_get', 'register_post', 'forgot_password_post', 'request_membership_post', 'requests_show', 'requests_get', 'requests_status_update', 'requests_pending_get', 'members_index', 'members_status_update', 'members_type_update', 'members_notifications_update', 'members_messages_update', 'members_emails_update', 'members_show', 'members_profile_get', 'members_comments_get', 'members_comment_store', 'members_endorsement_create_get', 'members_endorsement_store', 'dependents_get', 'dependents_create', 'dependents_store', 'messages_get', 'messages_read_all_update', 'messages_read_update', 'messages_create', 'messages_store', 'me_get', 'me_independent_get', 'me_independent_store', 'me_requests_get', 'me_settings_get', 'me_reservations_get', 'me_teetimes_get', 'me_cancel_get', 'me_cancel_post', 'me_password_get', 'me_password_post', 'accounting_members_get', 'billings_get', 'billing_get', 'inventories_get', 'inventory_show', 'courses_get', 'venues_get', 'facilities_get', 'facilities_show', 'discounts_get', 'discounts_show', 'teetimes_index', 'teetimes_create', 'teetimes_store', 'teetimes_cancel', 'tournaments_index', 'tournaments_create', 'tournaments_store', 'tournaments_cancel', 'reservations_index', 'reservations_create', 'reservations_store', 'reservations_cancel', 'banquets_index', 'banquets_create', 'banquets_store', 'banquets_cancel', 'invoices_index', 'invoices_create', 'invoices_status_update', 'events_teetimes_index', 'events_reservations_index', 'event_reservation_show', 'news_index', 'news_show', 'blogs_index', 'blogs_create', 'blogs_edit', 'blogs_delete', 'blogs_store', 'blogs_update', 'tickets_index', 'tickets_create', 'tickets_status_update', 'tickets_show', 'tickets_store', 'tickets_comments_get', 'error_get', 'error_code_get', 'home_get', 'guest_get', 'test_get', 'terms_get', 'contact_get']);
                    // } elseif (in_array($x, ['unregistered'])) {
                    //     $access->list = json_encode(['login_get', 'login_post', 'forgot_password_get', 'request_membership_get', 'register_get', 'track_get', 'logout_get', 'register_post', 'forgot_password_post', 'request_membership_post', 'requests_show', 'requests_get', 'requests_status_update', 'requests_pending_get', 'members_index', 'members_status_update', 'members_type_update', 'members_notifications_update', 'members_messages_update', 'members_emails_update', 'members_show', 'members_profile_get', 'members_comments_get', 'members_comment_store', 'members_endorsement_create_get', 'members_endorsement_store', 'dependents_get', 'dependents_create', 'dependents_store', 'messages_get', 'messages_read_all_update', 'messages_read_update', 'messages_create', 'messages_store', 'me_get', 'me_independent_get', 'me_independent_store', 'me_requests_get', 'me_settings_get', 'me_reservations_get', 'me_teetimes_get', 'me_cancel_get', 'me_cancel_post', 'me_password_get', 'me_password_post', 'accounting_members_get', 'billings_get', 'billing_get', 'inventories_get', 'inventory_show', 'courses_get', 'venues_get', 'facilities_get', 'facilities_show', 'discounts_get', 'discounts_show', 'teetimes_index', 'teetimes_create', 'teetimes_store', 'teetimes_cancel', 'tournaments_index', 'tournaments_create', 'tournaments_store', 'tournaments_cancel', 'reservations_index', 'reservations_create', 'reservations_store', 'reservations_cancel', 'banquets_index', 'banquets_create', 'banquets_store', 'banquets_cancel', 'invoices_index', 'invoices_create', 'invoices_status_update', 'events_teetimes_index', 'events_reservations_index', 'event_reservation_show', 'news_index', 'news_show', 'blogs_index', 'blogs_create', 'blogs_edit', 'blogs_delete', 'blogs_store', 'blogs_update', 'tickets_index', 'tickets_create', 'tickets_status_update', 'tickets_show', 'tickets_store', 'tickets_comments_get', 'error_get', 'error_code_get', 'home_get', 'guest_get', 'test_get', 'terms_get', 'contact_get']);
                    // }
                    $access->save();
                }
            }

            if (Capsule::schema()->hasTable('inventories')) {
                foreach (str_split('12345678') as $x) {
                    $facility = new Facility();
                    $facility->name = $faker->streetName;
                    $facility->label = $faker->text(80);
                    $facility->details = $faker->text(240);
                    $facility->latitude = $faker->latitude;
                    $facility->longitude = $faker->longitude;
                    $facility->image = 'default.png';
                    $facility->price = $faker->randomFloat(2, 100, 200);
                    $facility->base_price = $facility->price * 0.9;
                    $facility->type = $faker->randomElement(['facility', 'course']);
                    $facility->available = 1;
                    $facility->active = 1;
                    $facility->save();
                }

                foreach (str_split('jksdfbkgsbfdghdfgajdflkjhdsafafdsg') as $x) {
                    $facility = new Facility();
                    $facility->name = $faker->cityPrefix.' '.$faker->streetName.' '.$faker->randomElement(['juice', 'pasta', 'rum', 'meal']);
                    $facility->label = $faker->text(80);
                    $facility->details = $faker->text(240);
                    $facility->image = 'default.png';
                    $facility->price = $faker->randomFloat(2, 100, 200);
                    $facility->base_price = $facility->price * 0.9;
                    $facility->type = 'item';
                    $facility->amount = 5;
                    $facility->available = 1;
                    $facility->active = 1;
                    $facility->save();
                }
            }

            if (Capsule::schema()->hasTable('users')) {
                $user = new User();
                $user->email = 'admin@example.com';
                $user->mobile = $faker->e164PhoneNumber;
                $user->password = 'password';
                $user->color = $faker->randomElement(getColorList());
                $user->type = 'officer';
                $user->access = 'officer';
                $user->gender = 'male';
                $user->first_name = 'admin';
                $user->last_name = $faker->lastName;
                $user->middle_name = $faker->lastName;
                $user->address = $faker->address;
                $user->balance = 0.0;
                $user->intent = $faker->text($maxNbChars = 180);
                $user->details = $faker->text($maxNbChars = 180);
                $user->active = 1;
                $user->short = strtoupper($user->first_name[0].$user->last_name[0]);

                $user->save();

                foreach (str_split('12345678901234567890qwertyuiophjfdbnsjkdfhngdfg') as $x) {
                    $first_name = $faker->firstName;
                    $user = new User();
                    $user->email = "$first_name@example.com";
                    $user->mobile = $faker->e164PhoneNumber;
                    $user->password = 'password';
                    $user->color = $faker->randomElement(getColorList());
                    $user->type = $faker->randomElement(['member', 'dependent', 'guest', 'staff', 'officer', 'accounting', 'maintenance']);
                    $user->access = $user->type;
                    $user->gender = $faker->randomElement(['male', 'female']);
                    $user->first_name = $first_name;
                    $user->last_name = $faker->lastName;
                    $user->middle_name = $faker->lastName;
                    $user->address = $faker->address;
                    $user->intent = $faker->text($maxNbChars = 180);
                    $user->details = $faker->text($maxNbChars = 180);
                    $user->short = strtoupper($user->first_name[0].$user->last_name[0]);
                    $user->balance = 0.0;
                    $user->active = 1;

                    if ($user->type == 'dependent') {
                        $user->sponsor_id = 1;
                        $user->endorser_id = 1;
                    }

                    if (in_array($user->type, ['member', 'guest'])) {
                        $user->company = $faker->company;
                        $user->occupation = $faker->jobTitle;
                        $user->balance = $faker->randomFloat(2, 99, 999) * $faker->randomElement([1, -1, 1.5]);
                    }

                    if (in_array($user->type, ['dependent'])) {
                        $user->company = $faker->company;
                        $user->occupation = $faker->jobTitle;
                        $user->balance = 0;
                    }

                    $user->save();
                }
            }
        } catch (Exception $e) {
            error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');
            Session::set('danger', $e->getMessage());
        }
    }

    public static function sendBack()
    {
        User::logout();
        Session::set('success', 'Database restart and seeding complete.');
        sendTo('login');
    }

    public static function seedSendBack()
    {
        self::down();
        self::up();
        self::seed();
        self::sendBack();
    }

    public static function seedEnd()
    {
        self::down();
        self::up();
        self::seed();
        User::logout();

        return 'done';
    }
}
