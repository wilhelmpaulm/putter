<?php

class Crypt
{
    public static function encrypt($data = null)
    {
        if ($data == null) {
            return false;
        }

        $method = 'AES-256-CBC';
        $data_encoded = utf8_encode($data);
        $len = openssl_cipher_iv_length($method);
        $isCryptoStrong = false;
        $iv = openssl_random_pseudo_bytes($len, $isCryptoStrong);
        $data_encrypted = openssl_encrypt($data_encoded, $method, Config::get('system', 'app_secret'), 0, $iv);

        return base64_encode($iv.$data_encrypted);
    }

    public static function decrypt(string $data)
    {
        if ($data == null) {
            return false;
        }

        $data_decoded = base64_decode($data);
        $method = 'AES-256-CBC';
        $len = openssl_cipher_iv_length($method);
        $iv = substr($data_decoded, 0, $len);

        return openssl_decrypt(substr($data_decoded, $len), $method, Config::get('system', 'app_secret'), 0, $iv);
    }
}
