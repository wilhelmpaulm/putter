<?php

class Cookie
{
    public static function get($key = null)
    {
        if (isset($_COOKIE)) {
            if ($key) {
                $value = Crypt::decrypt($_COOKIE[$key]);

                return $value;
            } else {
                $cookie = [];
                foreach ($_COOKIE as $key => $value) {
                    $cookie[$key] = Crypt::decrypt($value);
                }

                return $cookie;
            }

            return false;
        }

        return false;
    }

    public static function set(string $key, $value = null)
    {
        if (isset($_COOKIE)) {
            if ($value) {
                $value = Crypt::encrypt($value);
            } else {
                $value = null;
            }
            setcookie($key, $value, time() + 3600, '/');

            return true;
        }

        return false;
    }

    public static function clear(string $key)
    {
        $_COOKIE[$key] = ' ';
        $_COOKIE[$key] = null;
        unset($_COOKIE[$key]);
        setcookie($key, '', time() - 3600, '/');

        return true;
    }

    public static function stop()
    {
        if (isset($_COOKIE)) {
            foreach ($_COOKIE as $key => $value) {
                unset($_COOKIE[$key]);
                setcookie($key, '', time() - 3600, '/');
            }
        }

        return true;
    }
}
