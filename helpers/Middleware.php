<?php

class Middleware
{
    const STAFF_TYPES = ['accounting', 'officer', 'staff', 'admin'];
    const FORBIDDEN_ERROR_HEADER = "Unauthorized access to page";
    const FORBIDDEN_ERROR_BODY = "If you wish to access this page for a concern please contact system admin";

    public static function authorization(string $route)
    {
        if (Auth::isLoggedIn() && !Auth::hasAccess($route)) {
            return IndexController::getError(403, self::FORBIDDEN_ERROR_HEADER, self::FORBIDDEN_ERROR_BODY);
        }

        return true;
    }

    public static function teetimeLimit(string $route)
    {
        $routes = ['teetimes_create', 'teetimes_store'];
        if (!in_array($route, $routes)) {
            return true;
        }
        if (in_array(user('type'), self::STAFF_TYPES)) {
            return true;
        }
        if (in_array($route, $routes)) {
            $count = Reservation::where('user_id', user('id'))->where('type', 'teetime')->where('end_date', '>', date('Y-m-d H:i:s'))->count();
            if ($count >= Config::get('limits', 'teetimes')) {
                Session::set('danger', ErrorMessages::RESERVATION_LIMIT_ERROR);
                sendTo('teetimes');
            }
        }

        return true;
    }

    public static function tournamentLimit(string $route)
    {
        $routes = ['tournaments_create', 'tournaments_store'];
        if (!in_array($route, $routes)) {
            return true;
        }
        if (in_array(user('type'), self::STAFF_TYPES)) {
            return true;
        }
        if (in_array($route, $routes)) {
            $count = Reservation::where('user_id', user('id'))->where('type', 'tournament')->where('end_date', '>', date('Y-m-d H:i:s'))->count();
            if ($count >= Config::get('limits', 'tournaments')) {
                Session::set('danger', ErrorMessages::RESERVATION_LIMIT_ERROR);
                sendTo('tournaments');
            }
        }

        return true;
    }

    public static function reservationLimit(string $route)
    {
        $routes = ['reservations_create', 'reservations_store'];
        if (!in_array($route, $routes)) {
            return true;
        }
        if (in_array(user('type'), self::STAFF_TYPES)) {
            return true;
        }
        if (in_array($route, $routes)) {
            $count = Reservation::where('user_id', user('id'))->where('type', 'reservation')->where('end_date', '>', date('Y-m-d H:i:s'))->count();
            if ($count >= Config::get('limits', 'reservations')) {
                Session::set('danger', ErrorMessages::RESERVATION_LIMIT_ERROR);
                sendTo('reservations');
            }
        }

        return true;
    }

    public static function banquetLimit(string $route)
    {
        $routes = ['banquets_create', 'banquets_store'];
        if (!in_array($route, $routes)) {
            return true;
        }
        if (in_array(user('type'), self::STAFF_TYPES)) {
            return true;
        }
        if (in_array($route, $routes)) {
            $count = Reservation::where('user_id', user('id'))->where('type', 'banquet')->where('end_date', '>', date('Y-m-d H:i:s'))->count();
            if ($count >= Config::get('limits', 'banquets')) {
                Session::set('danger', ErrorMessages::RESERVATION_LIMIT_ERROR);
                sendTo('banquets');
            }
        }

        return true;
    }
}
