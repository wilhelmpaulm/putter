<?php

// TODO: clean functions
function getReservationTypes()
{
    return ['teetime', 'tournament', 'reservation', 'banquet'];
}

function getUserTypes()
{
    return ['member', 'dependent', 'guest', 'staff', 'officer', 'accounting'];
}

function getStaffTypes()
{
    return ['staff', 'officer', 'accounting', 'maintenance'];
}

function getClientTypes()
{
    return ['member', 'dependent', 'guest'];
}

function getYearList()
{
    return ['2019', '2018', '2017', '2016'];
}

function getMonthsShort()
{
    return [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec',
    ];
}

function getMonthsFull()
{
    return [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July ',
        'August',
        'September',
        'October',
        'November',
        'December',
    ];
}

// TODO: clean functions
function getColorList()
{
    return ['blue', 'azure', 'indigo', 'purple', 'pink', 'red', 'orange', 'yellow', 'lime', 'green', 'teal', 'cyan', 'gray', 'gray-dark'];
}

function getRandomColor()
{
    $items = getColorList();

    return $items[array_rand($items)];
}

function padId($str)
{
    return str_pad($str, 6, '0', STR_PAD_LEFT);
}

function linkTo($base = '', $data = [])
{
    if ($base == 'back') {
        return urlencode($_SERVER['HTTP_REFERER']);
    } else {
        $append = '';
        if ($data) {
            $append .= '?';
            foreach ($data as $key => $value) {
                $append .= "&{$key}=".urlencode((string) $value);
            }
        }

        return "http://{$_SERVER['HTTP_HOST']}/{$base}".$append;
    }
}

function sendTo($data)
{
    if ($data == 'back') {
        echo '<script>location.href = document.referrer;</script>';
        die();
    } else {
        header("Location: http://{$_SERVER['HTTP_HOST']}/{$data}");
    }
    die();
}

function getPage($data)
{
    $dir = $_SERVER['DOCUMENT_ROOT'];
    include $dir.'/views/'.$data.'.php';
}

function linkPage($data, $php = true)
{
    $dir = $_SERVER['DOCUMENT_ROOT'];
    if ($php) {
        return "{$dir}/views/{$data}.php";
    } else {
        return "{$dir}/views/{$data}";
    }
}

function linkPublic($data)
{
    $dir = $_SERVER['DOCUMENT_ROOT'];

    return "http://{$_SERVER['HTTP_HOST']}/public/{$data}";
}

function getRoutes()
{
    // save routes to the session
    return Router::Router()->getRoutes() ?? [];
}

function getRouteGroups()
{
    return Router::Router()->getGroupedRoutes() ?? [];
}

//////////////////////////
// HELPER FUNCTIONS
//////////////////////////
function user($key = null)
{
    if (!Session::get('user')) {
        return null;
    }

    $user = Auth::user();

    if ($key && $user->$key) {
        if ($key == 'id') {
            $user->id = (int) $user->id;
        }

        return $user->$key;
    }

    if ($key == null) {
        return $user;
    }

    return false;
}

function url()
{
    return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}

function addOrdinalNumberSuffix($num)
{
    if (!in_array(($num % 100), array(11, 12, 13))) {
        switch ($num % 10) {
            // Handle 1st, 2nd, 3rd
            case 1:return $num.'st';
            case 2:return $num.'nd';
            case 3:return $num.'rd';
        }
    }

    return $num.'th';
}

function getResponseCodeMessage($code = 200)
{
    $http_status_codes = [100 => 'Continue', 101 => 'Switching Protocols', 102 => 'Processing', 200 => 'OK', 201 => 'Created', 202 => 'Accepted', 203 => 'Non-Authoritative Information', 204 => 'No Content', 205 => 'Reset Content', 206 => 'Partial Content', 207 => 'Multi-Status', 300 => 'Multiple Choices', 301 => 'Moved Permanently', 302 => 'Found', 303 => 'See Other', 304 => 'Not Modified', 305 => 'Use Proxy', 306 => '(Unused)', 307 => 'Temporary Redirect', 308 => 'Permanent Redirect', 400 => 'Bad Request', 401 => 'Unauthorized', 402 => 'Payment Required', 403 => 'Forbidden', 404 => 'Not Found', 405 => 'Method Not Allowed', 406 => 'Not Acceptable', 407 => 'Proxy Authentication Required', 408 => 'Request Timeout', 409 => 'Conflict', 410 => 'Gone', 411 => 'Length Required', 412 => 'Precondition Failed', 413 => 'Request Entity Too Large', 414 => 'Request-URI Too Long', 415 => 'Unsupported Media Type', 416 => 'Requested Range Not Satisfiable', 417 => 'Expectation Failed', 418 => "I'm a teapot", 419 => 'Authentication Timeout', 420 => 'Enhance Your Calm', 422 => 'Unprocessable Entity', 423 => 'Locked', 424 => 'Method Failure', 425 => 'Unordered Collection', 426 => 'Upgrade Required', 428 => 'Precondition Required', 429 => 'Too Many Requests', 431 => 'Request Header Fields Too Large', 444 => 'No Response', 449 => 'Retry With', 450 => 'Blocked by Windows Parental Controls', 451 => 'Unavailable For Legal Reasons', 494 => 'Request Header Too Large', 495 => 'Cert Error', 496 => 'No Cert', 497 => 'HTTP to HTTPS', 499 => 'Client Closed Request', 500 => 'Internal Server Error', 501 => 'Not Implemented', 502 => 'Bad Gateway', 503 => 'Service Unavailable', 504 => 'Gateway Timeout', 505 => 'HTTP Version Not Supported', 506 => 'Variant Also Negotiates', 507 => 'Insufficient Storage', 508 => 'Loop Detected', 509 => 'Bandwidth Limit Exceeded', 510 => 'Not Extended', 511 => 'Network Authentication Required', 598 => 'Network read timeout error', 599 => 'Network connect timeout error'];
    $http_status_code = $http_status_codes[$code];
    if ($http_status_code) {
        return $http_status_code;
    } else {
        return false;
    }
}

function html_response($html, $code = 200)
{
    // clear the old headers
    // header_remove();
    // set the actual code
    http_response_code($code);
    // set the header to make sure cache is forced
    header('Cache-Control: no-transform,public,max-age=300,s-maxage=900');
    // ok, validation error, or failure
    header('Status: '.$code.' '.getResponseCodeMessage($code));

    if ($code > 300) {
        sendTo("error/{$code}");
    } else {
        require $html;
    }
}

function json_response($data = null, $message = null, $code = 200)
{
    // clear the old headers
    header_remove();
    // set the actual code
    http_response_code($code);
    // set the header to make sure cache is forced
    header('Cache-Control: no-transform,public,max-age=300,s-maxage=900');
    // treat this as json
    header('Content-Type: application/json');
    // ok, validation error, or failure
    header('Status: '.$code.' '.getResponseCodeMessage($code));
    // return the encoded json
    return json_encode([
        'status' => $code < 300, // success or not?
        'data' => $data,
        'message' => $message ?? getResponseCodeMessage($code),
    ]);
}
