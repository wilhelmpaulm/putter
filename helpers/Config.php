<?php

class Config
{
    public static function get(string $type, $key = null)
    {
        if ($GLOBALS && !empty($GLOBALS['config'])) {
            $json = $GLOBALS['config'];
        } else {
            $string = file_get_contents(__DIR__.'/../config.json');
            $json = json_decode($string);
            $GLOBALS['config'] = $json;
        }

        return $key ? $json->$type->$key : $json->$type;
    }

    public static function set(string $type, string $key, $value = null)
    {
        $json = self::get($type);
        $json->$key = $value;
        $GLOBALS['config']->$type = $json;

        return $GLOBALS['config']->$type;
    }

    public static function save()
    {
        try {
            $fp = fopen(__DIR__.'/../config.json', 'w');
            fwrite($fp, json_encode($GLOBALS['config'], JSON_UNESCAPED_UNICODE));

            return fclose($fp);
        } catch (Exception $e) {
            error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');
            echo $e->getTraceAsString();

            return false;
        }
    }
}
