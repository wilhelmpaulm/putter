<?php

class Session
{
    public static function get($name = null, $key = null)
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (isset($_SESSION)) {
            if (isset($_SESSION[$name])) {
                if ($name != null && $key != null) {
                    $value = json_decode(Crypt::decrypt($_SESSION[$name]), true)[$key];

                    return $value;
                } elseif ($name != null) {
                    $value = json_decode(Crypt::decrypt($_SESSION[$name]), true);

                    return $value;
                } else {
                    $session = [];
                    foreach ($_SESSION as $key => $value) {
                        $session[$key] = json_decode(Crypt::decrypt($value), true);
                    }

                    return $session;
                }
            }

            return false;
        }

        return false;
    }

    public static function set(string $key, $value = null)
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (isset($_SESSION)) {
            if ($value) {
                $value = Crypt::encrypt(json_encode($value));
            } else {
                $value = null;
            }

            $_SESSION[$key] = $value;

            return true;
        }

        return false;
    }

    public static function pop(string $key)
    {
        $var = self::get($key);
        self::clear($key);

        return $var;
    }

    public static function clear(string $key)
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION[$key] = ' ';
        $_SESSION[$key] = null;

        return true;
    }

    public static function stop()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        session_cache_expire();
        if (isset($_SESSION)) {
            session_destroy();
        }

        return true;
    }
}
