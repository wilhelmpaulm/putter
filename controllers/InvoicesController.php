<?php

class InvoicesController implements ControllerInterface
{
    public static function index()
    {
        Auth::check();
        if (in_array(user('type'), ['member', 'dependent', 'guest'])) {
            $items = Invoice::where('type', 'invoice')
                ->where(function ($query) {
                    $query->where('user_id', user('id'))
                        ->orWhere('paid_by', user('id'));
                })
                ->orderBy('status', 'desc')->orderBy('id', 'desc')->get();
        } else {
            $items = Invoice::where('type', 'invoice')
                ->orderBy('status', 'desc')
                ->orderBy('id', 'desc')
                ->get();
        }

        include linkPage('invoices/list');
    }

    public static function show(int $id)
    {
        Auth::check();

        $item = Invoice::find($id);
        if (!$item) {
            return IndexController::getError(404);
        }

        include linkPage('invoices/view');
    }

    public static function create()
    {
        return false;
    }

    public static function store()
    {
        return false;
    }

    public static function edit(int $id)
    {
        return false;
    }

    public static function update(int $id)
    {
        return false;
    }

    public static function delete(int $id)
    {
        return false;
    }

    public static function updateStatus(int $id, string $status)
    {
        Auth::check();
        $item = Invoice::find($id);
        $item->status = $status;

        $user = User::find((int) $item->paid_by);

        if ($status == 'paid') {
            $user->balance -= $item->total;
            $user->save();

            $item->current_balance = $user->balance;
        }

        $item->status = $status;
        $item->save();

        $body = user('first_name').' marked your'
        .' invoice with the ID: '.padId($id)
            ." as {$status}.";

        if ($user->balance < 0) {
            $body .= ' Please make sure to settle your negative'
                .' balance with accounting.';
        }

        Message::send(null, $body, [$item->user_id]);

        $body = user('first_name').' has marked'
        .' invoice with the ID: '.padId($id)
            ." as {$status}.";
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'The invoice was updated');
        sendTo('invoices');
    }
}
