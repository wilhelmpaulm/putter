<?php

class InventoryController
{
    public static function index()
    {
        Auth::check();
        $items = Inventory::orderBy('id', 'desc')->whereNotIn('type', ['course', 'facility'])->get();

        include linkPage('inventory/list');
    }

    public static function create()
    {
        Auth::check();
        $types = ['item', 'service', 'rental'];

        include linkPage('inventory/add');
    }

    public static function store()
    {
        Auth::check();
        $input = Input::get();
        $inventory = new Inventory();

        if ($input['image']['name']) {
            $uploadedImage = File::upload('image');
            $inventory->image = $uploadedImage['name'];
        }

        $inventory->name = $input['name'];
        $inventory->label = $input['label'];
        $inventory->code = $input['code'];
        $inventory->details = $input['details'];
        $inventory->location = $input['location'];
        $inventory->image = $uploadedImage['name'];
        $inventory->amount = $input['amount'];
        $inventory->base_price = $input['base_price'];
        $inventory->price = $input['price'];
        $inventory->type = $input['type'];
        $inventory->available = $input['available'] ?? 0;
        $inventory->active = $input['active'] ?? 0;
        $inventory->save();

        $body = user('first_name').' has added a new'
        ." inventory item: {$input['name']}"
        .' (#'.padId($inventory->id).').';
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'Inventory item was added to the system');
        sendTo('back');
    }

    public static function show(int $id)
    {
        Auth::check();
        $item = Inventory::find($id);

        if (!$item) {
            return IndexController::getError(404);
        }

        $types = ['item', 'service', 'rental'];
        include linkPage('inventory/edit');
    }

    public static function delete(int $id)
    {
        Auth::check();
        $item = Inventory::find($id);
        $item->delete();

        $body = user('first_name').' has deleted'
        ." inventory item: {$item->name}"
        .' (#'.padId($item->id).').';
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'The inventory item was deleted from the system');
        sendTo('inventory');
    }

    public static function update(int $id)
    {
        Auth::check();
        $input = Input::get();
        $inventory = Inventory::find($id);

        if ($input['image']['name'] && $input['image']['name'] != $inventory->image) {
            $uploadedImage = File::upload('image');
            $inventory->image = $uploadedImage['name'];
        }

        $inventory->name = $input['name'];
        $inventory->label = $input['label'];
        $inventory->code = $input['code'];
        $inventory->details = $input['details'];
        $inventory->location = $input['location'];
        $inventory->amount = $input['amount'];
        $inventory->base_price = $input['base_price'];
        $inventory->price = $input['price'];
        $inventory->type = $input['type'];
        $inventory->available = $input['available'] ?? 0;
        $inventory->active = $input['active'] ?? 0;
        $inventory->save();

        $body = user('first_name').' has updated an'
        ." inventory item: {$input['name']}"
        .' (#'.padId($inventory->id).').';
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'Inventory item was updated');
        sendTo('back');
    }
}
