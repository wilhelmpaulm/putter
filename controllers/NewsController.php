<?php

class NewsController implements ControllerInterface
{
    public static function index()
    {
        $type = $type ?? 'all';
        $startDate = $startDate ?? date('Y-m-d H:i:s', strtotime('-7 day'));
        $endDate = $endDate ?? date('Y-m-d H:i:s', strtotime('+7 day'));
        if ($type && $startDate && $endDate) {
            if ($type == 'all') {
                $items = Blog::orderBy('id', 'desc')
                    ->where('created_at', '>=', $startDate)
                    ->where('created_at', '<=', $endDate)
                    ->where('public', 1)
                    ->get();
            } else {
                $items = Blog::orderBy('id', 'desc')
                    ->where('created_at', '>=', $startDate)
                    ->where('created_at', '<=', $endDate)
                    ->where('type', $type)
                    ->where('public', 1)
                    ->get();
            }
        } else {
            $items = Blog::orderBy('id', 'desc')
                ->where('type', 'blog')
                ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-7 day')))
                ->where('created_at', '<=', date('Y-m-d H:i:s', strtotime('+7 day')))
                ->where('public', 1)
                ->get();
        }

        include linkPage('news/list');
    }

    public static function show(int $id)
    {
        $item = Blog::find($id);
        ++$item->views;
        $item->save();
        include linkPage('news/view');
    }

    public static function create()
    {
        return false;
    }

    public static function store()
    {
        return false;
    }

    public static function edit(int $id)
    {
        return false;
    }

    public static function update(int $id)
    {
        return false;
    }

    public static function delete(int $id)
    {
        return false;
    }
}
