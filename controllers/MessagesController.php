<?php

class MessagesController
{
    public static function index(string $status)
    {
        Auth::check();
        if ($status == 'system') {
            $messages = Message::where('user_id', null)
                ->where('read', 0)
                ->where('sent_to', user('id'))
                ->orderBy('id', 'desc')
                ->get();
        } elseif ($status == 'unread') {
            $messages = Message::where('read', 0)
                ->where('user_id', '!=', null)
                ->where('sent_to', user('id'))
                ->orderBy('id', 'desc')
                ->get();
        } else {
            $messages = Message::where('read', 1)
                ->where('sent_to', user('id'))
                ->orderBy('id', 'desc')
                ->get();
        }

        include linkPage('messages/index');
    }

    public static function updateReadAll()
    {
        Auth::check();
        $message = Message::where('sent_to', user('id'))
            ->where('read', 0)
            ->update(['read' => 1]);
        Session::set('info', 'Marked all messages as read');
        sendTo('back');
    }

    public static function updateRead(int $id)
    {
        Auth::check();
        $message = Message::find($id);
        $message->read = 1;
        $message->save();
        Session::set('info', 'Message marked as read');
        if (Input::get('user_id')) {
            sendTo('messages/send?user_id='.Input::get('user_id'));
        }
        sendTo('messages/read');
    }

    public static function create()
    {
        Auth::check();
        $status = 'send';
        include linkPage('messages/send');
    }

    public static function store()
    {
        Auth::check();
        $status = 'send';
        Message::send(user('id'), Input::get('message'), Input::get('sent_to'));
        Session::set('success', 'Message sent');
        sendTo('messages/send');
    }
}
