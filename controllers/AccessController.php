<?php

class AccessController implements ControllerInterface
{
    public static function index()
    {
        $types = Access::all();
        $routes = getRoutes();
        $routeGroups = getRouteGroups();

        include linkPage('access/index');
    }

    public static function show(int $id)
    {
        $type = Access::findOrFail($id);
        include linkPage('access/index');
    }

    public static function create()
    {
        Auth::check();
        $type = Input::get('type') ?? '';
        $details = Input::get('details') ?? '';
        include linkPage('access/create');
    }

    public static function store()
    {
        Auth::check();
        if (Input::get('password') != user('password')) {
            Session::set('danger', 'Invalid password');
            sendTo('back');
        }
        $input = Input::get();
        $access = new Access();
        $access->type = $input['type'];
        $access->details = $input['details'];
        $access->save();

        Session::set('success', 'Access role created');
        sendTo('access');
    }

    public static function edit(int $id)
    {
        $type = Access::findOrFail($id);
        $routes = getRoutes();
        $routeGroups = getRouteGroups();

        include linkPage('access/edit');
    }

    public static function update(int $id)
    {
        $input = Input::get();
        $type = Access::findOrFail($id);
        $type->updateList($input['routes']);

        sendTo('back');
    }

    public static function delete(int $id)
    {
        $type = Access::findOrFail($id);
        $type->delete();

        $body = user('first_name').' has deleted'
            ." access role: {$type->name}"
            .' (#'.padId($type->id).').';
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'The access role was deleted from the system');
        sendTo('access');

        return false;
    }
}
