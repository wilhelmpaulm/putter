<?php

class ConfigController
{
    public static function getDashboard()
    {
        Auth::check(['officer', 'accounting', 'staff']);
        include linkPage('config/dashboard');
    }

    public static function getTraffic()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $logs = Ping::orderBy('id', 'desc')->limit(1000)->get();
        include linkPage('config/traffic');
    }

    public static function getAccounting()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $club = Config::get('accounting');
        include linkPage('config/accounting');
    }

    public static function updateAccounting()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $input = Input::get();
        foreach ($input as $key => $val) {
            Config::set('accounting', $key, $val);
        }
        Config::save();

        Session::set('success', 'config details updated');
        sendTo('config/accounting');
    }

    public static function getTerms()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $club = Config::get('terms');
        include linkPage('config/terms');
    }

    public static function updateTerms()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $input = Input::get();
        foreach ($input as $key => $val) {
            Config::set('terms', $key, $val);
        }
        Config::save();

        Session::set('success', 'config details updated');
        sendTo('config/site');
    }

    public static function getSite()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $club = Config::get('site');
        include linkPage('config/site');
    }

    public static function updateSite()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $input = Input::get();
        foreach ($input as $key => $val) {
            Config::set('site', $key, $val);
        }
        Config::save();

        Session::set('success', 'config details updated');
        sendTo('config/site');
    }

    public static function getClub()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $club = Config::get('club');
        include linkPage('config/club');
    }

    public static function updateClub()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $input = Input::get();
        foreach ($input as $key => $val) {
            Config::set('club', $key, $val);
        }
        Config::save();

        Session::set('success', 'config details updated');
        sendTo('config/club');
    }

    public static function getEmail()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $email = Config::get('email');
        include linkPage('config/email');
    }

    public static function updateEmail()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $input = Input::get();
        foreach ($input as $key => $val) {
            Config::set('email', $key, $val);
        }
        Config::save();

        Session::set('success', 'config details updated');
        sendTo('config/email');
    }

    public static function getSystem()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $system = Config::get('system');
        include linkPage('config/system');
    }

    public static function updateSystem()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $input = Input::get();

        Config::set('system', 'maps', $input['maps']);
        Config::set('system', 'weather', $input['weather']);
        Config::set('system', 'mail', $input['mail'] ?? '0');
        Config::set('system', 'message', $input['message'] ?? '0');
        Config::set('system', 'seed', $input['seed'] ?? '0');
        Config::set('system', 'traffic', $input['traffic'] ?? '0');
        Config::set('system', 'debug', $input['debug'] ?? '0');
        Config::save();

        if ($input['favicon']['name']) {
            $uploadedImage = File::upload('favicon', 'app/uploads');
            Config::set('system', 'favicon', $uploadedImage['name']);
        }

        if ($input['logo']['name']) {
            $uploadedImage = File::upload('logo', 'app/uploads');
            Config::set('system', 'logo', $uploadedImage['name']);
        }

        if ($input['header']['name']) {
            $uploadedImage = File::upload('header', 'app/uploads');
            Config::set('system', 'header', $uploadedImage['name']);
        }

        Session::set('success', 'config details updated');
        sendTo('config/system');
    }

    public static function getDashboardController()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $dashboard = Config::get('dashboard');
        include linkPage('config/dashboardController');
    }

    public static function updateDashboardController()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $input = Input::get();

        Config::set('dashboard', 'requests', $input['requests'] ?? '0');
        Config::set('dashboard', 'revenue', $input['revenue'] ?? '0');
        Config::set('dashboard', 'invoices', $input['invoices'] ?? '0');
        Config::set('dashboard', 'today', $input['today'] ?? '0');
        Config::set('dashboard', 'today', $input['today'] ?? '0');
        Config::set('dashboard', 'today_pending', $input['today_pending'] ?? '0');
        Config::set('dashboard', 'today_completed', $input['today_completed'] ?? '0');
        Config::save();

        Session::set('success', 'config details updated');
        sendTo('config/dashboard/controller');
    }

    public static function getLimits()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $limits = Config::get('limits');
        include linkPage('config/limits');
    }

    public static function updateLimits()
    {
        Auth::check(['officer', 'admin', 'administrator']);
        $input = Input::get();

        Config::set('limits', 'teetimes', $input['teetimes']);
        Config::set('limits', 'tournaments', $input['tournaments'] ?? '0');
        Config::set('limits', 'reservations', $input['reservations'] ?? '0');
        Config::set('limits', 'reservations_pax', $input['reservations_pax'] ?? '500');
        Config::set('limits', 'banquets', $input['banquets'] ?? '0');
        Config::set('limits', 'banquets_pax', $input['banquets_pax'] ?? '500');
        Config::set('limits', 'tournaments_slot_limit', $input['reservations_slot_limit'] ?? '5');
        Config::set('limits', 'reservations_slot_limit', $input['reservations_slot_limit'] ?? '5');
        Config::set('limits', 'vat', $input['vat'] ?? '20.0');
        Config::save();

        Session::set('success', 'config details updated');
        sendTo('config/limits');
    }
}
