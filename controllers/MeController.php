<?php

class MeController
{
    public static function index()
    {
        Auth::check();
        $member = User::find(user('id'));

        include linkPage('me/index');
    }

    public static function getIndependent()
    {
        Auth::check();
        include linkPage('me/independent');
    }

    public static function storeIndependent()
    {
        Auth::check();
        $request = Request::where('request', 'independent_request')
            ->where('status', 'pending')
            ->first();
        if ($request) {
            Session::set('warning', 'You currently have a pending similar'
                .' request. Please wait for the request to be acknowledged'
                .' before proceeding with this new request.');
            sendTo('back');
        }

        if (Input::get('password') == user('password')) {
            $request_id = Request::make(user('id'), 'independent_request', 'officer');
            $body = 'Thank you for expressing your interest to become an'
            .' independent member.'
            .' Please keep an eye on your email for updates.'
            .' You may track your request at '.linkTo("track/$request_id");
            $sent = Mail::send('Request To Be An Independent Member', [user('email')], $body);
            Message::send(null, $body, [user('id')]);

            $body2 = user('full_name').' has submitted a request to'
                .' become an independent member';
            Message::sendToTypes(null, $body2, ['officer', 'accounting']);
            if (user('sponsor_id')) {
                Message::send(null, $body2, [user('sponsor_id')]);
            }

            if ($sent) {
                Session::set('info', 'Thanks you for your interest.'
                    ." We've request and are now processing it.");
            } else {
                Session::set('warning', "We've encountered some problems."
                    .' Please try again in a few minutes. Thank you.');
            }
        }
        sendTo('back');
    }

    public static function getRequests()
    {
        Auth::check();
        $requests = Request::where('user_id', user('id'))
            ->orderBy('id', 'desc')
            ->get();
        include linkPage('me/requests');
    }

    public static function getSettings()
    {
        Auth::check();

        include linkPage('me/settings');
    }

    public static function getReservations()
    {
        Auth::check();

        include linkPage('me/reservations');
    }

    public static function getTeetimes()
    {
        Auth::check();

        include linkPage('me/teetimes');
    }

    public static function getCancel()
    {
        Auth::check();

        include linkPage('me/cancel');
    }

    public static function postCancel()
    {
        Auth::check();

        if (Input::get('password') != user('password')) {
            Session::set('danger', 'Invalid password');
            sendTo('back');
        }

        $requests = Request::where('request', 'membership_cancel')
            ->where('status', 'pending')
            ->get();
        if (count($requests)) {
            Session::set('warning', 'You currently have a pending similar'
                .' request. Please wait for the request to be acknowledged'
                .' before proceeding with this new request.');
            sendTo('back');
        }

        if (Input::get('password') == user('password')) {
            $request_id = Request::make(user('id'), 'membership_cancel', 'officer');

            $body = 'We have received your request to cancel you membership'
            .' please be patient while we review your request.'
            .' In the meantime we advice you to consider approaching a staff member'
            .' to explain the details of you membership status.'
            ." You may track the officer's approval at ".linkTo("track/{$request_id}");
            $sent = Mail::send('Request To Cancel Membership', [user('email')], $body);

            $body = 'You have requested to cancel your membership';
            Message::send(null, $body, [user('id')]);

            $body = user('first_name').' '.user('last_name').' has requested to cancel his/her membership';
            Message::sendToTypes(null, $body, ['officer', 'accounting']);

            if ($sent) {
                Session::set('info', 'Request to cancel membership sent');
            } else {
                Session::set('warning', "We've encountered some problems."
                    .' Please try again in a few minutes. Thank you.');
            }
        }
        sendTo('back');
    }

    public static function getPassword()
    {
        Auth::check();

        include linkPage('me/password');
    }

    public static function postPassword()
    {
        Auth::check();

        if (Input::get('password') != user('password')) {
            Session::set('danger', 'Invalid password');
            sendTo('back');
        }

        if (Input::get('new_password') != Input::get('confirm_password')) {
            Session::set('warning', "The new passwords you've entered does not match");
            sendTo('back');
        }

        $user = User::find(user('id'));
        $user->password = Input::get('new_password');
        $user->save();

        $body = 'Your password has been updated.'
        ." If you didn't change your password or have not requested it,"
        .' please contact us at '.Config::get('email', 'address').'';
        $sent = Mail::send('Password Updated', [user('email')], $body);

        $body = 'You updated your password';
        Message::send(null, $body, [user('id')]);

        if ($sent) {
            Session::set('info', 'Password updated.');
        } else {
            Session::set('warning', "We've encountered some problems."
                .' Please try again in a few minutes. Thank you.');
        }
        sendTo('back');
    }
}
