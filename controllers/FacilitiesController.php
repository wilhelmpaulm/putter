<?php

class FacilitiesController
{
    public static function index()
    {
        Auth::check();
        $facilities = Facility::orderBy('id')->orderBy('type')->get();

        //Auth::check(['accounting', 'staff', 'officer']);
        // $facilities = Facility::orderBy('id')->orderBy('type')->whereIn('type', ['facility', 'course'])->get();

        include linkPage('facilities/list');
    }

    public static function create()
    {
        Auth::check();

        $types = ['course', 'facility'];
        include linkPage('facilities/add');
    }

    public static function store()
    {
        Auth::check();
        $input = Input::get();
        $facility = new Facility();

        if ($input['image']['name']) {
            $uploadedImage = File::upload('image');
            $facility->image = $uploadedImage['name'];
        }

        $facility->name = $input['name'];
        $facility->label = $input['label'];
        $facility->details = $input['details'];
        $facility->latitude = $input['latitude'];
        $facility->longitude = $input['longitude'];
        $facility->price = $input['price'];
        $facility->type = $input['type'];
        $facility->active = $input['active'] ?? 0;
        $facility->save();

        $body = user('first_name').' has added a new'
        ." facility: {$input['name']}"
        .' (#'.padId($facility->id).').';
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'Facility item was added to the system');
        sendTo('back');
    }

    public static function delete(int $id)
    {
        Auth::check();
        $item = Facility::find($id);
        $item->delete();

        $body = user('first_name').' has deleted'
        ." discount: {$item->name}"
        .' (#'.padId($item->id).').';
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'The facility was deleted from the system');
        sendTo('facilities');
    }

    public static function show(int $id)
    {
        Auth::check();
        $facility = Facility::find($id);

        if (!$facility) {
            return IndexController::getError(404);
        }

        $types = ['course', 'facility'];
        include linkPage('facilities/edit');
    }

    public static function update(int $id)
    {
        Auth::check();
        $input = Input::get();
        $facility = Facility::find($id);

        if ($input['image']['name'] && $input['image'] != $facility->image) {
            $uploadedImage = File::upload('image');
            $facility->image = $uploadedImage['name'];
        }

        $facility->name = $input['name'];
        $facility->label = $input['label'];
        $facility->details = $input['details'];
        $facility->latitude = $input['latitude'];
        $facility->longitude = $input['longitude'];
        $facility->price = $input['price'];
        $facility->type = $input['type'];
        $facility->active = $input['active'] ?? 0;
        $facility->save();

        $body = user('first_name').' has updated an'
        ." facility: {$input['name']}"
        .' (#'.padId($facility->id).').';
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'Facility item was updated');
        sendTo('back');
    }

    public static function getCourses()
    {
        $items = Facility::orderBy('id')->where('type', 'course')->get();
        include linkPage('facilities/courses');
    }

    public static function getVenues()
    {
        $items = Facility::orderBy('id')->where('type', 'facility')->get();

        include linkPage('facilities/venues');
    }
}
