<?php

class EventsController implements ControllerInterface
{
    public static function index()
    {
        return false;
    }

    public static function teetimes()
    {
        $courses = Facility::where('type', 'course')
            ->where('active', 1)
            ->get();

        if (!Input::get('facility_id') || !Input::get('start_date')) {
            sendTo('events/teetimes?facility_id='.$courses[0]->id.'&start_date='.date('Y-m-d', strtotime('now')));
        }

        $teeTimes = Reservation::getTeeTimes();
        $items = Reservation::whereIn('type', ['teetime', 'tournament'])
            ->where('facility_id', Input::get('facility_id'))
            ->whereDate('start_date', '=', date(Input::get('start_date')))
            ->where('status', 'approved')
            ->orderBy('start_date')
            ->get();

        foreach ($teeTimes as $key => $value) {
            $teeTimes[$key] = (object) [
                'value' => $value,
                'reservation' => null,
            ];
        }

        foreach ($items as $i) {
            $start = substr($i->start_date, 11);
            $end = substr($i->end_date, 11);
            foreach ($teeTimes as $key => $val) {
                if (!$end && $key == $start) {
                    $teeTimes[$key]->reservation = $i;
                }
                if ($end && $start <= $key && $key <= $end) {
                    $teeTimes[$key]->reservation = $i;
                }
            }
        }

        include linkPage('events/teetimes');
    }

    public static function reservations()
    {
        if (Input::get('type') && Input::get('start_date') && Input::get('end_date')) {
            if (Input::get('type') == 'all') {
                $items = Reservation::orderBy('id', 'desc')
                    ->where('created_at', '>=', Input::get('start_date'))
                    ->where('created_at', '<=', Input::get('end_date'))
                    ->whereIn('type', ['reservation', 'banquet', 'tournament'])
                    ->where('status', 'approved')
                    ->where('public', 1)
                    ->get();
            } else {
                $items = Reservation::orderBy('id', 'desc')
                    ->where('created_at', '>=', Input::get('start_date'))
                    ->where('created_at', '<=', Input::get('end_date'))
                    ->where('type', Input::get('type'))
                    ->where('status', 'approved')
                    ->whereIn('type', ['reservation', 'banquet', 'tournament'])
                    ->where('public', 1)
                    ->get();
            }
        } else {
            $items = Reservation::orderBy('id', 'desc')
                ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-7 day')))
                ->where('created_at', '<=', date('Y-m-d H:i:s', strtotime('+7 day')))
                ->where('status', 'approved')
                ->whereIn('type', ['reservation', 'banquet', 'tournament'])
                ->where('public', 1)
                ->get();
        }

        include linkPage('events/list');
    }

    public static function reservation(int $id)
    {
        $item = Reservation::find($id);

        if (!$item || (in_array($item->status, ['pending', 'rejected']) && $item->user_id != user('id'))) {
            return IndexController::getError(404);
        } elseif (in_array(user('type'), ['accounting', 'officer', 'staff'])) {
            include linkPage('events/view');
        } elseif (in_array($item->status, ['pending', 'rejected']) && $item->user_id == user('id')) {
            include linkPage('events/view');
        } elseif (in_array($item->status, ['approved']) && $item->public) {
            include linkPage('events/view');
        } else {
            return IndexController::getError(404);
        }
    }

    public static function show(int $id)
    {
        return false;
    }

    public static function create()
    {
        return false;
    }

    public static function store()
    {
        return false;
    }

    public static function edit(int $id)
    {
        return false;
    }

    public static function update(int $id)
    {
        return false;
    }

    public static function delete(int $id)
    {
        return false;
    }
}
