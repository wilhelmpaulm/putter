<?php

class AccountingController
{
    public static function getPos()
    {
        Auth::check();
        $items = Inventory::where('type', 'item')
            ->where('active', 1)
            ->where('available', 1)
            ->where('amount', '>', 0)
            ->orderBy('id', 'desc')->get();
        $members = User::whereIn('type', getClientTypes())->get();
        if (!Input::get('user_id')) {
            sendTo('accounting/pos?user_id='.$members[0]->id);
        }
        if (Input::get('user_id')) {
            $member = User::find((int) Input::get('user_id'));
            $item = Invoice::preview((int) user('id'), (int) Input::get('user_id'), []);
        }

        $amount = 0;
        if (Input::get('inventory_id')) {
            $inventories = [];
            foreach (Input::get('inventory_id') as $key => $val) {
                if ($val > 0) {
                    $inventory = Inventory::find((int) $key);
                    $inventory->amount = (int) $val;
                    $inventory->price = (int) $inventory->current_price;
                    $inventories[] = $inventory;
                }
            }
            $item = Invoice::preview((int) user('id'), (int) Input::get('user_id'), $inventories);
        }

        include linkPage('accounting/pos');
    }

    public static function postPos()
    {
        Auth::check();
        $inventories = [];
        $member = User::find(Input::get('user_id'));
        $amount = 0;
        foreach (Input::get('inventory_id') as $key => $val) {
            if ($val > 0) {
                $amount += $val;
                $inventory = Inventory::find((int) $key);
                $inventory->amount = (int) $val;
                $inventory->price = (int) $inventory->current_price;
                $inventories[] = $inventory;
            }
        }
        if ($amount <= 0) {
            Session::set('danger', 'No items were added');
            sendTo('accounting/pos');
        }

        $invoice = Invoice::make((int) user('id'), (int) Input::get('user_id'), $inventories, 20);

        foreach (Input::get('inventory_id') as $key => $val) {
            if ($val > 0) {
                $inventory = Inventory::find((int) $key);
                $inventory->amount -= (int) $val;
                $inventory->save();
            }
        }

        $body = ' A new invoice is available for you'
            ." #{$invoice->id}";
        Message::send(null, $body, [(int) Input::get('user_id')]);

        Session::set('success', 'Invoice created');
        sendTo('accounting/pos');
    }

    public static function getBillings()
    {
        Auth::check();
        if (in_array(user('type'), ['member', 'dependent', 'guest'])) {
            $items = Invoice::where('type', 'billing')
                ->where(function ($query) {
                    $query->where('user_id', user('id'))
                        ->orWhere('paid_by', user('id'));
                })
                ->orderBy('status', 'desc')
                ->orderBy('id', 'desc')->get();
        } else {
            $items = Invoice::where('type', 'billing')
                ->orderBy('status', 'desc')
                ->orderBy('id', 'desc')
                ->get();
        }

        include linkPage('billings/list');
    }

    public static function getBillingsGenerate()
    {
        Auth::check(['accounting', 'officer', 'staff']);

        include linkPage('billings/generate');
    }

    public static function postBillingsGenerate()
    {
        Auth::check(['accounting', 'officer', 'staff']);
        if (Input::get('password') != user('password')) {
            Session::set('warning', 'Incorrect password.');
            sendTo('back');
        }

        $invoice = Invoice::generate(user('id'), Input::get('start_date'), Input::get('end_date'));

        Session::set('success', 'Billing Statements were generated');
        sendTo('back');
    }

    public static function getBilling(int $id)
    {
        Auth::check();

        $item = Invoice::find($id);
        if (!$item) {
            return IndexController::getError(404);
        }

        include linkPage('billings/view');
    }

    public static function getAccountingMembers()
    {
        Auth::check(['accounting', 'officer']);
        $members = User::where('type', 'member')->get();

        include linkPage('accounting/members');
    }

    public static function getAccountingAdjust()
    {
        Auth::check(['accounting', 'officer']);
        $members = User::where('type', 'member')->get();
        if (Input::get('user_id')) {
            $member = User::find((int) Input::get('user_id'));
        } else {
            sendTo('accounting/adjust?user_id='.$members[0]->id);
        }

        include linkPage('accounting/adjust');
    }

    public static function postAdjustBalance()
    {
        Auth::check(['accounting', 'officer', 'staff']);
        if (Input::get('password') != user('password')) {
            Session::set('warning', 'Incorrect password.');
            sendTo('back');
        }

        $input = Input::get();
        $member = User::find((int) $input['user_id']);
        $member->balance += $input['amount'];
        $member->save();

        $inventory = new Inventory();
        $inventory->name = 'accounting transaction';
        $inventory->label = $input['amount'] > 0 ? 'amount credited' : 'amount debited';
        $inventory->price = $input['amount'];
        $inventory->amount = 1;
        $inventory->type = 'service';
        $inventory->details = $input['reason'];

        $invoice = Invoice::make(user('id'), $input['user_id'], [$inventory], 0);
        $invoice->status = 'paid';
        $invoice->save();

        $body = 'An amount of:'
        ." {$input['amount']} has been"
        .' '.$input['amount'] > 0 ? 'credited' : 'debited'
            ." from your account due to {$input['reason']}."
            .' If you have any questions, please feel free to request'
            .' assistance from any of our staff.';
        Message::send(null, $body, [$input['user_id']]);

        $body = 'You have '
        .' '.$input['amount'] > 0 ? 'credited' : 'debited'
            ." an amount of {$input['amount']}."
            ." from the account of {$member->full_name}"
            ." with the reason {$input['reason']}.";
        Message::send(null, $body, [user('id')]);

        Session::set('success', "{$member->first_name}'s balance was adjusted");
        sendTo('back');
    }
}
