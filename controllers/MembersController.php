<?php

class MembersController
{
    public static function index(string $type)
    {
        $members = [];
        $staff_types = getUserTypes();
        $access_types = Access::pluck('type');
        $title = '';
        if ($type == 'review') {
            $title = 'Members Currently for Review';
            $requests = Request::where('request', 'independent_request')
                ->where('status', 'pending')
                ->pluck('user_id');
            $members = User::where('type', 'review')
                ->orWhereIn('id', $requests)
                ->get();
        } elseif ($type == 'current') {
            $title = 'Current Members';
            $members = User::whereIn('type', ['member'])
                ->where('active', '1')
                ->get();
            if (Auth::isLoggedIn() && user('type') != 'dependent') {
                $members = User::whereIn('type', ['member', 'dependent'])
                    ->where('active', '1')
                    ->get();
            }
        } elseif ($type == 'officers') {
            $title = 'Club Officers';
            $members = User::whereIn('type', ['officer', 'accounting'])
                ->where('active', '1')
                ->get();
        } elseif ($type == 'staff') {
            $title = 'Club Staff';
            $members = User::whereIn('type', ['staff', 'maintenance'])
                ->where('active', '1')
                ->get();
        }

        include linkPage('members/list');
    }

    public static function updateStatus(int $id, string $status)
    {
        $member = User::find($id);
        $member->active = $status == 'activate' ? 1 : 0;
        $success = $member->save();

        $body = "Your account was {$status}d at ".date('Y-m-d H:i:s').'.'
        .' Please contact '.Config::get('email', 'address').' for further details.'
            .' Thank you.';
        Message::send(null, $body, [$id]);

        $body = "You {$status}d the access of {$member->full_name} at ".date('Y-m-d H:i:s').'.';
        Message::send(null, $body, [user('id')]);

        if ($success) {
            Session::set('info', 'Member status updated');
        } else {
            Session::set('danger', 'Failed to update member status');
        }

        echo $success ? $member : false;
    }

    public static function updateType(int $id, string $type)
    {
        $member = User::find($id);
        $type = urldecode($type);
        if (!$member) {
            Session::set('danger', 'Member does not exist');
            echo false;
        }

        if (!in_array($member->type, ['accounting', 'staff'])) {
            Session::set('danger', 'Action not allowed');
            echo false;
        }

        $member->type = $type;
        if (Access::where('type', $type)->first()) {
            $member->access = $type;
        }
        $success = $member->save();

        $body = "Your user type was updated to {$type} at ".date('Y-m-d H:i:s').'.'
        .' Please contact '.Config::get('email', 'address').' for further details.'
            .' Thank you.';
        Message::send(null, $body, [$id]);

        $body = "You updated user type of {$member->full_name} to {$type} at"
        .' '.date('Y-m-d H:i:s').'.';
        Message::send(null, $body, [user('id')]);

        if ($success) {
            Session::set('info', 'User type updated');
        } else {
            Session::set('danger', 'Failed to update user type');
        }

        echo $success ? $member : false;
    }

    public static function updateRole(int $id, string $role)
    {
        $member = User::find($id);
        $role = urldecode($role);
        if (!$member) {
            Session::set('danger', 'Member does not exist');
            echo false;
        }

        $member->access = $role;
        $success = $member->save();

        $body = "Your user access was updated to {$role} at ".date('Y-m-d H:i:s').'.'
        .' Please contact '.Config::get('email', 'address').' for further details.'
            .' Thank you.';
        Message::send(null, $body, [$id]);

        $body = "You updated user access of {$member->full_name} to {$role} at"
        .' '.date('Y-m-d H:i:s').'.';
        Message::send(null, $body, [user('id')]);

        if ($success) {
            Session::set('info', 'User access updated');
        } else {
            Session::set('danger', 'Failed to update user access');
        }

        echo $success ? $member : false;
    }

    public static function updateNotifications(int $id, string $status)
    {
        $member = User::find($id);
        $member->notifications = $status == 'activate' ? 1 : 0;
        $success = $member->save();
        if ($success) {
            Session::set('info', 'Notification preference updated');
        } else {
            Session::set('danger', 'Failed to update dependent status');
        }

        echo $success ? $member : false;
    }

    public static function updateMessages(int $id, string $status)
    {
        $member = User::find($id);
        $member->messages = $status == 'activate' ? 1 : 0;
        $success = $member->save();
        if ($success) {
            Session::set('info', 'Message preference updated');
        } else {
            Session::set('danger', 'Failed to update dependent status');
        }

        echo $success ? $member : false;
    }

    public static function updateEmails(int $id, string $status)
    {
        $member = User::find($id);
        $member->emails = $status == 'activate' ? 1 : 0;
        $success = $member->save();
        if ($success) {
            Session::set('info', 'Email preference updated');
        } else {
            Session::set('danger', 'Failed to update dependent status');
        }

        echo $success ? $member : false;
    }

    public static function show(int $id)
    {
        sendTo("members/$id/profile");
    }

    public static function getProfile(int $id)
    {
        Auth::notMe($id);

        $member = User::find($id);
        if (!$member) {
            Session::set('warning', 'User with that id does not exist');

            return IndexController::getError(404);
        }

        include linkPage('members/view');
    }

    public static function editProfile(int $id)
    {
        $member = User::find($id);
        if (!$member) {
            Session::set('warning', 'User with that id does not exist');

            return IndexController::getError(404);
        }

        include linkPage('members/edit');
    }

    public static function updateProfile(int $id)
    {
        $member = User::find($id);
        if (!$member) {
            Session::set('warning', 'User with that id does not exist');

            return IndexController::getError(404);
        }

        try {
            $input = Input::get();
            if ($input['image']['name']) {
                $uploadedImage = File::upload('image');
                $member->image = $uploadedImage['name'];
            }

            $member->gender = $input['gender'] ?: $member->gender;
            $member->first_name = $input['first_name'] ?: $member->first_name;
            $member->middle_name = $input['middle_name'] ?: $member->middle_name;
            $member->last_name = $input['last_name'] ?: $member->last_name;
            $member->mobile = $input['mobile'] ?: $member->mobile;
            $member->company = $input['company'] ?: $member->company;
            $member->occupation = $input['occupation'] ?: $member->occupation;
            $member->address = $input['address'] ?: $member->address;
            $member->city = $input['city'] ?: $member->city;
            $member->zip_code = $input['zip_code'] ?: $member->zip_code;
            $member->country = $input['country'] ?: $member->country;
            $member->details = $input['details'] ?: $member->details;
            $member->intent = $input['intent'] ?: $member->intent;
            $member->color = getRandomColor();
            $member->short = strtoupper($member->first_name[0].$member->last_name[0]);
            $member->save();
        } catch (Exception $e) {
            error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');
            Session::set('danger', 'Failed to update member details.');
        }

        sendTo("members/$id/profile");
    }

    public static function getComments(int $id)
    {
        Auth::notMe($id);

        $member = User::find($id);
        if (!$member) {
            Session::set('warning', 'User with that id does not exist');

            return IndexController::getError(404);
        }

        include linkPage('members/comments');
    }

    public static function storeComment(int $id)
    {
        Auth::notMe($id);

        $member = User::find($id);
        if (!$member) {
            Session::set('warning', 'User with that id does not exist');

            return IndexController::getError(404);
        }

        $comment = new Comment();
        $comment->type = 'member';
        $comment->topic_id = $id;
        $comment->user_id = user('id');
        $comment->message = Input::get('message');
        $comment->save();

        Session::set('success', 'Comment was saved');
        sendTo('back');
    }

    public static function getEndorsement()
    {
        Auth::check();

        include linkPage('members/endorse');
    }

    public static function storeEndorsement()
    {
        Auth::check();
        $body = ucfirst(user('first_name').' '.user('last_name')).' would'
        .' like to endorse you to join the club as a member.'
        .' Please bear in mind that this is still subject'
        .' to a club officer review. You may access the membership'
        .' registration form at'
        .' '.linkTo('register', [
            'endorser_id' => user('id'),
            'email' => Input::get('email'),
        ]);
        $sent = Mail::send('Endorsed Member Registration Link', [Input::get('email')], $body);
        if ($sent) {
            Session::set('info', 'Endorsed member registration link sent.');
        } else {
            Session::set('warning', "We've encountered some problems."
                .' Please try again in a few minutes. Thank you.');
        }
        sendTo('back');
    }
}
