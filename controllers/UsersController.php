<?php

class UsersController
{
    public static function getLogin()
    {
        include linkPage('login');
    }

    public static function getLogout()
    {
        User::logout();
        Session::set('info', "You've been logged out");
        sendTo('login');
    }

    public static function postLogin()
    {
        $isLogged = User::login(Input::get('email'), Input::get('password'));
        if ($isLogged) {
            Session::set('success', 'Welcome back');
            sendTo('');
        } else {
            Session::set('danger', 'Email or password is incorrect');
            sendTo('back');
        }
    }

    public static function getForgot()
    {
        include linkPage('forgot');
    }

    public static function postForgot()
    {
        $email = Input::get('email');
        $user = User::where('email', $email)->first();
        if ($user) {
            $body = "Your password is: $user->password. Login at ".linkTo('login');
            $sent = Mail::send('Password Recovery', [$user->email], $body);
            if ($sent) {
                Session::set('info', 'Your password has been sent to your email');
            } else {
                Session::set('warning', "We've encountered some problems. Please try again in a few minutes. Thank you.");
            }
            sendTo('login');
        }

        Session::set('warning', 'Email address submitted is'
            .' not associated with an account');
        sendTo('back');
    }

    public static function getTrack(int $id)
    {
        $request = Request::find($id);
        if (!$request) {
            Session::set('warning', 'Request with the specified id was not found');
            sendTo('login');
        }
        $request->request = str_replace('_', ' ', $request->request);

        include linkPage('track');
    }

    public static function getRequest()
    {
        include linkPage('request');
    }

    public static function getRegister()
    {
        include linkPage('register');
    }

    public static function postRequest()
    {
        if (Input::get('email')) {
            $body = 'To access the registration form, please proceed'
            .' to '.linkTo('register', [
                'email' => Input::get('email'),
            ]);
            $sent = Mail::send('Registration Link', [Input::get('email')], $body);
            if ($sent) {
                Session::set('info', 'Thanks you for your interest.'
                    ." We've sent the registration link to your email.");
            } else {
                Session::set('warning', "We've encountered some problems."
                    .' Please try again in a few minutes. Thank you.');
            }
        }
        sendTo('back');
    }

    public static function postRegister()
    {
        $complete = Input::validate([
            'email',
            'password',
            'first_name',
            'last_name',
            'gender',
            'mobile',
            'address',
            'city',
            'zip_code',
            'country',
            'details',
            'intent',
        ]);

        $input = Input::get();
        try {
            $oldUser = User::where('email', $input['email'])->first();
            if ($oldUser) {
                Session::set('danger', 'This email account is either used by a current member, waiting for a membership request, or is blocked.');
                sendTo('back');
            }

            $uploadedImage = File::upload('image');

            $user = new User();
            $user->type = 'request';
            $user->access = 'dependent';
            $user->email = $input['email'];
            $user->password = $input['password'];
            $user->gender = $input['gender'];
            $user->first_name = $input['first_name'];
            $user->middle_name = $input['middle_name'];
            $user->last_name = $input['last_name'];
            $user->mobile = $input['mobile'];
            $user->company = $input['company'];
            $user->occupation = $input['occupation'];
            $user->address = $input['address'];
            $user->city = $input['city'];
            $user->zip_code = $input['zip_code'];
            $user->country = $input['country'];
            $user->details = $input['details'];
            $user->intent = $input['intent'];
            $user->image = $uploadedImage['name'];
            $user->color = getRandomColor();
            $user->short = strtoupper($user->first_name[0].$user->last_name[0]);

            if (Input::get('sponsor_id')) {
                $user->sponsor_id = $input['sponsor_id'];
            }

            if (Input::get('endorser_id')) {
                $user->endorser_id = $input['endorser_id'];
            }

            $user->save();

            if ($user->sponsor_id) {
                $request_id = Request::make($user->id, 'dependent_request', 'officer');
                $body = 'Thank you for expressing your interest to join our club.'
                .' Please keep an eye on your email for updates.'
                .' You may track your request at '.linkTo("track/$request_id");
                Mail::send('Dependent Request Acknowledgement', [$user->email], $body);

                $sponsor = User::find($user->sponsor_id);
                $body = 'Dear '.$sponsor->first_name.', this is to notify you that'
                .$user->first_name.' which you sponsored has accomplished the'
                .' registration process. Please feel free to assist him/her while'
                .' we review his/her request. You may also track his/her request'
                .' at '.linkTo("track/$request_id");
                Mail::send('Dependent Request Acknowledgement', [$sponsor->email], $body);
                Message::send(null, $body, [$sponsor->id]);

                Session::set('success', 'Membership request form was submitted successfully.');
                sendTo('track/'.$request_id);
            }

            if ($user->endorser_id) {
                $endorser = User::find($user->endorser_id);
                $body = 'Dear '.$endorser->first_name.', this is to notify you that'
                .$user->first_name.' which you endorsed has accomplished the'
                .' registration process. Please feel free to assist him while'
                .' we review his request. You may also track his/her request'
                .' at '.linkTo("track/$request_id");
                Mail::send('Endorsed Registration Acknowledgement', [$endorser->email], $body);
                Message::send(null, $body, [$endorser->id]);
            }

            $request_id = Request::make($user->id, 'membership_request', 'officer');
            $body = 'Thank you for expressing your interest to join our club. Please keep an eye on your email for updates. You may track your request at '.linkTo("track/$request_id");
            Mail::send('Membership Request Acknowledgement', [$user->email], $body);
            Session::set('success', 'Membership request form was submitted successfully.');
            sendTo('track/'.$request_id);
        } catch (Exception $e) {
            Session::set('danger', 'Failed to submit membership request form. '.$e->getMessage());
            error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');
            sendTo('back');
        }

        Session::set('danger', 'Something went wrong while processing your request. Please try again later.');
        sendTo('back');
    }
}
