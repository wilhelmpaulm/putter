<?php

class BlogsController implements ControllerInterface
{
    public static function index()
    {
        Auth::check();
        $items = Blog::orderBy('id', 'desc')->where('user_id', user('id'))->get();

        include linkPage('blogs/list');
    }

    public static function show(int $id)
    {
        return false;
    }

    public static function create()
    {
        Auth::check();
        $types = ['item', 'service', 'rental'];

        include linkPage('blogs/add');
    }

    public static function store()
    {
        Auth::check();
        $input = Input::get();
        $blog = new Blog();

        if ($input['image']['name']) {
            $uploadedImage = File::upload('image');
            $blog->image = $uploadedImage['name'];
        }

        $blog->user_id = user('id');
        $blog->name = $input['name'];
        $blog->label = $input['label'];
        $blog->body = $input['body'];
        $blog->tags = json_encode(explode(',', $input['tags']) ?? []);
        $blog->type = $input['news'] ? 'news' : 'blog';
        $blog->public = $input['public'] ?? 0;
        $blog->save();

        $body = "You've created a new blog post (".padId($blog->id).')';
        Message::send(null, $body, [user('id')]);
        Session::set('success', 'Blog post created');
        sendTo('back');
    }

    public static function edit(int $id)
    {
        Auth::check();
        $item = Blog::find($id);

        if (!$item) {
            return IndexController::getError(404);
        }

        include linkPage('blogs/edit');
    }

    public static function update(int $id)
    {
        Auth::check();
        $input = Input::get();
        $blog = Blog::find($id);

        if ($input['image']['name']) {
            $uploadedImage = File::upload('image');
            $blog->image = $uploadedImage['name'];
        }

        $blog->user_id = user('id');
        $blog->name = $input['name'];
        $blog->label = $input['label'];
        $blog->body = $input['body'];
        $blog->tags = json_encode(explode(',', $input['tags']) ?? []);
        $blog->type = $input['news'] ? 'news' : 'blog';
        $blog->public = $input['public'] ?? 0;
        $blog->save();

        $body = "You've updated the blog post (".padId($blog->id).')';
        Message::send(null, $body, [user('id')]);

        Session::set('success', 'The blog post was updated');
        sendTo('back');
    }

    public static function delete(int $id)
    {
        Auth::check();
        $item = Blog::find($id);
        $item->delete();

        $body = "You've deleted a blog post (".padId($item->id).')';
        Message::send(null, $body, [user('id')]);

        Session::set('success', 'The blog post was deleted');
        sendTo('blogs');
    }
}
