<?php

class ReportsController
{
    public static function getGolfRounds()
    {
        Auth::check(['officer', 'accounting', 'staff']);
        $chartTypes = ['bar', 'line'];
        $input = Input::get();
        $chart = null;

        if ($input && Input::get('date') && Input::get(['date', 'start']) && Input::get(['date', 'end'])) {
            $startDate = $input['date']['start'].'-00-00 00:00:00';
            $endDate = ($input['date']['end'] + 1).'-00-00 00:00:00';
            $teetimes = Reservation::where('type', 'teetime')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->orderBy('created_at')
                ->get();

            $chart = ChartData::build();
            $year = $input['date']['start'];
            while ($year <= $input['date']['end']) {
                $chart->addLabel($year);
                ++$year;
            }
            foreach (getUserTypes() as $access) {
                $chart->addDatasetLabel($access);
            }

            foreach ($teetimes as $key => $value) {
                $chart->addLabel($value->year);
                $chart->addDatasetLabel($value->user->type);
                $chart->sumDatasetData($value->user->type, $value->year, $value->facility->price);
            }
        }
        include linkPage('reports/rounds');
    }

    public static function getReservations()
    {
        Auth::check(['officer', 'accounting', 'staff']);
        $chartTypes = ['bar', 'line'];
        $input = Input::get();
        $chart = null;

        if ($input && Input::get('date') && Input::get(['date', 'start']) && Input::get(['date', 'end'])) {
            $startDate = $input['date']['start'].'-00-00 00:00:00';
            $endDate = ($input['date']['end'] + 1).'-00-00 00:00:00';
            $teetimes = Reservation::where('type', 'reservation')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->orderBy('created_at')
                ->get();

            $chart = ChartData::build();
            $year = $input['date']['start'];
            while ($year <= $input['date']['end']) {
                $chart->addLabel($year);
                ++$year;
            }
            foreach (getUserTypes() as $access) {
                $chart->addDatasetLabel($access);
            }

            foreach ($teetimes as $key => $value) {
                $chart->addLabel($value->year);
                $chart->addDatasetLabel($value->user->type);
                $chart->sumDatasetData($value->user->type, $value->year, $value->facility->price);
            }
        }
        include linkPage('reports/reservations');
    }

    public static function getReservationsRevenue()
    {
        Auth::check(['officer', 'accounting', 'staff']);
        $chartTypes = ['bar', 'line'];
        $input = Input::get();
        $chart = null;

        if ($input && Input::get('date') && Input::get(['date', 'start']) && Input::get(['date', 'end'])) {
            $startDate = $input['date']['start'].'-00-00 00:00:00';
            $endDate = ($input['date']['end'] + 1).'-00-00 00:00:00';
            $teetimes = Reservation::where('type', 'reservation')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->orderBy('created_at')
                ->get();

            $chart = ChartData::build();
            foreach (getReservationTypes() as $type) {
                $chart->addLabel($type);
            }
            $year = $input['date']['start'];
            while ($year <= $input['date']['end']) {
                $chart->addDatasetLabel($year);
                ++$year;
            }

            foreach ($teetimes as $key => $value) {
                $chart->addLabel($value->type);
                $chart->addDatasetLabel($value->year);
                $chart->sumDatasetData($value->year, $value->type, $value->facility->price);
            }
        }
        include linkPage('reports/reservationsRevenue');
    }

    public static function getBookingUsage()
    {
        Auth::check(['officer', 'accounting', 'staff']);
        $reservationTypes = getReservationTypes();
        $chartTypes = ['bar', 'line'];
        $input = Input::get();
        $chart = null;

        if ($input && Input::get('date') && Input::get(['date', 'start']) && Input::get(['date', 'end'])) {
            $startDate = Input::get(['date', 'start']);
            $endDate = Input::get(['date', 'end']);
            $teetimes = Reservation::where('type', Input::get('reservationType'))
                ->where('status', 'approved')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->orderBy('created_at')
                ->get();

            $counts = [];
            foreach (getUserTypes() as $type) {
                $counts[$type] = 0;
            }

            foreach ($teetimes as $reservation) {
                ++$counts[$reservation->user->type];
            }

            $chart = PieChartData::build();
            $chart->setLabels(getUserTypes());
            $chart->setData(array_values($counts));
        }
        include linkPage('reports/booking');
    }

    public static function getBookingAppearance()
    {
        Auth::check(['officer', 'accounting', 'staff']);
        $reservationTypes = ['teetime', 'reservation'];
        $chartTypes = ['bar', 'line'];
        $input = Input::get();
        $chart = null;

        if ($input && Input::get('date') && Input::get(['date', 'start']) && Input::get(['date', 'end'])) {
            $startDate = Input::get(['date', 'start']);
            $endDate = Input::get(['date', 'end']);
            $teetimes = Reservation::where('type', Input::get('reservationType'))
                ->where('status', 'approved')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->orderBy('created_at')
                ->get();

            $counts = [];
            foreach ( ['pending', 'showed', 'missed'] as $type) {
                $counts[$type] = 0;
            }

            foreach ($teetimes as $reservation) {
                ++$counts[$reservation->true_appearance];
            }

            $chart = PieChartData::build();
            $chart->setLabels(['pending', 'showed', 'missed']);
            $chart->setData(array_values($counts));
        }
        include linkPage('reports/appearance');
    }

    public static function getProfit()
    {
        Auth::check(['officer', 'accounting', 'staff']);
        $chartTypes = ['bar', 'line'];
        $years = getYearList();
        $input = Input::get();
        $chart = null;

        if ($input && $input['year']) {
            $startDate = $input['year'].'-00-00 00:00:00';
            $endDate = ($input['year'] + 1).'-00-00 00:00:00';
            $invoices = Invoice::where('type', 'invoice')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->where('status', 'paid')
                ->orderBy('created_at')
                ->get();

            $chart = ChartData::build();
            $chart->setLabels(getMonthsShort());

            foreach (['revenue with vat', 'revenue', 'base price', 'profit'] as $item) {
                $chart->addDatasetLabel($item);
            }

            foreach ($invoices as $key => $value) {
                $chart->addLabel($value->created_at->format('M'));
                $chart->sumDatasetData('base price', $value->created_at->format('M'), round($value->base_price, 2));
                $chart->sumDatasetData('profit', $value->created_at->format('M'), round($value->profit, 2));
                $chart->sumDatasetData('revenue', $value->created_at->format('M'), round($value->subtotal, 2));
                $chart->sumDatasetData('revenue with vat', $value->created_at->format('M'), round($value->total, 2));
            }
        }
        include linkPage('reports/profit');
    }

    public static function getFacilities()
    {
        Auth::check(['officer', 'accounting', 'staff']);
        $chartTypes = ['bar', 'line'];
        $years = getYearList();
        $input = Input::get();
        $chart = null;

        if ($input && $input['year']) {
            $startDate = $input['year'].'-00-00 0 0 :00:00';
            $endDate = ($input['year'] + 1).'-00-00 0 0 :00:00';
            $reservations = Reservation::whereBetween('created_at', [$startDate, $endDate])
                ->whereIn('type', ['reservation', 'banquet'])
                ->orderBy('created_at')
                ->get();

            $facilityNames = Facility::where('type', 'facility')->get()->pluck('name');

            $chart = ChartData::build();
            $chart->setLabels(getMonthsShort());

            foreach ($facilityNames as $name) {
                $chart->addDatasetLabel($name);
            }

            foreach ($reservations as $key => $value) {
                $invoice = $value->invoice;

                if (!$invoice) {
                    continue;
                }
                $chart->sumDatasetData($value->facility->name, $value->created_at->format('M'), round($invoice->total, 2));
            }
        }
        include linkPage('reports/facilities');
    }

    public static function getCourses()
    {
        Auth::check(['officer', 'accounting', 'staff']);
        $chartTypes = ['bar', 'line'];
        $years = getYearList();
        $input = Input::get();
        $chart = null;

        if ($input && $input['year']) {
            $startDate = $input['year'].'-00-00 00:00:00';
            $endDate = ($input['year'] + 1).'-00-00 00:00:00';
            $reservations = Reservation::whereBetween('created_at', [$startDate, $endDate])
                ->whereIn('type', ['teetime', 'tournament'])
                ->orderBy('created_at')
                ->get();

            $facilityNames = Facility::where('type', 'course')->get()->pluck('name');

            $chart = ChartData::build();
            $chart->setLabels(getMonthsShort());

            foreach ($facilityNames as $name) {
                $chart->addDatasetLabel($name);
            }

            foreach ($reservations as $key => $value) {
                $invoice = $value->invoice;

                if (!$invoice) {
                    continue;
                }
                $chart->sumDatasetData($value->facility->name, $value->created_at->format('M'), round($invoice->total, 2));
            }
        }
        include linkPage('reports/courses');
    }

    public static function getDelinquency()
    {
        $members = User::where('type', 'member')->where('balance', '<', 0)->get();
        include linkPage('reports/delinquency');
    }

    public static function showDelinquency(int $id)
    {
        Auth::check();
        $user = User::findOrFail($id);
        $inventory = new Inventory();
        $inventory->id = 0;
        $inventory->name = 'Negative Balance';
        $inventory->label = $user->negativeSince ? 'Unsettled balance since '.$user->negativeSince : 'Unsettled balance';
        $inventory->details = 'Please make sure to settle your balance with our accounting.';
        $inventory->amount = 1;
        $inventory->price = $user->balance ?? 0.0;
        $inventories[] = $inventory;

        $item = Invoice::preview((int) user('id'), (int) $id, $inventories, 1.0);
        $item->current_balance = $user->balance ?? 0.0;
        $item->total = $user->balance ?? 0.0;
        if (!$item) {
            return IndexController::getError(404);
        }

        include linkPage('reports/delinquencyView');
    }
}
