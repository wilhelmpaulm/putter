<?php

class DiscountsController
{
    public static function index()
    {
        Auth::check();
        $items = Discount::orderBy('id', 'desc')->get();

        include linkPage('discounts/list');
    }

    public static function create()
    {
        Auth::check();

        $items = Inventory::where('active', 1)->get();
        if (Input::get('inventory_id')) {
            $item = Inventory::find((int) Input::get('inventory_id'));
        } else {
            sendTo('discounts/add?inventory_id='.$items[0]->id);
        }
        include linkPage('discounts/add');
    }

    public static function store()
    {
        Auth::check();
        $input = Input::get();
        $discount = new Discount();

        if ($input['image']['name']) {
            $uploadedImage = File::upload('image');
            $discount->image = $uploadedImage['name'];
        }

        $discount->inventory_id = $input['inventory_id'];
        $discount->name = $input['name'];
        $discount->label = $input['label'];
        $discount->details = $input['details'];
        $discount->image = $uploadedImage['name'];
        $discount->multiplier = $input['multiplier'];
        $discount->start_date = $input['start_date'];
        $discount->end_date = $input['end_date'];
        if ($input['reservation'] ?? false) {
            $discount->discount_start_date = $input['discount_start_date'];
            $discount->discount_end_date = $input['discount_end_date'];
        }
        $discount->active = $input['active'] ?? 0;
        $discount->public = $input['public'] ?? 0;
        $discount->save();

        $body = user('first_name').' has created a new'
        ." discount: {$input['name']}"
        .' (#'.padId($discount->id).').';
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'The discount was added into the system');
        sendTo('back');
    }

    public static function show(int $id)
    {
        Auth::check();
        $items = Inventory::where('active', 1)->get();
        $item = Discount::find($id);

        if (!$item) {
            return IndexController::getError(404);
        }

        include linkPage('discounts/edit');
    }

    public static function delete(int $id)
    {
        Auth::check();
        $item = Discount::find($id);
        $item->delete();

        $body = user('first_name').' has deleted'
        ." discount: {$item->name}"
        .' (#'.padId($item->id).').';
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'The discount was deleted from the system');
        sendTo('discounts');
    }

    public static function update(int $id)
    {
        Auth::check();
        $input = Input::get();
        $discount = Discount::find($id);

        if ($input['image']['name']) {
            $uploadedImage = File::upload('image');
            $discount->image = $uploadedImage['name'];
        }

        $discount->name = $input['name'];
        $discount->label = $input['label'];
        $discount->details = $input['details'];
        $discount->multiplier = $input['multiplier'];
        $discount->start_date = $input['start_date'];
        $discount->end_date = $input['end_date'];
        if ($input['reservation'] ?? false) {
            $discount->discount_start_date = $input['discount_start_date'];
            $discount->discount_end_date = $input['discount_end_date'];
        }
        $discount->active = $input['active'] ?? 0;
        $discount->public = $input['public'] ?? 0;
        $discount->save();

        $body = user('first_name').' has updated a'
        ." discount: {$input['name']}"
        .' (#'.padId($discount->id).').';
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'The discount was updated');
        sendTo('back');
    }
}
