<?php

class ReservationsController implements ControllerInterface
{
    public static function index()
    {
        Auth::check();
        $items = Reservation::where('type', 'reservation')
            ->orderBy('status')
            ->orderBy('id', 'desc')
            ->get();

        include linkPage('reservations/list');
    }

    public static function show(int $id)
    {
        return false;
    }

    public static function create()
    {
        if (!user('type')) {
            Session::set('info', 'Reservations are only'
                .' available to club members');
            sendTo('back');
        }

        Auth::check();
        $input = Input::get();
        $courses = Facility::where('type', 'facility')
            ->where('active', 1)
            ->get();

        if (!(bool) $input['facility_id'] || !(bool) $input['start_date']) {
            sendTo('reservations/add?'
                .'facility_id='.$courses[0]->id.'&'
                .'start_date='.date('Y-m-d', strtotime('now')));
        }

        $start_times = Reservation::getAvailableReservationTimes($input['facility_id'], $input['start_date']);

        if (!(bool) $input['start_time']) {
            sendTo('reservations/add?'
                .'facility_id='.$input['facility_id'].'&'
                .'start_date='.$input['start_date'].'&'
                .'start_time='.array_keys($start_times)[0]);
        }

        $facility = Inventory::find($input['facility_id']);

        if ($input['start_time']) {
            $end_times = $start_times;
            foreach ($end_times as $key => $val) {
                if ($key <= $input['start_time']) {
                    unset($end_times[$key]);
                }
            }
            $end_times = array_slice($end_times, 0, Config::get('limits', 'reservations_slot_limit') ?? null);
        }

        if (!(bool) $input['end_time']) {
            sendTo('reservations/add?'
                .'facility_id='.$courses[0]->id.'&'
                .'start_date='.date('Y-m-d', strtotime('now')).'&'
                .'start_time='.array_keys($start_times)[0].'&'
                .'end_time='.array_keys($end_times)[0]);
        }

        if ($input['start_time'] >= $input['end_time']) {
            $input['end_time'] = array_keys($end_times)[0];
        }

        if ($input['end_time']) {
            $start_date = new DateTime($input['start_date'].' '.$input['start_time']);
            $end_date = new DateTime($input['start_date'].' '.$input['end_time']);
            $since_start = $start_date->diff($end_date);
            $slots = $since_start->h + 1;
        }

        $members = User::whereIn('type', ['member', 'guest', 'dependent'])
            ->where('id', '!=', user('id'))
            ->where('active', 1)
            ->get();

        include linkPage('reservations/add');
    }

    public static function store()
    {
        Auth::check();

        $input = Input::get();
        $reservation = new Reservation();

        if ($input['image']['name']) {
            $uploadedImage = File::upload('image');
            $reservation->image = $uploadedImage['name'];
        }

        $reservation->user_id = user('id');
        $reservation->facility_id = $input['facility_id'];
        $reservation->start_date = $input['start_date'].' '.$input['start_time'];
        $reservation->end_date = $input['start_date'].' '.$input['end_time'];
        $reservation->guests = json_encode($input['guests']);
        $reservation->details = $input['details'];
        $reservation->status = 'pending';
        $reservation->name = $input['name'];
        $reservation->label = $input['label'];
        $reservation->tags = json_encode(explode(',', $input['tags']) ?? []);
        $reservation->capacity = $input['capacity'];
        $reservation->public = $input['public'] ?? 0;
        $reservation->type = 'reservation';
        $reservation->save();

        Request::make(user('id'), 'request_reservation_officer', 'officer', $reservation->id);
        Session::set('success', 'Request for reservation was created');
        sendTo('reservations');
    }

    public static function edit(int $id)
    {
        return false;
    }

    public static function update(int $id)
    {
        return false;
    }

    public static function delete(int $id)
    {
        return false;
    }

    public static function cancel(int $id)
    {
        Auth::check();
        $requests = Request::where('user_id', user('id'))
            ->where('target_id', $id)
            ->whereIn('request', ['request_reservation_officer', 'request_reservation_accounting'])
            ->get();

        foreach ($requests as $r) {
            $r->status = 'cancelled';
            $r->save();
        }

        $item = Reservation::find($id);
        $item->public = 0;
        $item->status = 'cancelled';

        $body = user('first_name').' has cancelled the reservation'
        ." on {$item->start_date} at"
        .' '.$item->facility->name.'.';
        Message::send(null, $body, json_decode($item->guests));
        $item->save();
        Session::set('success', 'Reservation was cancelled');
        sendTo('back');
    }

    public static function patchAppearance(int $id, string $appearance = 'pending')
    {
        Auth::check();

        $item = Reservation::find($id);
        $item->appearance = $appearance;
        $item->public = 0;
        $item->save();

        Session::set('success', "Reservation was marked as {$appearance}");
        sendTo('back');
    }
}
