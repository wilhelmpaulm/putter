<?php

class IndexController
{
    public static function getHome()
    {
        if (!Auth::isLoggedIn()) {
            sendTo('guest');
        }

        include linkPage('index');
    }

    public static function getContact()
    {
        include linkPage('contact');
    }

    public static function getTerms()
    {
        include linkPage('terms');
    }

    public static function getGuest()
    {
        if (Auth::isLoggedIn()) {
            sendTo('');
        }
        $members = User::where('status', 'review')->get();

        include linkPage('listing');
    }

    public static function getError(int $code = 404, $header = null, $body = null)
    {
        require 'views/errors/404.php';
    }

    public static function getTest()
    {
        echo '<h3>#Input variables</h3>';
        echo '<pre>';
        var_dump(Input::get());
        echo '</pre>';
        if ($_SESSION) {
            echo '<h3>#Sessions variables</h3>';
            echo '<pre>';
            var_dump($_SESSION);
            echo '</pre>';
        }
        echo '<h3>#Cookies variables</h3>';
        echo '<pre>';
        var_dump($_COOKIE);
        echo '</pre>';
        echo '<h3>#Server variables</h3>';
        echo '<pre>';
        var_dump($_SERVER);
        echo '</pre>';
    }
}
