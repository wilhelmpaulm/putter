<?php

interface ControllerInterface
{
    public static function index();

    public static function create();

    public static function store();

    public static function show(int $id);

    public static function update(int $id);

    public static function edit(int $id);

    public static function delete(int $id);
}
