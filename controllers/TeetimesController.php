<?php

class TeetimesController
{
    public static function index()
    {
        Auth::check();
        $items = Reservation::where('type', 'teetime')
            ->orderBy('status')
            ->orderBy('id', 'desc')
            ->get();

        include linkPage('teetimes/list');
    }

    public static function create()
    {
        if (!user('type')) {
            Session::set('info', 'Tee time reservation is only'
                .' available to club members');
            sendTo('back');
        }

        Auth::check();
        $courses = Facility::where('type', 'course')
            ->where('active', 1)
            ->get();

        if (!Input::get('facility_id') || !Input::get('start_date')) {
            sendTo('teetimes/add?facility_id='.$courses[0]->id.'&start_date='.date('Y-m-d', strtotime('now')));
        }

        $times = Reservation::getAvailableTeeTimes(Input::get('facility_id'), Input::get('start_date'));

        if (!$times) {
            sendTo('teetimes/add?facility_id='.$courses[0]->id.'&start_date='.date('Y-m-d', strtotime('+1 day')));
        }
        if (!Input::get('time')) {
            sendTo(substr($_SERVER['REQUEST_URI'].'&time='.array_keys($times)[0], 1));
        }

        $facility = Inventory::find(Input::get('facility_id'));
        $members = User::where('active', 1)->get();

        include linkPage('teetimes/add');
    }

    public static function store()
    {
        Auth::check();

        $input = Input::get();
        $reservation = new Reservation();
        $reservation->user_id = user('id');
        $reservation->facility_id = $input['facility_id'];
        $reservation->start_date = $input['start_date'].' '.$input['time'];
        $reservation->end_date = $reservation->start_date;
        $reservation->guests = json_encode($input['guests']);
        $reservation->details = $input['details'];
        $reservation->public = $input['public'] ?? 0;
        $reservation->status = 'approved';
        $reservation->type = 'teetime';
        $reservation->save();

        $body = user('first_name')
        .' created a tee time reservation'
        ." on {$reservation->start_date} at "
        .$reservation->facility->name
            .'. Please make sure to come at least 15 minutes before the tee time.'
            .' Additional Details: '.$input['details'];
        Message::send(null, $body, $input['guests']);
        Mail::send('Tee Time Reservation', User::whereIn('id', $input['guests'])->pluck('email'), $body);

        $details = [];
        $inventory = Inventory::find($reservation->facility_id);
        $inventory->details = $body;
        $inventory->price = $inventory->current_price($reservation->start_date, $reservation->start_date);
        $details[] = $inventory;

        $paid_by = $input['paid_by'] ? $input['paid_by'] : user('id');
        $invoice = Invoice::make(null, $paid_by, $details);

        $reservation->invoice_id = $invoice->id;
        $reservation->save();

        Session::set('success', 'Tee time was created');
        sendTo('teetimes');
    }

    public static function cancel(int $id)
    {
        Auth::check();
        $item = Reservation::find($id);
        $item->public = 0;
        $item->status = 'cancelled';

        $body = user('first_name').' has cancelled the tee time reservation'
        ." on {$item->start_date} at"
        .' '.$item->facility->name.'.';
        Message::send(null, $body, json_decode($item->guests));
        $item->save();
        Session::set('success', 'Tee time was cancelled');
        sendTo('back');
    }
}
