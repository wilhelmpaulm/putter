<?php

class RequestsController
{
    public static function index(string $request_status)
    {
        Auth::check(['member', 'dependent', 'officer', 'staff', 'accounting']);
        if (in_array(user('type'), ['member', 'dependent'])) {
            $requests = Request::where('user_id', user('id'))
                ->where('status', $request_status)
                ->get();
        } else {
            $requests = Request::getDepartmentRequests(user('type'), [$request_status]);
        }
        include linkPage('requests/index');
    }

    public static function show(int $id)
    {
        $request = Request::find($id);
        // dd($request);
        $request_status = $request->status;
        $request->user = User::find($request->user_id);

        if (!$request) {
            return IndexController::getError(404);
        }

        if (str_contains($request->request, ['membership', 'independent'])) {
            include linkPage('requests/previewMember');
        } elseif (str_contains($request->request, ['reservation', 'tournament', 'teetime', 'banquet'])) {
            $item = Reservation::find($request->target_id);
            include linkPage('requests/previewReservation');
        } else {
            Session::set('danger', 'Request preview not available');
            sendTo('requests');
        }
    }

    public static function updateStatus($id, $request_status)
    {
        if ($request_status == 'approve') {
            Request::approve($id);
        } else {
            Request::reject($id);
        }

        sendTo('requests/pending');
    }

    public static function getPending()
    {
        sendTo('requests/pending');
    }
}
