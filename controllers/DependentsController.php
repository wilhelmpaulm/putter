<?php

class DependentsController
{
    public static function index()
    {
        Auth::isLoggedIn();
        $staff_types = getUserTypes();
        $access_types = Access::pluck('type');
        $members = User::where('sponsor_id', user('id'))->get();

        include linkPage('dependents/list');
    }

    public static function create()
    {
        Auth::check();

        include linkPage('dependents/add');
    }

    public static function store()
    {
        Auth::check();
        $body = ucfirst(user('first_name').' '.user('last_name')).' would'
        .' like to invite you to join the club under a dependent'
        .' membership. Please bear in mind that this is still subject'
        .' to a club officer review. You may access the dependents'
        .' registration form at'
        .' '.linkTo('register', [
            'email' => Input::get('email'),
            'sponsor_id' => user('id'),
            'endorser_id' => user('id'),
        ]);
        $sent = Mail::send('Dependent Registration Link', [Input::get('email')], $body);
        if ($sent) {
            Session::set('info', 'Dependent registration link sent.');
        } else {
            Session::set('warning', "We've encountered some problems."
                .' Please try again in a few minutes. Thank you.');
        }
        sendTo('back');
    }
}
