<?php

class TournamentsController
{
    public static function index()
    {
        Auth::check();
        $items = Reservation::where('type', 'tournament')
            ->orderBy('status')
            ->orderBy('id', 'desc')
            ->get();

        include linkPage('tournaments/list');
    }

    public static function create()
    {
        if (!user('type')) {
            Session::set('info', 'Tee time reservation is only'
                .' available to club members');
            sendTo('back');
        }

        Auth::check();
        $courses = Facility::where('type', 'course')
            ->where('active', 1)
            ->get();

        if (!Input::get('facility_id') || !Input::get('start_date')) {
            sendTo('tournaments/add?'
                .'facility_id='.$courses[0]->id.'&'
                .'start_date='.date('Y-m-d', strtotime('now')));
        }

        $start_times = Reservation::getAvailableTeeTimes(Input::get('facility_id'), Input::get('start_date'));

        if (!Input::get('start_time')) {
            sendTo('tournaments/add?'
                .'facility_id='.Input::get('facility_id').'&'
                .'start_date='.Input::get('start_date').'&'
                .'start_time='.array_keys($start_times)[0]);
        }

        $facility = Inventory::find(Input::get('facility_id'));

        if (Input::get('start_time')) {
            $end_times = $start_times;
            foreach ($end_times as $key => $val) {
                if ($key <= Input::get('start_time')) {
                    unset($end_times[$key]);
                }
            }
            $end_times = array_slice($end_times, 0, Config::get('limits', 'tournaments_slot_limit') ?? null);
        }

        if (!Input::get('end_time')) {
            sendTo('tournaments/add?'
                .'facility_id='.$courses[0]->id.'&'
                .'start_date='.date('Y-m-d', strtotime('now')).'&'
                .'start_time='.array_keys($start_times)[0].'&'
                .'end_time='.array_keys($end_times)[0]);
        }

        if (Input::get('end_time')) {
            $slots = 0;
            foreach ($start_times as $key => $val) {
                if ($key <= Input::get('end_time')) {
                    ++$slots;
                } else {
                    break;
                }
            }
        }

        $members = User::whereIn('type', ['member', 'guest', 'dependent'])
            ->where('id', '!=', user('id'))
            ->where('active', 1)
            ->get();

        include linkPage('tournaments/add');
    }

    public static function store()
    {
        Auth::check();

        $input = Input::get();
        $reservation = new Reservation();

        if ($input['image']['name']) {
            $uploadedImage = File::upload('image');
            $reservation->image = $uploadedImage['name'];
        }

        $reservation->user_id = user('id');
        $reservation->facility_id = $input['facility_id'];
        $reservation->start_date = $input['start_date'].' '.$input['start_time'];
        $reservation->end_date = $input['start_date'].' '.$input['end_time'];
        $reservation->guests = json_encode($input['guests']);
        $reservation->details = $input['details'];
        $reservation->name = $input['name'];
        $reservation->label = $input['label'];
        $reservation->tags = json_encode(explode(',', $input['tags']) ?? []);
        $reservation->public = $input['public'] ?? 0;
        $reservation->status = 'pending';
        $reservation->type = 'tournament';
        $reservation->save();

        Request::make(user('id'), 'request_tournament_officer', 'officer', $reservation->id);
        Session::set('success', 'Request for tournament was created');
        sendTo('tournaments');
    }

    public static function cancel(int $id)
    {
        Auth::check();
        $item = Reservation::find($id);
        $item->public = 0;
        $item->status = 'cancelled';

        $requests = Request::where('user_id', user('id'))
            ->where('target_id', $id)
            ->whereIn('request', ['request_tournament_officer', 'request_tournament_accounting'])
            ->get();

        foreach ($requests as $r) {
            $r->status = 'cancelled';
            $r->save();
        }

        $item = Reservation::find($id);
        $item->public = 0;
        $item->status = 'cancelled';

        $body = user('first_name').' has cancelled the tournament reservation'
        ." on {$item->start_date} at"
        .' '.$item->facility->name.'.';
        Message::send(null, $body, json_decode($item->guests));
        $item->save();
        Session::set('success', 'Tournament was cancelled');
        sendTo('back');
    }
}
