<?php

class BanquetsController implements ControllerInterface
{
    public static function index()
    {
        Auth::check();
        $items = Reservation::where('type', 'banquet')
            ->orderBy('status')
            ->orderBy('id', 'desc')
            ->get();

        include linkPage('banquets/list');
    }

    public static function show(int $id)
    {
        return false;
    }

    public static function create()
    {
        if (!user('type')) {
            Session::set('info', 'Banquet reservations are only'
                .' available to club members');
            sendTo('back');
        }

        Auth::check();
        $input = Input::get();
        $courses = Facility::where('type', 'facility')
            ->where('active', 1)
            ->get();

        if (!(bool) Input::get('facility_id') || (bool) Input::get('start_time')) {
            sendTo('banquets/add?'
                .'facility_id='.$courses[0]->id.'&'
                .'start_date='.date('Y-m-d 00:00:00', strtotime('+7 day')).'&'
                .'end_date='.date('Y-m-d 23:59:59', strtotime('+8 day')));
        }

        $facility = Inventory::find(Input::get('facility_id'));
        $members = User::whereIn('type', ['member', 'guest', 'dependent'])
            ->where('id', '!=', user('id'))
            ->where('active', 1)
            ->get();

        $slots = 0;
        if ((bool) Input::get('end_date')) {
            $start_date = new DateTime(Input::get('start_date'));
            $end_date = new DateTime(Input::get('end_date'));
            $since_start = $start_date->diff($end_date);
            $slots = ($since_start->d + 1) * 24;
        }

        include linkPage('banquets/add');
    }

    public static function store()
    {
        Auth::check();

        $input = Input::get();
        $banquet = new Reservation();

        if ($input['image']['name']) {
            $uploadedImage = File::upload('image');
            $banquet->image = $uploadedImage['name'];
        }

        $banquet->user_id = user('id');
        $banquet->facility_id = $input['facility_id'];
        $banquet->start_date = $input['start_date'];
        $banquet->end_date = $input['end_date'];
        $banquet->capacity = $input['capacity'];
        $banquet->guests = json_encode($input['guests']);
        $banquet->details = $input['details'];
        $banquet->name = $input['name'];
        $banquet->label = $input['label'];
        $banquet->tags = json_encode(explode(',', $input['tags']) ?? []);
        $banquet->public = $input['public'] ?? 0;
        $banquet->status = 'pending';
        $banquet->type = 'banquet';
        $banquet->save();

        Request::make(user('id'), 'request_banquet_officer', 'officer', $banquet->id);
        Session::set('success', 'Request for banquet was created');
        sendTo('banquets');
    }

    public static function edit(int $id)
    {
        return false;
    }

    public static function update(int $id)
    {
        return false;
    }

    public static function delete(int $id)
    {
        return false;
    }

    public static function cancel(int $id)
    {
        Auth::check();
        $requests = Request::where('user_id', user('id'))
            ->where('target_id', $id)
            ->whereIn('request', ['request_banquet_officer', 'request_banquet_accounting'])
            ->get();

        foreach ($requests as $r) {
            $r->status = 'cancelled';
            $r->save();
        }

        $item = Reservation::find($id);
        $item->public = 0;
        $item->status = 'cancelled';

        $body = user('first_name')
        .' has cancelled the banquet'
        ." on {$item->start_date} at"
        .' '.$item->facility->name.'.';
        Message::send(null, $body, json_decode($item->guests));
        $item->save();
        Session::set('success', 'Banquet was cancelled');
        sendTo('back');
    }
}
