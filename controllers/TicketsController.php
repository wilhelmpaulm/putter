<?php

class TicketsController implements ControllerInterface
{
    public static function index()
    {
        Auth::check();
        if (in_array(user('type'), ['officer', 'admin'])) {
            $items = Ticket::orderBy('status', 'desc')
                ->orderBy('id', 'desc')->get();
        } else {
            $items = Ticket::where(function ($query) {
                $query->where('user_id', user('id'))
                    ->orWhere('staff_id', user('id'));
            })
                ->orderBy('status', 'desc')
                ->orderBy('id', 'desc')->get();
        }

        include linkPage('tickets/list');
    }

    public static function show(int $id)
    {
        Auth::check();
        $item = Ticket::find($id);
        include linkPage('tickets/view');
    }

    public static function create()
    {
        Auth::check();
        $reasons = [];
        $targets = [];
        $types = ['member', 'staff', 'facility', 'inventory'];
        if (Input::get('type')) {
            $reasons = Ticket::getReasons(Input::get('type'));
        }
        if (Input::get('type')) {
            switch (Input::get('type')) {
                case 'member':
                    $targets = User::whereIn('type', ['member', 'dependent'])
                        ->where('active', 1)
                        ->get();
                    break;
                case 'staff':
                    $targets = User::whereIn('type', ['staff', 'officer', 'accounting', 'maintenance'])
                        ->where('active', 1)
                        ->get();
                    break;
                case 'inventory':
                    $targets = Inventory::whereIn('type', ['item'])
                        ->where('active', 1)
                        ->get();
                    break;
                case 'facility':
                    $targets = Facility::whereIn('type', ['course', 'facility'])
                        ->where('active', 1)
                        ->get();
                    break;
                default:
                    break;
            }
            $reasons = Ticket::getReasons(Input::get('type'));
        }
        include linkPage('tickets/add');
    }

    public static function store()
    {
        Auth::check();

        if (Input::get('password') != user('password')) {
            Session::set('danger', 'Invalid password');
            sendTo('back');
        }

        $input = Input::get();
        $ticket = new Ticket();
        $ticket->user_id = user('id');
        $ticket->type = $input['type'];
        $ticket->target_id = $input['target_id'];
        $ticket->title = $input['title'];
        $ticket->body = $input['body'];
        $ticket->save();

        $body = user('first_name').' has created a new ticket'
        .' (#'.padId($ticket->id).').';
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', 'Ticket created');
        sendTo('tickets');
    }

    public static function edit(int $id)
    {
        return false;
    }

    public static function update(int $id)
    {
        return false;
    }

    public static function updateStatus(int $id, string $status)
    {
        Auth::check();
        $item = Ticket::find($id);
        $item->status = $status;
        $item->save();

        $body = user('first_name').' marked the ticket'
        .' (#'.padId($item->id).').'
            ." as {$status}.";
        Message::sendToTypes(null, $body, ['accounting', 'officer', 'staff']);

        Session::set('success', "Ticket marked as $status");
        sendTo('tickets');
    }

    public static function delete(int $id)
    {
        return false;
    }

    public static function comments(int $id)
    {
        Auth::notMe($id);

        $member = Ticket::find($id);
        if (!$member) {
            Session::set('warning', 'Ticket with that id does not exist');

            return IndexController::getError(404);
        }

        $comment = new Comment();
        $comment->type = 'ticket';
        $comment->topic_id = $id;
        $comment->user_id = user('id');
        $comment->message = Input::get('message');
        $comment->save();

        Session::set('success', 'Comment was saved');
        sendTo('back');
    }
}
