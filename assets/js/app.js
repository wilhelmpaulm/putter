$(document).ready(function() {
  if ($("#jodit").length) {
    let jodit = new Jodit("#jodit", {
      height: "100%",
      uploader: {
        insertImageAsBase64URI: true
      }
    });
  }

  $(".tag").each(function(index, element) {
    text = $(this)
      .text()
      .trim();
    if (text === "service") {
      $(this).addClass("tag-teal");
    } else if (text === "item") {
      $(this).addClass("tag-yellow");
    } else if (text === "rental") {
      $(this).addClass("tag-pink");
    }

    if (text === "blog") {
      $(this).addClass("tag-teal");
    } else if (text === "news") {
      $(this).addClass("tag-azure");
    } else if (text === "club") {
      $(this).addClass("tag-lime");
    }

    if (text === "facility") {
      $(this).addClass("tag-indigo");
    } else if (text === "course") {
      $(this).addClass("tag-lime");
    }

    if (["approved", "showed"].includes(text)) {
      $(this).addClass("tag-green");
    } else if (text === "reviewed") {
      $(this).addClass("tag-blue");
    } else if (["missed", "cancelled"].includes(text)) {
      $(this).addClass("tag-red");
    } else if (text === "pending") {
      $(this).addClass("tag-gray");
    }
  });

  $("#select-all").bind("click", function() {
    if (confirm("Are you sure you want to select/deselect all the items?")) {
      if ($(this).prop("checked")) {
        $(".select")
          .prop("checked", true)
          .change();
      } else {
        $(".select")
          .prop("checked", false)
          .removeAttr("checked")
          .change();
      }
      return true;
    }
    return false;
  });

  $(".reload").bind("change", function() {
    const url = $(this)
      .parents("form")
      .prop("action");
    const params = $(this)
      .parents("form")
      .serialize();
    window.location = url + "?" + params;
  });

  $("a").each(function() {
    if ($(this).attr("href") == window.location.href.split("?")[0]) {
      $(this).addClass("active");
    }
  });
  //    $('input[type="hidden"].range').bind('change load', function () {
  $(".range").bind("change keypress keyup mousedown mouseup", function() {
    //        const blob = window.URL.createObjectURL($(this).prop('files')[0]);
    $(this)
      .parent()
      .parent()
      .find(".range")
      .val($(this).val());
  });

  $('input[accept="image/*"]').bind("change load", function() {
    const blob = window.URL.createObjectURL($(this).prop("files")[0]);
    console.log(
      $(this)
        .parent()
        .siblings(".img-thumbnail")
        .attr("src", blob)
    );
  });

  $(".img-thumbnail").on("click", function() {
    const input = $(this)
      .parent()
      .find('input[accept="image/*"]');
    $(this)
      .parent()
      .find('input[accept="image/*"]')
      .click();
  });

  if ($("input[type='date']").length) {
    const format = $(this).data("format") || "M / DD / YYYY hh: mm A";
    const singleDatePicker = $(this).data("singleDatePicker") || true;
    const showDropDowns = $(this).data("showDropDowns") || true;
    const autoSelect = $(this).data("autoSelect") || true;
    const minYear = $(this).data("minYear") || 1901;
    const maxYear =
      $(this).data("maxYear") || parseInt(moment().format("YYYY"), 10);

    $('input[name="date"]').daterangepicker(
      {
        singleDatePicker,
        showDropDowns,
        autoSelect,
        minYear,
        maxYear,
        locale: { format }
      },
      function(start, end, label) {
        console.log(start, end, label);
      }
    );
  }

  if ($("input[type='daterange']").length) {
    $.each($("input[type='daterange']"), function(
      indexInArray,
      valueOfElement
    ) {
      const input = $(valueOfElement);
      const format = input.data("format") || "M / DD / YYYY hh: mm A";
      const showDropDowns = input.data("showDropDowns") || true;
      const autoSelect = input.data("autoSelect") || true;
      const minYear = input.data("minYear") || 1901;
      const maxYear =
        input.data("maxYear") || parseInt(moment().format("YYYY"), 10);
      const startDate = input.data("startdate")
        ? moment(input.data("startdate"), format)
        : moment().format(format);
      const endDate = input.data("enddate")
        ? moment(input.data("enddate"), format)
        : moment()
            .add("days", 1)
            .format(format);

      const ranges = input.data("ranges")
        ? {
            Today: [moment(), moment()],
            Yesterday: [
              moment().subtract(1, "days"),
              moment().subtract(1, "days")
            ],
            "Last 7 Days": [moment().subtract(6, "days"), moment()],
            "Last 30 Days": [moment().subtract(29, "days"), moment()],
            "This Month": [moment().startOf("month"), moment().endOf("month")],
            "Last Month": [
              moment()
                .subtract(1, "month")
                .startOf("month"),
              moment()
                .subtract(1, "month")
                .endOf("month")
            ],
            "Last Year": [moment().subtract(1, "years"), moment()],
            "Last 2 Years": [moment().subtract(2, "years"), moment()],
            "Last 3 Years": [moment().subtract(3, "years"), moment()]
          }
        : null;

      input.daterangepicker(
        {
          showDropDowns,
          autoSelect,
          minYear,
          maxYear,
          startDate,
          endDate,
          ranges,
          format,
          autoApply: true,
          locale: { format }
        },
        function(start, end, label) {
          const list = { start, end };
          for (const key in list) {
            if (
              !input
                .parents("form")
                .find(`input[name="${input.attr("name")}[${key}]"]`).length
            ) {
              $("<input>")
                .attr({
                  type: "hidden",
                  name: `${input.attr("name")}[${key}]`
                })
                .val(list[key].format(format))
                .appendTo(input.parents("form"));
            } else {
              input
                .parents("form")
                .find(`input[name="${input.attr("name")}[${key}]"]`)
                .val(list[key].format(format));
            }
          }
          console.log(start, end, label, input);
        }
      );
    });
  }

  if ($("#daterangepicker-plain").length) {
    $("#daterangepicker-plain").daterangepicker(
      {
        alwaysShowCalendars: true,
        startDate: moment().format("L"),
        endDate: moment()
          .add("days", 1)
          .format("L"),
        locale: { format: "M/DD/YYYY hh:mm A" }
      },
      function(start, end) {
        console.log(
          "New date range selected: " +
            start.format("YYYY-MM-DD HH:mm:ss") +
            " to " +
            end.format("YYYY-MM-DD HH:mm:ss")
        );
        $("#start_date").val(start.format("YYYY-MM-DD HH:mm:ss"));
        $("#end_date").val(end.format("YYYY-MM-DD HH:mm:ss"));
      }
    );

    if ($("#start_date").length && $("#end_date").length) {
      $("#daterangepicker-plain")
        .data("daterangepicker")
        .setStartDate(moment($("#start_date").val()));
      $("#daterangepicker-plain")
        .data("daterangepicker")
        .setEndDate(moment($("#end_date").val()));
    }
  }

  if ($("#daterangepicker-discount").length) {
    $("#daterangepicker-discount").daterangepicker(
      {
        alwaysShowCalendars: true,
        opens: $("#daterangepicker-discount").data("position")
          ? $("#daterangepicker-discount").data("position")
          : "right",
        startDate: moment().format("L"),
        endDate: moment()
          .add("days", 1)
          .format("L"),
        locale: { format: "M/DD/YYYY hh:mm A" }
      },
      function(start, end) {
        console.log(
          "New date range selected: " +
            start.format("YYYY-MM-DD HH:mm:ss") +
            " to " +
            end.format("YYYY-MM-DD HH:mm:ss")
        );
        $("#discount_start_date").val(start.format("YYYY-MM-DD HH:mm:ss"));
        $("#discount_end_date").val(end.format("YYYY-MM-DD HH:mm:ss"));
      }
    );

    if ($("#discount_start_date").length && $("#discount_end_date").length) {
      $("#daterangepicker-discount")
        .data("daterangepicker")
        .setStartDate(moment($("#discount_start_date").val()));
      $("#daterangepicker-discount")
        .data("daterangepicker")
        .setEndDate(moment($("#discount_end_date").val()));
    }
  }

  if ($("#daterangepicker").length) {
    $("#daterangepicker").daterangepicker(
      {
        alwaysShowCalendars: true,
        timePicker:
          $("#daterangepicker").data("time") === false
            ? false
            : $("#daterangepicker").data("min") === false
            ? false
            : true,
        timePickerIncrement: 15,
        opens: $("#daterangepicker").data("position")
          ? $("#daterangepicker").data("position")
          : "right",
        minDate: $("#daterangepicker").data("add-days")
          ? moment()
              .add("days", $("#daterangepicker").data("add-days"))
              .format("L")
          : $("#daterangepicker").data("min") === false
          ? null
          : moment().format("L"),
        startDate: $("#daterangepicker").data("add-days")
          ? moment()
              .add("days", $("#daterangepicker").data("add-days"))
              .format("L")
          : moment().format("L"),
        endDate: $("#daterangepicker").data("add-days")
          ? moment()
              .add("days", $("#daterangepicker").data("add-days"))
              .add("days", 1)
              .format("L")
          : moment()
              .add("days", 1)
              .format("L"),
        locale: { format: "M/DD/YYYY hh:mm A" }
      },
      function(start, end) {
        console.log(
          "New date range selected: " +
            start.format("YYYY-MM-DD HH:mm:ss") +
            " to " +
            end.format("YYYY-MM-DD HH:mm:ss")
        );
        $("#start_date").val(start.format("YYYY-MM-DD HH:mm:ss"));
        $("#end_date").val(end.format("YYYY-MM-DD HH:mm:ss"));

        if (
          $("#daterangepicker").data("min") == false ||
          $("#daterangepicker").data("reload") == true
        ) {
          const url = $("#start_date")
            .parents("form")
            .prop("action");
          const params = $("#start_date")
            .parents("form")
            .serialize();
          window.location = url + "?" + params;
        }
      }
    );

    if ($("#start_date").length && $("#end_date").length) {
      $("#daterangepicker")
        .data("daterangepicker")
        .setStartDate(moment($("#start_date").val()));
      $("#daterangepicker")
        .data("daterangepicker")
        .setEndDate(moment($("#end_date").val()));
    }
  }

  if ($("#date").length) {
    $("#date").daterangepicker(
      {
        autoApply: true,
        singleDatePicker: true,
        minDate: $("#date").data("min") === false ? null : moment().format("L"),
        minYear: moment().format("YYYY"),
        maxYear: parseInt(moment().format("YYYY"), 10)
      },
      function(start) {
        if ($("#date").data("reload") === false) {
          return true;
        }

        $("#start_date").val(start.format("YYYY-MM-DD"));
        console.log("New date selected: " + start.format("YYYY-MM-DD"));
        const url = $("#start_date")
          .parents("form")
          .prop("action");
        const params = $("#start_date")
          .parents("form")
          .serialize();
        window.location = url + "?" + params;
      }
    );
    if ($("#start_date").length && !$("#end_date").length) {
      $("#date")
        .data("daterangepicker")
        .setStartDate(moment($("#start_date").val()));
    }
  }

  $(".dataTable").DataTable({
    order: [],
    lengthMenu: [1000, 500, 100, 75, 50, 25, 10, 5],
    pageLength: 1000
  });

  $(".custom-file-input").on("change", function() {
    const fileName = $(this).val();
    $(this)
      .parent()
      .find("label")
      .text(fileName);
  });

  var timeagoInstance = timeago();
  var nodes = document.querySelectorAll(".timeago");
  // use render method to render nodes in real time
  timeagoInstance.render(nodes, "en");

  $(".agree").bind("click change", function() {
    console.log($(this));
    if ($(this).prop("checked")) {
      $(".needAgree").removeAttr("disabled");
    } else {
      $(".needAgree").attr("disabled", "");
    }
  });

  $(".dependents-toggle-status").click(function() {
    const user_id = $(this).data("user_id");
    const status = $(this).data("status");
    $.ajax({
      type: "GET",
      url: `/members/${user_id}/${status}`
    })
      .done(data => {
        window.location.reload();
      })
      .fail(data => {
        alert("action failed");
      });
  });

  $(".staff-toggle-type").change(function() {
    const user_id = $(this).data("user_id");
    const type = $(this)
      .find("option:selected")
      .val();
    $.ajax({
      type: "GET",
      url: `/members/${user_id}/type/${type}`
    })
      .done(data => {
        window.location.reload();
      })
      .fail(data => {
        alert("action failed");
      });
  });

  $(".staff-toggle-access").change(function() {
    const user_id = $(this).data("user_id");
    const type = $(this)
      .find("option:selected")
      .val();
    $.ajax({
      type: "GET",
      url: `/members/${user_id}/role/${type}`
    })
      .done(data => {
        window.location.reload();
      })
      .fail(data => {
        alert("action failed");
      });
  });

  $(".notifications-toggle-status").click(function() {
    const user_id = $(this).data("user_id");
    const status = $(this).data("status");
    $.ajax({
      type: "GET",
      url: `/members/${user_id}/notifications/${status}`
    })
      .done(data => {
        window.location.reload();
      })
      .fail(data => {
        alert("action failed");
      });
  });

  $(".messages-toggle-status").click(function() {
    const user_id = $(this).data("user_id");
    const status = $(this).data("status");
    $.ajax({
      type: "GET",
      url: `/members/${user_id}/messages/${status}`
    })
      .done(data => {
        window.location.reload();
      })
      .fail(data => {
        alert("fail fail fail");
      });
  });

  $(".emails-toggle-status").click(function() {
    const user_id = $(this).data("user_id");
    const status = $(this).data("status");
    $.ajax({
      type: "GET",
      url: `/members/${user_id}/emails/${status}`
    })
      .done(data => {
        window.location.reload();
      })
      .fail(data => {
        alert("fail fail fail");
      });
  });

  $(".update-balance").bind("keyup change", function() {
    const balance = $("#current_balance").val();
    $("#final_balance").val(
      (Number(balance) + Number($(this).val())).toFixed(2)
    );
  });

  $("#multiplier").bind("keyup change", function() {
    const price = $("#current_price").val();
    $("#final_price").val((Number(price) * Number($(this).val())).toFixed(2));
  });

  $("#final_price").bind("keyup change", function() {
    const price = $("#current_price").val();
    $("#multiplier").val(Number($(this).val() / Number(price)).toFixed(2));
  });

  if ($("#reservation_date").length) {
    if (!$("#for_reservation").is(":checked")) {
      $("#reservation_date").hide();
    }
    $("#for_reservation").bind("change", function() {
      $("#reservation_date").toggle("fast");
    });
  }

  require(["jquery", "selectize"], function($, selectize) {
    $(document).ready(function() {
      $("#input-tags").selectize({
        delimiter: ",",
        persist: false,
        create: function(input) {
          return {
            value: input,
            text: input
          };
        }
      });
      const select_reload = $("#select-reload").selectize({
        render: {
          option: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              '<span class="title">' +
              escape(data.text) +
              "</span>" +
              "</div>"
            );
          },
          item: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              escape(data.text) +
              "</div>"
            );
          }
        }
      });
      $("#select-target").selectize({
        render: {
          option: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              '<span class="title">' +
              escape(data.text) +
              "</span>" +
              "</div>"
            );
          },
          item: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              escape(data.text) +
              "</div>"
            );
          }
        }
      });
      const select_inventory = $("#select-inventory").selectize({
        render: {
          option: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              '<span class="title">' +
              escape(data.text) +
              "</span>" +
              "</div>"
            );
          },
          item: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              escape(data.text) +
              "</div>"
            );
          }
        }
      });
      const select_user_plain = $("#select-user-plain").selectize({
        render: {
          option: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              '<span class="title">' +
              escape(data.text) +
              "</span>" +
              "</div>"
            );
          },
          item: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              escape(data.text) +
              "</div>"
            );
          }
        }
      });
      const select_user = $("#select-user").selectize({
        render: {
          option: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              '<span class="title">' +
              escape(data.text) +
              "</span>" +
              "</div>"
            );
          },
          item: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              escape(data.text) +
              "</div>"
            );
          }
        }
      });
      const select_users = $("#select-users").selectize({
        maxItems: $("#select-users").data("max"),
        render: {
          option: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              '<span class="title">' +
              escape(data.text) +
              "</span>" +
              "</div>"
            );
          },
          item: function(data, escape) {
            return (
              "<div>" +
              '<span class="image avatar-' +
              data.color +
              '">' +
              (data.image
                ? '<img src="' + data.image + '" alt="">'
                : data.short) +
              "</span>" +
              escape(data.text) +
              "</div>"
            );
          }
        }
      });
      $("#select-countries").selectize({
        render: {
          option: function(data, escape) {
            return (
              "<div>" +
              '<span class="image"><img src="' +
              data.image +
              '" alt=""></span>' +
              '<span class="title">' +
              escape(data.text) +
              "</span>" +
              "</div>"
            );
          },
          item: function(data, escape) {
            return (
              "<div>" +
              '<span class="image"><img src="' +
              data.image +
              '" alt=""></span>' +
              escape(data.text) +
              "</div>"
            );
          }
        }
      });

      select_user.change(function() {
        if (!$(this).val()) {
          return false;
        }
        window.location = "/accounting/adjust?user_id=" + $(this).val();
      });

      select_inventory.change(function() {
        if (!$(this).val()) {
          return false;
        }
        window.location = "/discounts/add?inventory_id=" + $(this).val();
      });

      select_reload.change(function() {
        if(!$(this).val()) {
          return false;
        }
        const url = $(this)
          .parents("form")
          .prop("action");
        const params = $(this)
          .parents("form")
          .serialize();
        window.location = url + "?" + params;
        //                window.location = $(this).data('url') + $(this).val() + "&start_date=" + $('#start_date').val();
      });
    });
  });
});

function back() {
  if (history.back()) {
    window.location.href = "/";
  }
}
