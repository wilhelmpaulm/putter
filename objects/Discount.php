<?php

class Discount extends Illuminate\Database\Eloquent\Model
{
    public function getColorAttribute()
    {
        return 'gray';
    }

    public function getShortAttribute()
    {
        return strtoupper(substr($this->name, 0, 2));
    }

    public function getInventoryAttribute()
    {
        return Inventory::find((int) $this->inventory_id);
    }
}
