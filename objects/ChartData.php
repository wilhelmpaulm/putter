<?php

class ChartData extends Illuminate\Database\Eloquent\Collection
{
    public $labels = [];
    public $datasets = [];

    public static function build(array $labels = [], array $datasets = []): ChartData
    {
        $chartData = new static();
        $chartData->setLabels($labels);
        foreach ($datasets as $key => $value) {
            $chartData->addDataset($value['label'], $value['data']);
        }

        return $chartData;
    }

    public function addLabel(string $label): ChartData
    {
        if (!in_array($label, $this->labels)) {
            $this->labels[] = $label;
        }

        return $this;
    }

    public function setLabels(array $labels = []): ChartData
    {
        $this->labels = $labels;

        return $this;
    }

    public function addDatasetData(string $label = '', string $key = '', $data): ChartData
    {
        if (array_key_exists($label, $this->datasets)) {
            $this->datasets[$label][$key] = $data;
        }

        return $this;
    }

    public function sumDatasetData(string $label = '', string $key = '', $data): ChartData
    {
        if (array_key_exists($label, $this->datasets)) {
            if (!array_key_exists($key, $this->datasets[$label])) {
                $this->datasets[$label][$key] = 0;
            }
            $this->datasets[$label][$key] += $data;
        }

        return $this;
    }

    public function addDatasetLabel(string $label = ''): ChartData
    {
        if (!array_key_exists($label, $this->datasets)) {
            $this->datasets[$label] = [];
        }

        return $this;
    }

    public function addDataset(string $label = '', array $data = []): ChartData
    {
        $this->datasets[$label] = $data;

        return $this;
    }

    public function sortLabels(): ChartData
    {
        sort($this->labels);

        return $this;
    }

    public function json(): string
    {
        return json_encode($this->all());
    }

    public function all()
    {
        $datasets = [];
        foreach ($this->datasets as $key => $value) {
            $data = [];
            foreach ($this->labels as $label) {
                $data[] = $value[$label] ?? 0;
            }
            $datasets[] = (object) [
                'label' => $key,
                'fill' => false,
                'data' => $data,
            ];
        }

        return (object) [
            'labels' => $this->labels,
            'datasets' => $datasets,
        ];
    }
}
