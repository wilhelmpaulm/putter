<?php

class Access extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'access';

    public function updateList(array $list = null)
    {
        if ($list == null) {
            $list = [];
        }

        $this->list = json_encode($list);

        return $this->update();
    }

    public function getList(): array
    {
        return json_decode($this->list ?? []);
    }

    public function getRatioAttribute()
    {
        $max = count($this->getList());
        $val = count(getRoutes());

        return ($max / $val) * 100 ?? 0;
    }
}
