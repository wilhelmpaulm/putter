<?php

class Mail
{
    public static function view($page)
    {
        ob_start();

        include_once __DIR__.$page;

        $output = ob_get_contents();

        ob_get_clean();
        ob_end_clean();

        return $output;
    }

    public static function send($subject = 'Support Email', $to = [], $body = "you're important")
    {
        if (!Config::get('system', 'mail')) {
            return true;
        }

        try {
            // Create the Transport
            $transport = (new Swift_SmtpTransport('smtp.gmail.com', 587, 'tls'))
                ->setUsername(Config::get('email', 'address'))
                ->setPassword(Config::get('email', 'password'));

            // Create the Mailer using your created Transport
            $mailer = new Swift_Mailer($transport);

            // Create a message
            $subject = ucwords(Config::get('club')->name.' - '.$subject);
            $new_to = [];

            foreach ($to as $t) {
                $u = User::where('email', $t)->where('emails', 0)->first();
                if (!$u) {
                    $new_to[] = $t;
                }
            }

            if ($new_to) {
                $message = (new Swift_Message($subject))
                    ->setFrom([Config::get('email', 'address') => Config::get('club', 'name').' Support'])
                    ->setTo($new_to)
                    ->setBody($body);
                // Send the message
                return $mailer->send($message);
            }
        } catch (Exception $e) {
            error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');

            return false;
        }
    }
}
