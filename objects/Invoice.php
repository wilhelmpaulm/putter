<?php

class Invoice extends Illuminate\Database\Eloquent\Model
{
    protected $appends = ['base_price', 'profit'];

    public static function make($staff_id, $user_id, $details = [], $vat = null)
    {
        $vat = $vat ?? Config::get('limits', 'vat') ?? 20.0;
        $user = User::find($user_id);
        $subtotal = 0.0;

        foreach ($details as $d) {
            $subtotal += $d->price * $d->amount;
        }

        $invoice = new Invoice();
        $invoice->staff_id = $staff_id;
        $invoice->user_id = $user_id;
        $invoice->paid_by = $user->sponsor_id ? $user->sponsor_id : $user_id;
        $invoice->details = json_encode($details);
        $invoice->subtotal = (float) $subtotal;
        $invoice->current_balance = (float) $user->balance;
        $invoice->vat = (float) $vat ?? 0.0;
        $invoice->total = (float) $subtotal + ($vat ? ((float)$subtotal * ((float)$vat / 100)) : 0);
        $invoice->save();

        $body = 'You have a new invoice #'.padId($invoice->id);
        Message::send(null, $body, [$invoice->paid_by]);

        return $invoice;
    }

    public static function preview($staff_id, $user_id, $details = [], $vat = null)
    {
        $vat = $vat ?? Config::get('limits', 'vat') ?? 20.0;
        $user = User::find($user_id);
        $subtotal = 0.0;

        foreach ($details as $d) {
            $subtotal += $d->price * $d->amount;
        }

        $invoice = new Invoice();
        $invoice->staff_id = $staff_id;
        $invoice->user_id = $user_id;
        $invoice->paid_by = $user->sponsor_id ? $user->sponsor_id : $user_id;
        $invoice->details = json_encode($details);
        $invoice->subtotal = (float) $subtotal;
        $invoice->vat = (float) $vat ?? 0.0;
        $invoice->total = (float) $subtotal + ($vat ? ((float) $subtotal * ((float) $vat / 100) ) : 0);

        return $invoice;
    }

    public static function generate($staff_id, $start_date, $end_date)
    {
        $users = User::where('type', 'member')
            ->where('active', 1)->get();

        foreach ($users as $user) {
            $user_id = $user->id;
            $subtotal = 0.0;

            $invoices = Invoice::where('type', 'invoice')
                ->whereBetween('created_at', [$start_date, $end_date])
                ->whereIn('status', ['paid', 'pending'])
                ->where('paid_by', $user_id)
                ->get();

            $details = [];
            foreach ($invoices as $i) {
                $names = [];
                foreach ($i->items as $item) {
                    $names[] = "<p class='font-w600 mb-1'>".$item->name."</p> <div class='text-muted small'>-".$item->label."</div> <div class='text-muted small'>-".$item->details.'<div>';
                }
                $name = implode('<br>', $names);

                $inventory = new Inventory();
                $inventory->created_at = $i->created_at;
                $inventory->id = $i->id;
                $inventory->name = $name;
                $inventory->price = $i->total;
                $inventory->amount = $i->status == 'pending' ? 1 : 0;
                $inventory->status = $i->status;

                $details[] = $inventory;
            }

            foreach ($details as $d) {
                $subtotal += $d->price * $d->amount;
            }

            if ($user->balance < 0) {
                $subtotal += ($user->balance * -1);
            }

            $invoice = new Invoice();
            $invoice->staff_id = $staff_id;
            $invoice->user_id = $user_id;
            $invoice->current_balance = $user->balance;
            $invoice->paid_by = $user_id;
            $invoice->details = json_encode($details);
            $invoice->subtotal = $subtotal;
            $invoice->vat = 0.0;
            $invoice->total = $subtotal;
            $invoice->type = 'billing';
            $invoice->status = 'paid';
            $invoice->start_date = Input::get('start_date');
            $invoice->end_date = Input::get('end_date');
            $invoice->save();

            $body = 'You have a new billing statement #'.padId($invoice->id);
            Message::send(null, $body, [$invoice->paid_by]);
        }

        return $invoice;
    }

    public function getItemsAttribute()
    {
        return json_decode($this->details);
    }

    public function getUserAttribute()
    {
        return User::find($this->user_id);
    }

    public function getPayerAttribute()
    {
        return User::find($this->paid_by);
    }

    public function getStaffAttribute()
    {
        return User::find($this->staff_id);
    }

    public function getBasePriceAttribute()
    {
        $basePrice = 0;
        foreach ($this->items as $key => $value) {
            $basePrice += $value->base_price * $value->amount;
        }

        return $basePrice;
    }

    public function getProfitAttribute()
    {
        return $this->subtotal - $this->basePrice;
    }

    public static function getRevenueBefore()
    {
        $invoices = Invoice::where('status', 'paid')
            ->where('created_at', '<', date('Y-m-d 00:00:00', strtotime('now')))
            ->where('created_at', '>', date('Y-m-d 23:59:59', strtotime('-1 day')))
            ->where('type', 'invoice')
            ->pluck('total');
        $total = 0;
        foreach ($invoices as $i) {
            $total += (float) $i;
        }

        return self::getRevenueToday() - $total;
    }

    public static function getRevenueToday()
    {
        $invoices = Invoice::where('status', 'paid')
            ->where('created_at', '>', date('Y-m-d 00:00:00', strtotime('now')))
            ->where('type', 'invoice')
            ->pluck('total');
        $total = 0;
        foreach ($invoices as $i) {
            $total += (float) $i;
        }

        return $total;
    }

    public static function getExpectedRevenueBefore()
    {
        $invoices = Invoice::where('status', 'pending')
            ->where('created_at', '<', date('Y-m-d 00:00:00', strtotime('now')))
            ->where('created_at', '>', date('Y-m-d 23:59:59', strtotime('-1 day')))
            ->where('type', 'invoice')
            ->pluck('total');
        $total = 0;
        foreach ($invoices as $i) {
            $total += (float) $i;
        }

        return self::getRevenueToday() - $total;
    }

    public static function getExpectedRevenueToday()
    {
        $invoices = Invoice::where('status', 'pending')
            ->where('created_at', '>', date('Y-m-d 00:00:00', strtotime('now')))
            ->where('type', 'invoice')
            ->pluck('total');
        $total = 0;
        foreach ($invoices as $i) {
            $total += (float) $i;
        }

        return $total;
    }

    public static function getPendingBefore()
    {
        return self::getPending() - Invoice::where('status', 'pending')
            ->where('created_at', '<', date('Y-m-d 00:00:00', strtotime('now')))
            ->where('type', 'invoice')
            ->count();
    }

    public static function getPending()
    {
        return Invoice::where('status', 'pending')
            ->where('created_at', '>', date('Y-m-d 00:00:00', strtotime('now')))
            ->where('type', 'invoice')
            ->count();
    }
}
