<?php

class Comment extends Illuminate\Database\Eloquent\Model
{
    public function getUserAttribute()
    {
        return User::find((int) $this->user_id);
    }

    public static function getComments($topic_id, $type)
    {
        if (!$topic_id || !$type) {
            return false;
        }

        return self::where('type', $type)
            ->where('topic_id', $topic_id)
            ->orderBy('id', 'desc')
            ->get() ?? [];
    }

    public function getRepliesAttribute($topic_id)
    {
        return Comment::where('reply_to', (int) $this->id)
            ->orderBy('id', 'desc')
            ->get();
    }
}
