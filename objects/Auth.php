<?php

class Auth
{
    public static function check($types = [])
    {
        $user = self::user();
        if (!$user->active) {
            User::logout();
            Session::set('warning', "You're user is not active."
                .' Please contact '.Config::get('email', 'address').''
                .' for more information.');
            header('HTTP/1.1 401 Unauthorized');
            sendTo('login');

            return false;
        }

        if ($types == [] && $user) {
            return true;
        }

        if (is_array($types) && in_array($user->type, $types)) {
            return true;
        }

        User::logout();
        Session::set('warning', "You're not allowed to access that page.");
        header('HTTP/1.1 401 Unauthorized');
        sendTo('login');

        return false;
    }

    public static function user()
    {
        if (!Session::get('user')) {
            return null;
        }
        $id = Session::get('user')['id'];

        return User::find((int) $id);
    }

    public static function isLoggedIn()
    {
        return (bool) self::user();
    }

    public static function hasAccess($name)
    {
        return in_array($name, user('access_list'));
        // if () {
        //     Session::set('alert', "You don't have access to this page");
        //     sendTo('me');
        // }
    }

    public static function notMe($id)
    {
        if (user('id') == $id) {
            Session::set('alert', "You don't have access to this page");
            sendTo('me');
        }
    }
}
