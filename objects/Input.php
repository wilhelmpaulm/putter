<?php

class Input
{
    public static function get($key = null)
    {
        $var = array_merge($_REQUEST, $_FILES);
        if (!$key) {
            return $var;
        }
        if (is_array($key)) {
            $x = $var;
            foreach ($key as $value) {
                if (is_array($x) && key_exists($value, $x)) {
                    $x = $x[$value];
                } else {
                    return false;
                }
            }

            return $x;
        } elseif (key_exists($key, $var)) {
            return  $var[$key];
        }

        return false;
    }

    public static function validate($list = [])
    {
        $var = array_merge($_REQUEST, $_FILES);

        if ($list == []) {
            return true;
        }
        if (is_array($list)) {
            foreach ($list as $item) {
                if (!key_exists($item, $var)) {
                    Session::set('danger', 'Form submitted was missing some elements');
                    sendTo('back');

                    return false;
                }
            }
        }

        return true;
    }
}
