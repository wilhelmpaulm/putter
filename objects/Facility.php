<?php

class Facility extends Inventory
{
    protected $table = 'inventories';

    public function getColorAttribute()
    {
        return 'list';
    }

    public function getSettingsAttribute(){
        return json_decode($this->options);
    }

    public function setSetting(string $key, $value) {
        $settings = $this->settings;
        $settings->$key = $value;
        $this->options = json_encode($settings);
    }

    public function getReservationsAttribute()
    {
        return Reservation::where('facility_id', $this->id)->get();
    }

    public function getShortAttribute()
    {
        return strtoupper(substr($this->name, 0, 2));
    }
}
