<?php

class Ticket extends Illuminate\Database\Eloquent\Model
{
    public function getUserAttribute()
    {
        return User::find((int) $this->user_id);
    }

    public function getStaffAttribute()
    {
        return User::find((int) $this->staff_id);
    }

    public function getTargetAttribute()
    {
        if (in_array($this->type, ['member', 'staff'])) {
            return User::find((int) $this->target_id);
        }
        if ($this->type == 'facility') {
            return Facility::find((int) $this->target_id);
        }
        if ($this->type == 'inventory') {
            return Inventory::find((int) $this->target_id);
        }

        return null;
    }

    public function getCommentsAttribute()
    {
        return Comment::where('type', 'ticket')
            ->where('topic_id', (int) $this->id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public static function getMemberReasons()
    {
        return [
            'Request for changes in profile',
            'Invoice / Billing Statement',
            'Not issued / Balance has error',
            'impolite to members',
            'racist/ sexist behaviors towards other members',
            'stealing club property',
            'vandalism of club property',
            'indecency within club premises',
        ];
    }

    public static function getStaffReasons()
    {
        return [
            'impolite to members',
            'racist/ sexist behaviors towards other members',
            'stealing club property',
            'vandalism of club property',
            'indecency within club premises',
        ];
    }

    public static function getFacilityReasons()
    {
        return [
            'general maintenance',
            'broken fixtures',
            'foul odors',
            'insect/vermin infestation',
        ];
    }

    public static function getInventoryReasons()
    {
        return [
            'hazardous item',
            'low quality item',
            'price is too high',
            'remove from circulation',
        ];
    }

    public static function getReasons($type)
    {
        switch ($type) {
            case 'member':
                return self::getMemberReasons();
                break;
            case 'staff':
                return self::getStaffReasons();
                break;
            case 'facility':
                return self::getFacilityReasons();
                break;
            case 'inventory':
                return self::getInventoryReasons();
                break;
            default:
                return [];
                break;
        }
    }
}
