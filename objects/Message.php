<?php

class Message extends Illuminate\Database\Eloquent\Model
{
    public function getUserAttribute()
    {
        if (!$this->user_id) {
            return User::system();
        } else {
            return User::find((int) $this->user_id);
        }
    }

    public static function sendToTypes($id = null, $body = '', $types = [], $avoid = [])
    {
        if (!Config::get('system', 'message')) {
            return true;
        }

        if ($types == []) {
            return false;
        }
        try {
            foreach (User::whereIn('type', $types)
                ->whereNotIn('id', $avoid)
                ->get() as $user) {
                if ($id == null && !$user->notifications) {
                    continue;
                }
                $message = new Message();
                $message->user_id = $id;
                $message->message = $body;
                $message->sent_to = $user->id;
                $message->save();
            }
        } catch (Exception $e) {
            error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');
            Session::set('danger', 'A message failed to send');
            echo $e->getTraceAsString();
        }

        return true;
    }

    public static function send($id = null, $body = '', $ids = [], $avoid = [])
    {
        if (!Config::get('system', 'message')) {
            return true;
        }

        if ($ids == []) {
            return false;
        }
        try {
            $users = User::whereIn('id', $ids)
                ->whereNotIn('id', $avoid)
                ->get();
            foreach ($users as $user) {
                if ($id == null && !$user->notifications) {
                    continue;
                }
                $message = new Message();
                $message->user_id = $id;
                $message->message = $body;
                $message->sent_to = $user->id;
                $message->save();
            }
        } catch (Exception $e) {
            error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');
            Session::set('danger', 'A message failed to send');
            echo $e->getTraceAsString();
        }

        return true;
    }
}
