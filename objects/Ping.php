<?php

class Ping extends Illuminate\Database\Eloquent\Model
{
    public function getUserAttribute()
    {
        return User::find((int) $this->user_id);
    }

    public static function track()
    {
        $detect = new Mobile_Detect();
        $device = 'desktop';

        if ($detect->isTablet()) {
            $device = 'tablet';
        }
        if ($detect->isMobile()) {
            $device = 'mobile';
        }

        $ping = new Ping();

        $ping->user_id = user('id') ?? null;
        $ping->input = json_encode(Input::get()) ?? null;

        $ping->device = $device;
        $ping->uri = $_SERVER['REQUEST_URI'];
        $ping->method = $_SERVER['REQUEST_METHOD'];
        $ping->save();
    }
}
