<?php

class Inventory extends Illuminate\Database\Eloquent\Model
{
    public function getColorAttribute()
    {
        return 'gray';
    }

    public function getProfitAttribute()
    {
        return $this->price - $this->base_price;
    }

    public function getShortAttribute()
    {
        return strtoupper(substr($this->name, 0, 2));
    }

    public function getTicketsAttribute()
    {
        return Ticket::where('type', $this->type)
            ->where('target_id', $this->id)
            ->get();
    }

    public function getDiscountAttribute()
    {
        $current_price = $this->price;
        $discount = Discount::where('inventory_id', $this->id)
            ->where('active', 1)
            ->where('start_date', '<', date('Y-m-d H:i:s', strtotime('now')))
            ->where('end_date', '>', date('Y-m-d H:i:s', strtotime('now')))
            ->where('discount_start_date', null)
            ->where('discount_start_date', null)
            ->orderBy('id', 'desc')
            ->first();

        return $discount;
    }

    public function getCurrentPriceAttribute()
    {
        $current_price = $this->price;
        $discount = Discount::where('inventory_id', $this->id)
            ->where('active', 1)
            ->where('start_date', '<', date('Y-m-d H:i:s', strtotime('now')))
            ->where('end_date', '>', date('Y-m-d H:i:s', strtotime('now')))
            ->where('discount_start_date', null)
            ->where('discount_end_date', null)
            ->orderBy('id', 'desc')
            ->first();

        return $discount && $discount->multiplier ? $current_price * $discount->multiplier : $current_price;
    }

    public function current_price($start_date, $end_date)
    {
        $current_price = $this->price;
        $discount = Discount::where('inventory_id', $this->id)
            ->where('active', 1)
            ->where('start_date', '<', date('Y-m-d H:i:s', strtotime('now')))
            ->where('end_date', '>', date('Y-m-d H:i:s', strtotime('now')))
            ->where('discount_start_date', '<', $start_date)
            ->where('discount_end_date', '>', $end_date)
            ->orderBy('id', 'desc')
            ->first();

        if (!$discount) {
            return $this->current_price;
        }

        return $discount && $discount->multiplier ? $current_price * $discount->multiplier : $current_price;
    }

    public function discount($start_date, $end_date)
    {
        $current_price = $this->price;
        $discount = Discount::where('inventory_id', $this->id)
            ->where('active', 1)
            ->where('start_date', '<', date('Y-m-d H:i:s', strtotime('now')))
            ->where('end_date', '>', date('Y-m-d H:i:s', strtotime('now')))
            ->where('discount_start_date', '<', $start_date)
            ->where('discount_end_date', '>', $end_date)
            ->orderBy('id', 'desc')
            ->first();

        if (!$discount) {
            return $this->discount;
        }

        return $discount;
    }

    public function getMultiplierAttribute()
    {
        $current_price = $this->price;
        $discount = Discount::where('inventory_id', $this->id)
            ->where('active', 1)
            ->where('start_date', '<', date('Y-m-d H:i:s', strtotime('now')))
            ->where('end_date', '>', date('Y-m-d H:i:s', strtotime('now')))
            ->orderBy('id', 'desc')
            ->first();

        return $discount->multiplier;
    }
}
