<?php

class PieChartData extends ChartData
{
    public function setData(array $data): PieChartData
    {
        $this->datasets = [(object) ['label' => '', 'data' => $data]];

        return $this;
    }

    public function json(): string
    {
        return json_encode($this->all());
    }

    public function all()
    {
        return (object) [
            'labels' => $this->labels,
            'datasets' => $this->datasets,
        ];
    }
}
