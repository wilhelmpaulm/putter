<?php

class Request extends Illuminate\Database\Eloquent\Model
{
    public function getUserAttribute()
    {
        return User::find((int) $this->user_id);
    }

    public static function getUserRequests($id, $status = [])
    {
        if (!$id) {
            return false;
        }

        $requests = false;
        $userIds = false;

        if ($status && is_array($status)) {
            $requests = self::where('user_id', $id)
                ->whereIn('status', $status)
                ->orderBy('id', 'desc')
                ->get();
        }
        if ($status) {
            $requests = self::where('user_id', $id)->get();

            return $requests;
        }

        return false;
    }

    public static function getDepartmentRequests($type, $status = [])
    {
        if (!$type) {
            return false;
        }

        $requests = [];

        if ($status && is_array($status)) {
            $requests = self::where('type', $type)
                ->whereIn('status', $status)
                ->orderBy('id', 'desc')
                ->get();

            return $requests;
        }

        return false;
    }

    public static function make($request_by, $request_type, $type = 'officer', $target_id = null, $message = null)
    {
        try {
            $request = new Request();
            $request->user_id = $request_by;
            $request->request = $request_type;
            $request->target_id = $target_id;
            $request->message = $message;
            if (is_int($type)) {
                $request->approved_by = $type;
                $request->type = 'member';
            } else {
                $request->type = $type;
            }
            $request->save();

            switch ($request->request):
        case 'request_tournament_officer':
            $body = 'Your request to create a tournament (Reservation'
            .' #'.padId($request->target_id).') has been'
                .' is now pending for review';
            Message::send(null, $body, [$request->user_id]);

            $body = 'A new tournement (Reservation #'.padId($request->target_id).')'
                .' is pending for review.';

            Message::sendToTypes(null, $body, ['officer', 'admin', 'administrator']);
            break;

            case 'request_reservation_officer':
            $body = 'Your request to create a facility reservation (Reservation'
            .' #'.padId($request->target_id).') has been'
                .' is now pending for review';
            Message::send(null, $body, [$request->user_id]);

            $body = 'A new facility reservation (Reservation #'.padId($request->target_id).')'
                .' is pending for review.';

            Message::sendToTypes(null, $body, ['officer', 'admin', 'administrator']);
            break;

            case 'request_banquet_officer':
            $body = 'Your request to create a banquet (Reservation'
            .' #'.padId($request->target_id).') has been'
                .' is now pending for review';
            Message::send(null, $body, [$request->user_id]);

            $body = 'A new banquet (Reservation #'.padId($request->target_id).')'
                .' is pending for review.';

            Message::sendToTypes(null, $body, ['officer', 'admin', 'administrator']);
            break;
            endswitch;

            return $request->id;
        } catch (Exception $e) {
            error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');
            Session::set('danger', 'Failed to create request.');

            return false;
        }

        return false;
    }

    public static function approve($id)
    {
        $request = self::find($id);
        if (!$request) {
            return false;
        }

        switch ($request->request):
        case 'membership_request':
        $user = User::find((int) $request['user_id']);
        $user->type = 'review';
        $user->access = 'dependent';
        $user->save();

        $body = 'This is to inform all that '.$user->full_name.' has'
        .' requested to become a member of the club.'
        .' Please feel free in commenting the candidates profile'
        .' to aid in revieing the application. '.linkTo("members/{$request['user_id']}");
        Message::sendToTypes(null, $body, ['members', 'accounting', 'officer']);

        $new_id = Request::make($user->id, 'membership_interview', 'officer');

        $body = 'Your membership request has been approved by an'
        .' officer and is currently in review.'
        .' You may track your request at  '.linkTo("track/$new_id");
        Mail::send('Membership Request In Review', [$user->email], $body);
        break;

        case 'membership_interview':
        $user = User::find((int) $request['user_id']);

        $body = 'This is to inform all that '.$user->full_name.' has'
            .' passed the initial membership review and will now be.'
            .' invited for an interview.';
        Message::sendToTypes(null, $body, ['members', 'accounting', 'officer']);

        $new_id = Request::make($user->id, 'membership_review', 'officer');

        $body = "Dear $user->first_name, we would like to interview you"
        .' regarding your request to be a member with our club.'
        .' Please contact us via mail at'
        .' '.Config::get('email', 'address').'. with your availability.'
        .' You may track your request at  '.linkTo("track/$new_id");
        Mail::send('Membership Request Interview', [$user->email], $body);
        break;

        case 'membership_review':
        $user = User::find((int) $request['user_id']);
        $user->type = 'member';
        $user->access = 'member';
        $user->active = 1;
        $user->save();

        $body = 'Please welcome our new member '.$user->full_name.'.';
        Message::sendToTypes(null, $body, ['members', 'accounting', 'officer', 'dependent'], [$user->id]);

        $body = "Dear $user->first_name, after careful consideration and deliberation amongst"
        .' our members, we would like to welcome you into our club. You may now'
        .' use your registered account by logging in '.linkTo('login').'. Please '
            .' feel free to contact us for further details.';
        Mail::send('Membership Request Approved', [$user->email], $body);
        break;

        case 'dependent_request':
        $user = User::find((int) $request['user_id']);
        $user->type = 'dependent';
        $user->access = 'dependent';
        $user->active = 1;
        $user->save();

        $body = 'Please welcome our new member '.$user->full_name.'.';
        Message::sendToTypes(null, $body, ['members', 'accounting', 'officer', 'dependent'], [$user->id]);

        $body = "Dear $user->first_name, after careful consideration and deliberation amongst"
        .' our members, we would like to welcome you into our club. You may now access'
        .' use your registered account by logging in '.linkTo('login').'. Please '
            .' feel free to contact us for further details.';
        Mail::send('Dependent Request Approved', [$user->email], $body);
        break;

        case 'independent_request':
        $user = User::find((int) $request['user_id']);
        $body = 'This is to inform all that '.$user->full_name.' (currently a depedent) has'
        .' requested to become an independent member of the club.'
        .' Please feel free in commenting the candidates profile'
        .' to aid in revieing the application. '.linkTo("members/{$user->id}");
        Message::sendToTypes(null, $body, ['members', 'accounting', 'officer'], [$user->id]);

        $new_id = Request::make($user->id, 'independent_interview', 'officer');

        $body = "Dear {$user->first_name}, your request is now being reviewed."
        .' You may track your request at '.linkTo("track/$new_id");
        Mail::send('Request To Be An Independent Member Pending', [$user->email], $body);
        break;

        case 'independent_interview':
        $user = User::find((int) $request['user_id']);

        $body = 'This is to inform all that '.$user->full_name.' has'
            .' passed the initial membership review and will now be.'
            .' invited for an interview.';
        Message::sendToTypes(null, $body, ['members', 'accounting', 'officer'], [$user->id]);

        $new_id = Request::make($user->id, 'independent_review', 'officer');

        $body = "Dear $user->first_name, we would like to interview you"
        .' regarding your request to be a member with our club.'
        .' Please contact us via mail at'
        .' '.Config::get('email', 'address').'. with your availability.'
        .' You may track your request at '.linkTo("track/$new_id");
        Mail::send('Request To Be An Independent Member Interview', [$user->email], $body);
        Message::send(null, $body, [$user->id]);
        break;

        case 'independent_review':
        $user = User::find((int) $request['user_id']);
        $user->type = 'member';
        $user->access = 'member';
        $user->active = 1;
        $user->save();

        $body = 'Please welcome our new independent member '.$user->full_name.'.';
        Message::sendToTypes(null, $body, ['members', 'accounting', 'officer', 'dependent'], [$user->id]);

        $body = "Dear $user->first_name, after careful consideration and deliberation amongst"
        .' our members, we would like to welcome you into our club as an independent'
        .' member. You may now reap the benefits bestowed to regular memebrs.'
        .' Use your registered account by logging in '.linkTo('login').'. Please '
            .' feel free to contact us for further details.';
        Mail::send('Membership Request Approved', [$user->email], $body);
        break;

        case 'membership_cancel':
        $user = User::find((int) $request['user_id']);
        if ($user->balance < 0.0) {
            Request::make($user->id, 'membership_cancel_accounting', 'accounting');
            $body = 'Please make sure to settle any dues with accounting.';
            Message::send(null, $body, [$user->id]);

            $body = $user->full_name.' has requested to cancel his/her membership.'
                    .' Please make sure to finalize all dues before finalizing his/her request.';
            Message::sendToTypes(null, $body, ['accounting']);
        } else {
            Request::make($user->id, 'membership_cancel_approval', (int) $user->id);
            $body = 'To finalize your request, please approve the cancellation on your requests page.';
            Message::send(null, $body, [$user->id]);
        }

        $body = 'An officer has approved your request to cancel your membership.';
        Message::send(null, $body, [$user->id]);
        break;

        case 'membership_cancel_accounting':
        $user = User::find((int) $request['user_id']);
        Request::make($user->id, 'membership_cancel_approval', (int) $user->id);

        $body = 'Your account was cleared by accounting.'
            .' To finalize your request, please approve'
            .' the cancellation on your requests page.';
        Message::send(null, $body, [$user->id]);
        break;

        case 'membership_cancel_approval':
        $dependents = User::where('sponsor_id', (int) $request['user_id'])->get();
        foreach ($dependents as $dependent) {
            $dependent->active = 0;
            $dependent->save();
        }

        $user = User::find((int) $request['user_id']);
        $body = "Dear {$user->first_name}, thank you for your patronage."
        .' We hope you enjoyed your membership with us. Please'
        .' feel free to contact us at '.Config::get('email', 'address')
            .' for any questions. We hope to hear from you again.'
            .' Thank you again from all of us at the club.';
        Mail::send('Membership Cancellation successful', [$user->email], $body);
        Session::set('success', 'Thank you for your patronage.'
            .' Please check your email for the details'
            .' regarding the cancellation of your membership');

        $user->active = 0;
        $user->save();

        User::logout();
        break;

        case 'request_tournament_officer':
        $user = User::find((int) $request['user_id']);
        $tournament = Reservation::find((int) $request['target_id']);
        $tournament->status = 'reviewed';
        $tournament->save();

        $body = 'Your request to create a tournament (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' reviewed by an officer. The accounting department'
            .' will now process your request.';

        Message::send(null, $body, [$user->id]);

        Request::make($user->id, 'request_tournament_accounting', 'accounting', $request['target_id']);
        $body = 'Tournament (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' reviewed by an officer and is awaiting accounting'
            .' review.';

        Message::sendToTypes(null, $body, ['accounting', 'officer']);

        Session::set('success', 'Tournament approved');
        break;

        case 'request_tournament_accounting':
        $user = User::find((int) $request['user_id']);
        $tournament = Reservation::find((int) $request['target_id']);
        $tournament->status = 'approved';
        $tournament->save();

        $body = user('first_name').' created a tournament reservation'
        ." on {$tournament->start_date} until {$tournament->end_date} at"
        .' '.$tournament->facility->name.'. Please make sure to come'
        .' at least 15 minutes before the tee time.'
        .' Name: '.$tournament->name
        .' | Label: '.$tournament->label;

        Message::send(null, $body, $tournament->guest_id_list);
        Mail::send('Tournament Reservation', User::whereIn('id', $tournament->guest_id_list)->pluck('email'), $body);

        $start_date = new DateTime($tournament->start_date);
        $since_start = $start_date->diff(new DateTime($tournament->end_date));

        $details = [];
        $inventory = Inventory::find($tournament->facility_id);
        $inventory->details = $body;
        $inventory->amount = (abs($since_start->i) / 15) + 1;
        $inventory->price = $inventory->current_price($tournament->start_date, $tournament->end_date);
        $details[] = $inventory;
        $invoice = Invoice::make(null, (int) $user->id, $details);

        $tournament->invoice_id = $invoice->id;
        $tournament->save();

        $body = 'Your request to create a tournament (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' approved';

        Message::send(null, $body, [$user->id]);

        $body = 'Tournament (Reservation'
        .' #'.padId($tournament->id).') has been approved'
            .' by the accounting department.';

        Message::sendToTypes(null, $body, ['accounting', 'staff', 'officer']);

        Session::set('success', 'Tournament approved');
        break;

        case 'request_reservation_officer':
        $user = User::find((int) $request['user_id']);
        $tournament = Reservation::find((int) $request['target_id']);
        $tournament->status = 'reviewed';
        $tournament->save();

        $body = 'Your request to create a facility reservation (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' reviewed by an officer. The accounting department'
            .' will now process your request.';

        Message::send(null, $body, [$user->id]);

        Request::make($user->id, 'request_reservation_accounting', 'accounting', $request['target_id']);
        $body = 'Facility reservation (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' reviewed by an officer and is awaiting accounting'
            .' review.';

        Message::sendToTypes(null, $body, ['accounting', 'officer']);

        Session::set('success', 'Tournament approved');
        break;

        case 'request_reservation_accounting':
        $user = User::find((int) $request['user_id']);
        $tournament = Reservation::find((int) $request['target_id']);
        $tournament->status = 'approved';
        $tournament->save();

        $body = user('first_name').' created a facility reservation'
        ." for {$tournament->capacity} people"
        ." on {$tournament->start_date} until {$tournament->end_date} at"
        .' '.$tournament->facility->name.'.'
        .' Name: '.$tournament->name
        .' | Label: '.$tournament->label;

        Message::send(null, $body, $tournament->guest_id_list);
        Mail::send('Facility Reservation', User::whereIn('id', $tournament->guest_id_list)->pluck('email'), $body);

        $start_date = new DateTime($tournament->start_date);
        $since_start = $start_date->diff(new DateTime($tournament->end_date));

        $details = [];
        $inventory = Inventory::find($tournament->facility_id);
        $inventory->details = $body;
        $inventory->amount = abs($since_start->h) + 1;
        $inventory->price = $inventory->current_price($tournament->start_date, $tournament->end_date);
        $details[] = $inventory;
        $invoice = Invoice::make(null, (int) $user->id, $details);

        $tournament->invoice_id = $invoice->id;
        $tournament->save();

        $body = 'Your request to create a facility reservation (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' approved';

        Message::send(null, $body, [$user->id]);

        $body = 'Facility Reservation (Reservation'
        .' #'.padId($tournament->id).') has been approved'
            .' by the accounting department.';

        Message::sendToTypes(null, $body, ['accounting', 'staff', 'officer']);

        Session::set('success', 'Reservation approved');
        break;

        case 'request_banquet_officer':
        $user = User::find((int) $request['user_id']);
        $tournament = Reservation::find((int) $request['target_id']);
        $tournament->status = 'reviewed';
        $tournament->save();

        $body = 'Your request to create a banquet (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' reviewed by an officer. The accounting department'
            .' will now process your request.';

        Message::send(null, $body, [$user->id]);

        Request::make($user->id, 'request_banquet_accounting', 'accounting', $request['target_id']);
        $body = 'Banquet (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' reviewed by an officer and is awaiting accounting'
            .' review.';

        Message::sendToTypes(null, $body, ['accounting', 'officer']);

        Session::set('success', 'Banquet approved');
        break;

        case 'request_banquet_accounting':
        $user = User::find((int) $request['user_id']);
        $tournament = Reservation::find((int) $request['target_id']);
        $tournament->status = 'approved';
        $tournament->save();

        $body = user('first_name').' created a banquet'
        ." for {$tournament->capacity} people"
        ." on {$tournament->start_date} until {$tournament->end_date} at"
        .' '.$tournament->facility->name.'.'
        .' Name: '.$tournament->name
        .' | Label: '.$tournament->label;

        Message::send(null, $body, $tournament->guest_id_list);
        Mail::send('Banquet Reservation', User::whereIn('id', $tournament->guest_id_list)->pluck('email'), $body);

        $start_date = new DateTime($tournament->start_date);
        $since_start = $start_date->diff(new DateTime($tournament->end_date));

        $details = [];
        $inventory = Inventory::find($tournament->facility_id);
        $inventory->details = $body;
        $inventory->amount = ($since_start->d + 1) * 24;
        $inventory->price = $inventory->current_price($tournament->start_date, $tournament->end_date);
        $details[] = $inventory;
        $invoice = Invoice::make(null, (int) $user->id, $details);

        $tournament->invoice_id = $invoice->id;
        $tournament->save();

        $body = 'Your request to create a banquet (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' approved';

        Message::send(null, $body, [$user->id]);

        $body = 'Banquet (Reservation'
        .' #'.padId($tournament->id).') has been approved'
            .' by the accounting department.';

        Message::sendToTypes(null, $body, ['accounting', 'staff', 'officer']);

        Session::set('success', 'Banquet approved');
        break;
        endswitch;

        try {
            $request->status = 'approved';
            $request->approved_by = user('id');
            $request->save();

            Session::set('success', 'Request approved.');

            return $request;
        } catch (Exception $e) {
            error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');
            Session::set('danger', 'Failed to update request.');

            return false;
        }
    }

    public static function reject($id)
    {
        $request = self::find($id);
        if (!$request) {
            return false;
        }

        switch ($request->request):
    case 'membership_request':
        $user = User::find((int) $request['user_id']);
        $body = 'We are sorry to inform you that your request to be a'
            .' member of the club has been rejected. Feel free to'
            .' contact us and try again at a later date. Thank you'
            .' for your consideration.';
        Mail::send('Membership Request Rejected', [$user->email], $body);

        break;
        case 'membership_interview':
    case 'membership_review':
        $user = User::find((int) $request['user_id']);
        $user->type = 'pending';
        $user->access = 'dependent';
        $user->save();

        $body = 'This is to inform the members of the club that'
        .' the application of '.$user->full_name.' has'
            .' rejected.';
        Message::sendToTypes(null, $body, ['members', 'accounting', 'officer', 'dependent'], [$user->id]);

        $body = "Dear $user->first_name, after careful consideration and deliberation amongst"
            .' our members, we are saddened to inform you that we currently would not be'
            .' able to accomidate your request to become a member. Please'
            .' feel free to contact us for further details.';
        Mail::send('Membership Review Status', [$user->email], $body);
        break;

        case 'dependent_request':
        $user = User::find((int) $request['user_id']);
        $user->type = 'pending';
        $user->access = 'dependent';
        $user->save();

        $body = 'This is to inform you that'
        .' the application of '.$user->full_name.' has'
            .' rejected.';
        Message::send(null, $body, [$user->sponsored_by]);

        $body = "Dear $user->first_name, after careful consideration and deliberation amongst"
            .' our members, we are saddened to inform you that we currently would not be'
            .' able to accomidate your request to become a member. Please'
            .' feel free to contact us for further details.';
        Mail::send('Dependent Review Status', [$user->email], $body);

        // no break
        case 'independent_request':
    case 'independent_interview':
    case 'independent_review':
        $body = "Dear $user->first_name, after careful consideration and deliberation amongst"
            .' our members, we are saddened to inform you that we currently would not be'
            .' able to accomidate your request to become a member. Please'
            .' feel free to contact us for further details.';
        Mail::send('Request To Be An Independent Member', [$user->email], $body);
        Message::send(null, $body, [$user->id]);
        break;

        case 'request_tournament_officer':
    case 'request_tournament_accounting':
        $user = User::find((int) $request['user_id']);
        $tournament = Reservation::find((int) $request['target_id']);
        $tournament->status = 'rejected';
        $tournament->save();

        $body = 'Your request to create a tournament (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' rejected';

        Message::send(null, $body, [$user->id]);

        $body = 'Tournament (Reservation'
        .' #'.padId($tournament->id).') has been rejected';

        Message::sendToTypes(null, $body, ['accounting', 'staff', 'officer']);
        break;

        case 'request_reservation_officer':
    case 'request_reservation_accounting':
        $user = User::find((int) $request['user_id']);
        $tournament = Reservation::find((int) $request['target_id']);
        $tournament->status = 'rejected';
        $tournament->save();

        $body = 'Your request to create a facility reservation (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' rejected';

        Message::send(null, $body, [$user->id]);

        $body = 'Facility reservation (Reservation'
        .' #'.padId($tournament->id).') has been rejected';

        Message::sendToTypes(null, $body, ['accounting', 'staff', 'officer']);
        break;

        case 'request_banquet_officer':
    case 'request_banquet_accounting':
        $user = User::find((int) $request['user_id']);
        $tournament = Reservation::find((int) $request['target_id']);
        $tournament->status = 'rejected';
        $tournament->save();

        $body = 'Your request to create a banquet (Reservation'
        .' #'.padId($tournament->id).') has been'
            .' rejected';

        Message::send(null, $body, [$user->id]);

        $body = 'Banquet (Reservation'
        .' #'.padId($tournament->id).') has been rejected';

        Message::sendToTypes(null, $body, ['accounting', 'staff', 'officer']);
        break;
        endswitch;

        try {
            $request->status = 'rejected';
            $request->approved_by = user('id');
            $request->save();

            Session::set('success', 'Request rejected.');

            return $request;
        } catch (Exception $e) {
            error_log(json_encode([$e->getTraceAsString(), $e->getMessage()], JSON_PRETTY_PRINT).PHP_EOL, 3, './logs/error.log');
            Session::set('danger', 'Failed to update request.');

            return false;
        }
    }

    public static function getPendingCountBefore($type)
    {
        return self::getPendingCount($type) - Request::where('type', $type)
            ->where('created_at', '<', date('Y-m-d 00:00:00', strtotime('now')))
            ->where('status', 'pending')
            ->count();
    }

    public static function getPendingCount($type)
    {
        return Request::where('type', $type)
            ->where('status', 'pending')
            ->count();
    }
}
