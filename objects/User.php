<?php

class User extends Illuminate\Database\Eloquent\Model
{
    public static function system()
    {
        $user = new User();
        $user->first_name = 'System';
        $user->last_name = 'Generated';
        $user->type = 'system';
        $user->access = 'admin';
        $user->short = 'SYS';
        $user->color = 'list';
        $user->image = Config::get('system', 'logo');
        $user->email = Config::get('email', 'address');

        return $user;
    }

    public function getDependentsAttribute()
    {
        return User::where('sponsor_id', $this->id)->get();
    }

    public function getAccessListAttribute()
    {
        return Access::where('type', $this->access)->first()->getList();
    }

    public function getCommentsAttribute()
    {
        return Comment::where('type', 'member')
            ->where('topic_id', (int) $this->id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function getNameAttribute()
    {
        return title_case("{$this->first_name} {$this->last_name}");
    }

    public function getFullNameAttribute()
    {
        return title_case("{$this->first_name} {$this->last_name}");
    }

    public function getSponsorAttribute()
    {
        return User::find((int) $this->sponsor_id);
    }

    public function getEndorserAttribute()
    {
        return User::find((int) $this->endorser_id);
    }

    public function getNegativeSinceAttribute()
    {
        if ($this->balance > 0) {
            return '';
        }
        $billing = Invoice::where('user_id', $this->id)->where('type', 'billing')->where('current_balance', '<', 0)->first();
        if (!$billing) {
            return false;
        }

        return $billing->created_at;
    }

    public static function login($email, $password)
    {
        if (!$email || !$password) {
            return false;
        }

        $user = self::where('email', 'like', trim($email))
            ->where('password', $password)
            ->first();
        if ($user) {
            Session::set('user', $user);
            Auth::check();

            return $user;
        }

        return false;
    }

    public static function logout()
    {
        session_unset();
        Session::pop('user');
        Session::pop('club');
        Session::pop('email');
        Session::pop('system');
    }
}
