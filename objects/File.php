<?php

class File
{
    public static function upload($upload, $folder = 'uploads')
    {
        $storage = new \Upload\Storage\FileSystem($_SERVER['DOCUMENT_ROOT']."/$folder");
        $file = new \Upload\File($upload, $storage);

        // Optionally you can rename the file on upload
        $new_filename = uniqid();
        $file->setName($new_filename);

        // Validate file upload
        // MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
        //        $file->addValidations(array(
        //            // Ensure file is of type "image/png"
        //            new \Upload\Validation\Mimetype('image/png'),
        //            //You can also add multi mimetype validation
        //            //new \Upload\Validation\Mimetype(array('image/png', 'image/gif'))
        //            // Ensure file is no larger than 5M (use "B", "K", M", or "G")
        //            new \Upload\Validation\Size('5M')
        //        ));
        // Access data about the file that has been uploaded
        $data = [
            'name' => $file->getNameWithExtension(),
            'extension' => $file->getExtension(),
            'mime' => $file->getMimetype(),
            'size' => $file->getSize(),
            'md5' => $file->getMd5(),
            'dimensions' => $file->getDimensions(),
        ];

        // Try to upload file
        try {
            // Success!
            $file->upload();
        } catch (\Exception $e) {
            // Fail!
            $errors = $file->getErrors();
            Session::set('danger', $errors);
        }

        return $data;
    }

    public static function get($filename, $folder = 'uploads')
    {
        if (empty($filename)) {
            return null;
        }
        $location = $_SERVER['DOCUMENT_ROOT']."/{$folder}/{$filename}";
        if (file_exists($location)) {
            return "/{$folder}/{$filename}";
        } elseif (file_exists($_SERVER['DOCUMENT_ROOT']."/app/uploads/{$filename}")) {
            return "/app/uploads/{$filename}";
        } else {
            return '/assets/images/empty.png';
        }
    }
}
