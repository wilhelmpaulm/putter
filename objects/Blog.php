<?php

class Blog extends Illuminate\Database\Eloquent\Model
{
    public static function types()
    {
        return ['blog', 'news'];
    }

    public function getTagListAttribute()
    {
        return json_decode($this->tags) ?? [];
    }

    public function getUserAttribute()
    {
        return User::find((int) $this->user_id);
    }

    public function getCommentsAttribute()
    {
        return Comment::where('type', 'blog')
            ->where('topic_id', (int) $this->id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function view()
    {
        $this->views = $this->views + 1;
        $this->save();
    }
}
