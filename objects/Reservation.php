<?php

class Reservation extends Illuminate\Database\Eloquent\Model
{
    public $appends = ['expired', 'true_appearance'];

    public function getUserAttribute()
    {
        return User::find($this->user_id);
    }

    public function getFacilityAttribute()
    {
        return Facility::find((int) $this->facility_id);
    }

    public function getInvoiceAttribute()
    {
        return Invoice::find((int) $this->invoice_id);
    }

    public function getYearAttribute()
    {
        return substr($this->created_at, 0, 4) ?? null;
    }

    public function getTagListAttribute()
    {
        return json_decode($this->tags) ?? [];
    }

    public function getGuestListAttribute()
    {
        return User::whereIn('id', json_decode($this->guests))->get();
    }

    public function getGuestIdListAttribute()
    {
        return User::whereIn('id', json_decode($this->guests))->pluck('id');
    }

    public static function getAvailableTeeTimes($facility_id, $date)
    {
        $times = self::getTeeTimes();
        $reserved = [];
        //$reservations = Reservation::where('facility_id', $facility_id)
        //                ->where('status', 'approved')
        //                ->whereDate('start_date', '=', date($date))
        //                ->pluck('start_date');
        $reservations = Reservation::where('facility_id', $facility_id)
            ->where('status', 'approved')
            ->whereDate('start_date', '=', date($date))
            ->get();

        foreach ($reservations as $res) {
            $start = substr($res->start_date, 11);
            $end = substr($res->end_date, 11);
            foreach ($times as $key => $time) {
                if (!$end && $key == $start) {
                    unset($times[$key]);
                }
                if ($end && $start <= $key && $key <= $end) {
                    unset($times[$key]);
                }
            }
        }

        return $times;
    }

    public static function getAvailableReservationTimes($facility_id, $date)
    {
        $times = self::getReservationTimes();
        $reserved = [];
        $reservations = Reservation::where('facility_id', $facility_id)
            ->where('status', 'approved')
            ->whereDate('start_date', '=', date($date))
            ->pluck('start_date');

        foreach ($reservations as $res) {
            $reserved[] = substr($res, 11);
        }
        foreach ($reserved as $res) {
            unset($times[$res]);
        }

        return $times;
    }

    public static function getReservationTimes()
    {
        return self::hoursRange(0, 60 * 60 * 24, 60 * 60, 'h:i A');
    }

    public static function getTeeTimes()
    {
        return self::hoursRange(28800, 61200, 60 * 15, 'h:i A');
    }

    public static function hoursRange($lower = 0, $upper = 86400, $step = 3600, $format = '')
    {
        $times = array();

        if (empty($format)) {
            $format = 'g:i a';
        }

        foreach (range($lower, $upper, $step) as $increment) {
            $increment = gmdate('H:i:s', $increment);

            list($hour, $minutes) = explode(':', $increment);

            $date = new DateTime($hour.':'.$minutes.':00');

            $times[(string) $increment] = $date->format($format);
        }

        return $times;
    }

    public static function getReservationsCountToday($type)
    {
        return Reservation::where('status', 'approved')
            ->where('type', $type)
            ->where('created_at', '>', date('Y-m-d 00:00:00', strtotime('now')))
            ->count();
    }

    public static function getPendingReservationsCountToday($type)
    {
        return Reservation::where('status', 'approved')
            ->where('type', $type)
            ->where('created_at', '>', date('Y-m-d 00:00:00', strtotime('now')))
            ->where('start_date', '>', date('Y-m-d H:i:s', strtotime('now')))
            ->where('start_date', '<', date('Y-m-d 23:59:59', strtotime('now')))
            ->count();
    }

    public static function getCompletedReservationsCountToday($type)
    {
        return Reservation::where('status', 'approved')
            ->where('type', $type)
            ->where('created_at', '>', date('Y-m-d 00:00:00', strtotime('now')))
            ->where('start_date', '<', date('Y-m-d H:i:s', strtotime('now')))
            ->where('start_date', '<', date('Y-m-d 23:59:59', strtotime('now')))
            ->count();
    }

    public function getExpiredAttribute()
    {
        return Carbon\Carbon::now()->gt($this->end_date);
    }

    public function getTrueAppearanceAttribute()
    {
        if($this->expired && $this->appearance == 'pending') {
            return 'missed';
        }
        return $this->appearance;
    }
}
