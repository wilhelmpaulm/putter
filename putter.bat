@echo off
echo.
echo Usage:
echo     putter [command]
echo.
echo ----------------------------------------------------
echo.
echo Commands:
echo     start - run the server
echo     seed  - clean the database
echo     clean - clean the database then start the server
echo.
echo ----------------------------------------------------
echo.

:loop 
if "%1"=="" goto :done
echo Starting Command:
echo     Press CTRL + C to terminate the command early
echo.
echo ----------------------------------------------------
echo.
php composer.phar %1
shift
goto :loop

:done
echo Please use a command



:EOF