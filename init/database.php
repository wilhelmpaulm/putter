<?php

use Illuminate\Database\Capsule\Manager as Capsule;

// creates a connection to the database
$capsule = new Capsule();
$capsule->addConnection([
    'driver' => 'sqlite',
    'database' => __DIR__ . '/../database/' . (Config::get('system', 'seed') ? 'seed' : 'database') . '.sqlite',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();
