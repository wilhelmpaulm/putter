<?php

use Tracy\Debugger as Debugger;

$router = Router::Router();
$router->setBasePath('');

include './routes.php';
// dd(json_encode($router->getRouteList()));
// match current request url
$match = $router->match();

// call closure or throw 404 status
if ($match && (is_callable($match['target']) || is_callable(explode('#', $match['target'])))) {
    // makes the error pages pretty
    if (Config::get('system', 'debug')) {
        Debugger::enable();
    }

    // track site traffic
    if (Config::get('system', 'traffic')) {
        Ping::track();
    }

    // check middlewares
    Middleware::authorization($match['name']);
    Middleware::teetimeLimit($match['name']);
    Middleware::tournamentLimit($match['name']);
    Middleware::reservationLimit($match['name']);
    Middleware::banquetLimit($match['name']);

    if (is_callable($match['target'])) {
        return call_user_func_array($match['target'], $match['params']);
    } elseif (is_callable(explode('#', $match['target']))) {
        return call_user_func_array(explode('#', $match['target']), $match['params']);
    }
} else {
    // no route was matched
    return IndexController::getError(404);
}
