## super duper quick start
- open `cmd` and `cd` into the project folder
- run `putter init`
- open `127.0.0.1:8000` in the browser

## super quick start
- open `cmd` and `cd` into the project folder
- run `composer install`
- run `composer start`
- open `127.0.0.1:8000` in the browser

## requirements
- [php](http://www.wampserver.com/en/) installation (version 5.6 or higher)
-- make sure to [enable](https://www.wikihow.com/Enable-SQLite-for-PHP-5.X-in-Windows) [sqlite](https://www.sqlite.org/download.html) 
- [composer](https://getcomposer.org/download/)

## enable insecure gmail sending
- login to gmail
- u: `xyz.putter`
- p: `putterputter`
- [enable insecure sending](https://accounts.google.com/DisplayUnlockCaptcha) 
- also here [enable insecure sending](https://myaccount.google.com/lesssecureapps) 

## install the dependencies
- make sure the you've installed all the requirements
- open `cmd`
- go to the project directory in `cmd`
- type `cd <project_folder>`
- type `composer install`
- this might take a while . . . 

## to run the php server
- open `cmd`
- go to the project directory in `cmd`
- type `cd <project_folder>`
- run the `php server`
- type `php -S 127.0.0.1:8000 -F index.php`
- open the site in your browser
- url is `127.0.0.1:8000/`